<html>
  <head><meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>UKT ditutup</title>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>assets/images/uin.png" />

  	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/css.css" type="text/css "/>


    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>


  </head>
  <body>
  <div class="headerK">
	<div class="container">
		<div class="col-xs-12 col-sm-12 col-md-12" >
			<div class="row">
				<div class="logo"><img src="<?php echo base_url();?>assets/images/aan.png"/></div>
				<div class="judulK">Pengajuan UKT </br>Mahasiswa UIN Sunan Ampel Surabaya</div>
			</div>
		</div>
	</div>
	</div>
	<div class="container">
				<div class="col-xs-12 col-sm-12 col-md-12" style=" margin-top:4vw;" >
			<div class="row"align="center">
					<div class="modal-header">
						<h2 class="modal-title" style="color:#000;" >
              Website <a href="<?php echo base_url(); ?>"><?php echo base_url(); ?></a>
              sudah ditutup untuk Informasi Lainnya bisa mengakses
              <a href="http://www.uinsby.ac.id/">http://www.uinsby.ac.id/</a>
              <br>
			  <!-- Untuk Jalur SBMPTN Pengumuman hasil UKT tanggal 16 Juli 2018
			  <br/>
			  <br/>
				Pengisian Berkas Jalur UMPTKIN dibuka tanggal 12 sampai 15 Juli 2018
              Untuk persyaratan UKT bisa dilihat <a href="<?php echo base_url(); ?>assets/document/persyaratanUKT.jpeg">disini</a>  
			  -->
            </h2>
					</div>
				</div>
			</div>
		</div>
  </body>
 </html>
