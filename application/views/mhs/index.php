<html>
  <head><meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Pengajuan UKT</title>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>assets/images/uin.png" />

  	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/css.css" type="text/css "/>
    <link href="<?php echo base_url(); ?>assets/admin/css/style.css" rel="stylesheet">


    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>

  </head>
  <body>
  <div class="headerK">
	<div class="container">
		<div class="col-xs-12 col-sm-12 col-md-12" >
			<div class="row">
				<div class="logo"><img src="<?php echo base_url();?>assets/images/aan.png"/></div>
				<div class="judulK">Pengajuan UKT </br>Mahasiswa UIN Sunan Ampel Surabaya</div>
			</div>
		</div>
	</div>
	</div>
	<div class="container">
				<div class="col-xs-12 col-sm-12 col-md-12" style="width:40vw;margin-left:23vw; margin-top:2vw;" >
			<div class="row"align="center">
					<form action="<?php echo base_url();?>home/cek" method="POST">
					<div class="modal-header">
						<h2 class="modal-title" >Form Login</h2>
					</div>
					<div class="modal-body">
						<div class="form-group">
              <div class="col-md-12">
                <label for="nim" style="text-align:left;">No Pendaftaran</label>
              </div>
              <div class="col-md-12">
                <input type="text" class="form-control" id="pendaftaran"  placeholder="Masukkan no Pendaftaran" name="pendaftaran" required />
                <br>
              </div>
              <br>
              <div class="col-md-12">
                <label for="nama"  style="margin-bottom:10px;">Tanggal Lahir:</label>
              </div>
              <div class="col-md-12">
                <div class="col-md-3">
				  <label for="nama" >Hari</label>
                  <select class="form-control" id="hari" name="hari">
                    <?php
                    for ($i=1; $i <= 31; $i++) {
                      if (strlen($i) == 1) {
                        $i = "0".$i;
                      }
                      ?>
                      <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                    <?php
                    }
                    ?>
                  </select>
                </div>
                <div class="col-md-3">

				  <label for="nama" >Bulan</label>
                  <select class="form-control" id="bulan" name="bulan">
                    <?php
                    for ($i=1; $i <= 12; $i++) {
                      if (strlen($i) == 1) {
                        $i = "0".$i;
                      }
                      ?>
                      <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                    <?php
                    }
                    ?>
                  </select>
                </div>
                <div class="col-md-4">

				  <label for="nama" >Tahun</label>
                  <select class="form-control" id="tahun" name="tahun">
                    <?php
                    $tahun = date("Y");
                    for ($i=$tahun; $i >= 1980; $i--) { ?>
                      <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                    <?php
                    }
                    ?>
                  </select>
                </div>

              </div>
						</div>
					</div>
					<div class="modal-footer">
						<input style="" class="btn btn-success" type="submit" name="masuk" id="masuk" value="Masuk" />
					</div>
					</form>


          <div class="alert alert-danger alert-raised alert-dismissible fade in" role="alert">
            <strong>Ingat ! Pengumuman jalur SPANPTKIN</strong>
            <ul>
              <li>Pengumuman UKT Tanggal 11-04-2019 </li>
            </ul>
            <strong>Ingat ! Pengumuman jalur SNMPTN</strong>
            <ul>
              <li>Pembayaran dilaksanakan Tanggal 10 sampai 12 April 2019 </li>
            </ul>
              <!-- Untuk persyaratan UKT bisa dilihat <a href="<?php echo base_url(); ?>assets/document/persyaratanUKT2.jpg">disini</a> -->

            <!-- <strong>Ingat ! Pengumuman jalur <?php echo $jalur;?></strong>
            <ul>
              <li>Pengisian Borang UKT Tanggal <?php echo $set[2]['tgl_mulai1'];?> Sampai <?php echo $set[2]['tgl_akhir1'];?> </li>
              <li>Pengumuman UKT Tanggal <?php echo $set[0]['tgl_mulai1'];?> </li>
            </ul> -->
            <!-- Perpanjangan sampai tanggal 9 Juli 2018 jam 23:59 
			         Pengisian berkas UKT Mandiri dimulai tanggal 09 Agustus 2018 jam 09:00. Batas akhir pengisian tanggal 10 Agustus 2018 jam 23:59. -->
              
			  <!-- <br/> -->
              <!-- Untuk persyaratan UKT bisa dilihat <a href="<?php echo base_url(); ?>assets/document/persyaratanUKT2.jpg">disini</a> -->
              <!-- Untuk persyaratan UKT bisa dilihat <a href="https://drive.google.com/file/d/1Y6kU7q-FCudmtJqI-cGJuCxTfWBenCBf/view">disini</a> -->
          </div>
				</div>
			</div>
		</div>
  </body>
 </html>
