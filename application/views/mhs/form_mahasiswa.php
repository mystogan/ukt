
							<div class="kiri">
								<div class="kotJ">

									<div class="jud1">
										<?php echo date("d-m-Y");;?>
									</div>
									<div class="jud">
										DATA PENDUKUNG
									</div>
								</div>
								<div class="content">
									  <!-- Nav tabs -->
									<div class="card">
									<div class="alert alert-danger" role="alert">
										<span class="glyphicon glyphicon-info-sign" style="margin-right:1vw;"></span> - Data Yang ditulis dan dilampirkan harus benar dan lengkap, jika tidak maka kemungkinan dapat digugurkan<br/>
										<span class="glyphicon glyphicon-info-sign" style="margin-right:1vw;"></span> - Untuk melihat upload Sukses cek lampiran jika berubah menjadi gambar maka SUKSES jika tidak upload lagi<br/>
										<span class="glyphicon glyphicon-info-sign" style="margin-right:1vw;"></span> - Untuk Melihat Tutorial pengisian silahkan klik <a href="https://www.youtube.com/watch?v=pcZZe1Wn26s">disini</a><br/>
										<!-- <span class="glyphicon glyphicon-info-sign" style="margin-right:1vw;"></span> - Untuk pertanyaan yang biasa ditanyakan klik <a href="<?php echo base_url();?>assets/document/QA.pdf">disini </a> <br/> -->
										<span class="glyphicon glyphicon-info-sign" style="margin-right:1vw;"></span> - Untuk pengaduan silahkan mengirim pertanyaan di email <STRONG>ask.ukt@uinsby.ac.id</STRONG> <br/>
										<span class="glyphicon glyphicon-info-sign" style="margin-right:1vw;"></span> - Untuk Finalisasi silahkan menggunakan Komputer atau laptop<br/>
									</div>
										<ul class="nav nav-tabs" style="" role="tablist">
											<li role="presentation" class="active"><a href="#home" data-toggle="tab" aria-controls="home" role="tab" >Data Diri</a></li>
											<li role="presentation" ><a href="#profile" data-toggle="tab" aria-controls="profile" role="tab" >Keluarga</a></li>
											<li role="presentation" ><a href="#messages" data-toggle="tab" aria-controls="messages" role="tab" >Rumah Tinggal</a></li>
											<li role="presentation" ><a href="#settings" data-toggle="tab" aria-controls="settings" role="tab" >Lampiran</a></li>
										</ul>
										<div class="tab-content" style="border:1px solid #2b975b;padding-bottom:8vh; border-top:none;border-radius:5px;">
										<div role="tabpanel" class="tab-pane active" id="home" >
											<form action="<?php echo base_url()?>home/save1" method="post">
											<input type="hidden" id="kode_mhs" name="kode_mhs" value="<?php echo $user;?>" />

											<div class="form-group">
											  <label for="nama" class="lb">Tanggal Lahir</label>
											  <input type="text" class="form-control" style="height:8vh;" id="tgl_lhr" name="tgl_lhr" disabled value="<?php echo $profil['tgl_lhr'];?>" />
											</div>
											<div class="form-group">
												<label for="nama" class="lb">Alamat</label>
												<textarea  style="height:8vh;" class="form-control" id="alamat" name="alamat" ><?php echo $profil['alamat'];?></textarea>
											</div>
											<div class="form-group">
												<label for="nama" class="lb">Gol Darah</label>
												<div class="form-group">
												  <select class="form-control" id="gol_darah" name="gol_darah">
													<option value="A" <?php if($profil['gol_dar'] == "A"){ echo "selected";}?> >A</option>
													<option value="B" <?php if($profil['gol_dar'] == "B"){ echo "selected";}?>>B</option>
													<option value="AB" <?php if($profil['gol_dar'] == "AB"){ echo "selected";}?>>AB</option>
													<option value="O" <?php if($profil['gol_dar'] == "O"){ echo "selected";}?>>O</option>
												  </select>
												</div>
											</div>

											<div class="form-group">
												<label for="nama" class="lb">Provinsi</label>
												<div class="form-group">
												  <select onclick="javascript:cek();" class="form-control" id="provinsi" name="provinsi">
												  <?php

												  foreach($prov as $provinsi){ ?>
													<option value="<?php echo $provinsi['id']; ?>" <?php if($provinsi['id'] == $profil['prov']){ echo "selected";}?> ><?php echo $provinsi['name'];?></option>
												  <?php
												  }
												  ?>

												  </select>
												</div>
											</div>
											<div class="form-group">
												<label for="nama" class="lb">Kota</label>
												<div class="form-group">
												  <select class="form-control" id="kota" name="kota">
													<?php
													if($profil['kab'] == "" || $profil['kab'] == "0"){ ?>
													<option>-- Pilih Provinsi --</option>
													<?php
													}else { ?>
													<option value="<?php echo $profil['kab'];?>"><?php echo $profil['nama_kab'];?></option>
													<?php
													}
													?>
												  </select>
												</div>
											</div>
											<div class="form-group">
												<label for="nama" class="lb">Ukuran Jas</label>
												<div class="form-group">
												  <select class="form-control" id="ukuran_baju" name="ukuran_baju">
													<option <?php if($profil['ukuran_baju'] == "S"){ echo "selected";}?> >S</option>
													<option <?php if($profil['ukuran_baju'] == "M"){ echo "selected";}?> >M</option>
													<option <?php if($profil['ukuran_baju'] == "L"){ echo "selected";}?> >L</option>
													<option <?php if($profil['ukuran_baju'] == "XL"){ echo "selected";}?> >XL</option>
													<option <?php if($profil['ukuran_baju'] == "XXL"){ echo "selected";}?> >XXL</option>
												  </select>
												</div>
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Kode Pos</label>
											  <input type="number" class="form-control" id="kode_pos" name="kode_pos" value="<?php echo $profil['kode_pos'];?>" required />
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Telp Rumah</label>
											  <input type="text" class="form-control" id="telp_rumah" name="telp_rumah" value="<?php echo $profil['tlp_rmh'];?>" required />
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">No HP</label>
											  <input type="number" class="form-control" id="hp" name="hp" value="<?php echo $profil['hp'];?>" required />
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Email</label>
											  <input type="text" class="form-control" id="email" name="email" value="<?php echo $profil['email'];?>" required />
											</div>
											<div class="form-group">
												<label for="nama" class="lb">Agama</label>
												<div class="form-group">
												  <select class="form-control" id="agama" name="agama">
													<option <?php  if($profil['agama'] == '1'){ echo "selected";}?> value="1">Islam</option>
													<option <?php  if($profil['agama'] == '2'){ echo "selected";}?> value="2">Katholik</option>
													<option <?php  if($profil['agama'] == '3'){ echo "selected";}?> value="3">Kristen</option>
													<option <?php  if($profil['agama'] == '4'){ echo "selected";}?> value="4">Hindu</option>
													<option <?php  if($profil['agama'] == '5'){ echo "selected";}?> value="5">Buddha</option>
												  </select>
												</div>
											</div>
											<div class="form-group">
												<label for="nama" class="lb" >Kewarganegaraan</label>
												<div class="form-group">
												  <select class="form-control" id="warganegara" name="warganegara">
													<option value="1" <?php if($profil['warganegara'] == "1"){ echo "selected";}?> >Warga Negara Indonesia</option>
													<option value="2" <?php if($profil['warganegara'] == "2"){ echo "selected";}?> >Warga Negara Asing</option>
												  </select>
												</div>
											</div>
											<input type="submit" class="btn btn-success pull-right next-step" value="Simpan">
											</form>
										</div>
									<div role="tabpanel" class="tab-pane" id="profile">
											<div class="alert alert-success" role="alert" style="margin-top:-10px;">
												<span class="glyphicon glyphicon-info-sign" style="margin-right:1vw;"></span>
												Jika salah satu atau kedua orang tua sudah tiada, silahkan klik (info) untuk mengetahui informasi tersebut
											</div>
										<form action="<?php echo base_url()?>home/save2" method="post" id="formkeluarga">
											<div class="form-group">
											  <label for="usr" class="lb">Nama Ayah/Wali
													&emsp;&emsp; 
													<span data-toggle="modal" data-target="#ortu_modal" style="color:#4398ed">(info)</span> 
												</label>
											  <input type="text" class="form-control" placeholder="Muhammad Paijo"  id="nama_ayah" name="nama_ayah" value="<?php echo $profil['nama_ayah'];?>"  />
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Pekerjaan Ayah/Wali</label>
											  <div class="checkbox" >
													<label class="checkbox-inline"><input type="radio" name="peker_ayah" class="peker_ayah1" onclick="javascript:as();" <?php if($profil['peker_ayah'] == "" || $profil['peker_ayah'] == "PNS"){echo "checked"; $o=1;} ?>  value="PNS">PNS</label>
													<label class="checkbox-inline"><input type="radio" name="peker_ayah" class="peker_ayah1" onclick="javascript:as();" <?php if($profil['peker_ayah'] == "Pegawai Swasta"){echo "checked";$o=1;} ?> value="Pegawai Swasta">Pegawai Swasta</label>
													<label class="checkbox-inline"><input type="radio" name="peker_ayah" class="peker_ayah1" onclick="javascript:as();" <?php if($profil['peker_ayah'] == "Wirausaha"){echo "checked";$o=1;} ?> value="Wirausaha">Wirausaha</label>
													<label class="checkbox-inline"><input type="radio" name="peker_ayah" class="peker_ayah1" onclick="javascript:as();" <?php if($profil['peker_ayah'] == "TNI/PORLI"){echo "checked";$o=1;} ?> value="TNI/PORLI">TNI/PORLI</label>
													<label class="checkbox-inline"><input type="radio" name="peker_ayah" id="peker_ayah2" onclick="javascript:as();" value="1" <?php if($o == 0){echo "checked";} ?> >Lainnya</label>
											  </div>
												<div class="form-group" id="ayah_lainnya">
												  <input type="text" class="form-control" name="pekeer_ayah" id="peker_ayah5" style="width:40vh;" placeholder="Lainnya" value="<?php if($o == 0){echo $profil['peker_ayah'];} ?>"  />
												</div>
											</div>
											<div class="form-group">
												<label for="nama" class="lb">Penghasilan Ayah</label>
												<input type="number" placeholder="Contoh 1000000" class="form-control" name="peng_ayah" id="peng_ayah" value="<?php echo $profil['peng_ayah'];?>" required  />

											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Nama Ibu
												&emsp;&emsp; 
													<span data-toggle="modal" data-target="#ortu_modal" style="color:#4398ed">(info)</span> 
												</label>
											  <input type="text" class="form-control" placeholder="Sri Qomary" id="nama_ibu" name="nama_ibu" value="<?php echo $profil['nama_ibu'];?>"  />
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Pekerjaan Ibu</label>
											  <div class="checkbox">
													<label class="checkbox-inline"><input type="radio" name="peker_ibu" onclick="javascript:ibu();" <?php if($profil['peker_ibu'] == "" || $profil['peker_ibu'] == "PNS"){echo "checked"; $p=1;} ?> value="PNS">PNS</label>
													<label class="checkbox-inline"><input type="radio" name="peker_ibu" onclick="javascript:ibu();" <?php if($profil['peker_ibu'] == "Pegawai Swasta"){echo "checked"; $p=1;} ?> value="Pegawai Swasta">Pegawai Swasta</label>
													<label class="checkbox-inline"><input type="radio" name="peker_ibu" onclick="javascript:ibu();" <?php if($profil['peker_ibu'] == "Wirausaha"){echo "checked"; $p=1;} ?> value="Wirausaha">Wirausaha</label>
													<label class="checkbox-inline"><input type="radio" name="peker_ibu" onclick="javascript:ibu();" <?php if($profil['peker_ibu'] == "TNI/PORLI"){echo "checked"; $p=1;} ?> value="TNI/PORLI">TNI/PORLI</label>
													<label class="checkbox-inline"><input type="radio" name="peker_ibu" id="peker_ibu" onclick="javascript:ibu();" value="1" <?php if($p == 0){echo "checked";} ?> >Lainnya</label>
											  </div>
												<div class="form-group" id="ibu_lainnya">
												  <input type="text" class="form-control" style="width:20vw;" placeholder="Lainnya" id="peker_ibu5" name="pekeer_ibu" value="<?php if($p == 0){echo $profil['peker_ibu'];} ?>" />
												</div>
											</div>
											<div class="form-group">
												<label for="nama" class="lb">Penghasilan Ibu</label>
												<input type="number" placeholder="Contoh 1000000" class="form-control" name="peng_ibu" id="peng_ibu" value="<?php echo $profil['peng_ibu'];?>" required />

											</div>

											<div class="form-group">
												<label for="nama" class="lb">Penghasilan Sendiri</label>
												<input type="number"  class="form-control" name="peng_sendiri" id="peng_sendiri" value="<?php echo $profil['peng_sendiri'];?>" required />

											</div>
											<div class="form-group">
												<label for="nama" class="lb">Alamat</label>
												<textarea  style="height:8vh;"class="form-control" name="alamat_ortu" ><?php echo $profil['alamat_ortu'];?></textarea>
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Jumlah Anggota KK</label>
											  <input type="number" class="form-control" id="saudara" name="saudara" value="<?php echo $profil['anggota'];?>" required />
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">No HP Orang Tua</label>
											  <input type="number" class="form-control" name="hp_ortu" value="<?php echo $profil['hp_ortu'];?>" required >
											</div>

											<!-- <a href="javascript:form2();" class="btn btn-success pull-right" >Simpan</a> -->
												<!-- <button type="submit" class="btn btn-success pull-right" >Simpan</button> -->
												<input type="submit" class="btn btn-success pull-right" value="Submit" >
											</form>
										</div>
                      <div role="tabpanel" class="tab-pane" id="messages">
												<div class="alert alert-success" role="alert" style="margin-top:-10px;">
													<span class="glyphicon glyphicon-info-sign" style="margin-right:1vw;"></span>
													Jika tidak mempunyai atau tidak menggunakan salah satu dibawah informasi silahkan diklik info untuk informasi per-field
												</div>
											<form action="<?php echo base_url()?>home/save3" method="post">
											<div class="form-group">
											  <label for="usr" class="lb">Pembayaran PBB/Tahun</label>
											  <div class="fc2 w">
												  <div class="form-group" >
												  <input type="text" class="form-control" style="width:50%;float:left;" placeholder="contoh 100000" id="pbb" style="width:50%;" name="pbb" value="<?php echo $profil['pbb'];?>" required />
												  <div style="margin-left:52%;padding-top:.5vw;font-weight:bold;">
														&emsp;&emsp;
														<span data-toggle="modal" data-target="#PBB_modal" style="color:#4398ed">(info)</span> 
													</div>
													</div>
											  </div>
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Daya PLN</label>
											  <div class="fc2 w">
												  <div class="form-group" >
												  <input type="text" class="form-control" style="width:50%;float:left;"id="pln" name="pln" value="<?php echo $profil['pln'];?>" placeholder="Daya PLN" required />
													<div style="margin-left:52%;padding-top:.5vw;font-weight:bold;">VA
														&emsp;&emsp; 
														<span data-toggle="modal" data-target="#PLN_modal" style="color:#4398ed">(info)</span> 
													</div>
												  </div>
											  </div>
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Luas Tanah</label>
											  <div class="fc2 w">
												  <div class="form-group" >
												  <input type="text" class="form-control" style="width:50%;float:left;" id="luas_tanah" name="luas_tanah" placeholder="Luas Tanah" value="<?php echo $profil['luas_tanah'];?>" required />
													<div style="margin-left:52%;padding-top:.5vw;font-weight:bold;">M<sup>2</sup>
													&emsp;&emsp; 
														<span data-toggle="modal" data-target="#luastanah_modal" style="color:#4398ed">(info)</span> 
													</div>
												  </div>
											   </div>
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Luas Bangunan</label>
												<div class="fc2 w">
												  <div class="form-group" >
													<input type="text" class="form-control" style="width:50%;float:left;" id="luas_bangunan" name="luas_bangunan" value="<?php echo $profil['luas_bangunan'];?>" placeholder="Luas Bangunan" required />
													<div style="margin-left:52%;padding-top:.5vw;font-weight:bold;">M<sup>2</sup>
														&emsp;&emsp; 
														<span data-toggle="modal" data-target="#luasbangunan_modal" style="color:#4398ed">(info)</span> 
													</div>
												  </div>
												</div>
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Pembayaran PDAM</label>
												<div class="fc2 w">
												  <div class="form-group" >
													<input type="text" class="form-control" id="pdam" style="width:50%;float:left;" name="pdam" value="<?php echo $profil['pdam'];?>" placeholder="contoh 100000" required />
														<div style="margin-left:52%;padding-top:.5vw;font-weight:bold;">/Bulan 
															&emsp;&emsp; 
															<span data-toggle="modal" data-target="#pdam_modal" style="color:#4398ed">(info)</span> 
														</div>
													</div>
												</div>
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Kepemilikan</label>
											  <div class="checkbox fc2">
													<label class="checkbox-inline"><input type="radio" name="kepemilikan" <?php if($profil['kepemilikan'] == "" || $profil['kepemilikan'] == "Sendiri"){echo "checked"; } ?> value="Sendiri" >Sendiri</label>
													<label class="checkbox-inline"><input type="radio" name="kepemilikan" <?php if($profil['kepemilikan'] == "Sewa"){echo "checked"; } ?> value="Sewa">Sewa</label>
													<label class="checkbox-inline"><input type="radio" name="kepemilikan" <?php if($profil['kepemilikan'] == "Menumpang"){echo "checked"; } ?> value="Menumpang">Menumpang</label>
													<label class="checkbox-inline"><input type="radio" name="kepemilikan" <?php if($profil['kepemilikan'] == "Tidak Memiliki"){echo "checked"; } ?> value="Tidak Memiliki">Tidak Memiliki</label>
											  </div>
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Sumber Listrik</label>
											  <div class="checkbox fc2" >
													<label class="checkbox-inline"><input type="radio" name="listrik" <?php if($profil['sum_listrik'] == "" || $profil['sum_listrik'] == "PLN"){echo "checked"; } ?> value="PLN">PLN</label>
													<label class="checkbox-inline"><input type="radio" name="listrik" <?php if($profil['sum_listrik'] == "Genset Mandiri"){echo "checked"; } ?> value="Genset Mandiri">Genset Mandiri</label>
													<label class="checkbox-inline"><input type="radio" name="listrik" <?php if($profil['sum_listrik'] == "PLN dan Genset"){echo "checked"; } ?>  value="PLN dan Genset">PLN dan Genset</label>
													<label class="checkbox-inline"><input type="radio" name="listrik" <?php if($profil['sum_listrik'] == "Tidak Ada"){echo "checked"; } ?>  value="Tidak Ada">Tidak Ada</label>
											  </div>
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Sumber Air</label>
											  <div class="checkbox fc2" >
													<label class="checkbox-inline"><input type="radio" name="sum_air" <?php if($profil['sum_air'] == "" || $profil['sum_air'] == "PDAM"){echo "checked"; } ?>  value="PDAM">PDAM</label>
													<label class="checkbox-inline"><input type="radio" name="sum_air" <?php if($profil['sum_air'] == "Kemasan"){echo "checked"; } ?> value="Kemasan">Kemasan</label>
													<label class="checkbox-inline"><input type="radio" name="sum_air" <?php if($profil['sum_air'] == "Sumur"){echo "checked"; } ?> value="Sumur">Sumur</label>
													<label class="checkbox-inline"><input type="radio" name="sum_air" <?php if($profil['sum_air'] == "Sungai/Mata Air Gunung"){echo "checked"; } ?> value="Sungai/Mata Air Gunung">Sungai/Mata Air Gunung</label>
											  </div>
											</div>

											<button type="submit" class="btn btn-success pull-right" >Simpan</button>
											</form>
										</div>
                    <div role="tabpanel" class="tab-pane" id="settings">
										<form action="<?php echo base_url()?>home/upload" method="post" id="upload_all" enctype="multipart/form-data">
											<input type="hidden" name="pilih_ukt_" id="pilih_ukt_" value="<?php echo $profil['pilih_ukt']; ?>">
											<div class="form-group v col-xs-12 col-sm-12 col-md-12">
											  <label for="usr" class="lb" style="height:6vh;">Upload Foto </br>
													<p style="color:blue;font-size:1.9vh">Max 1Mb<br> <span  style="color:red;font-size:1.9vh">(Upload jpg/png)</span></p>
													</label>
													<div id="upload_foto" class="input-group image-preview preview fc3">
														<input type="file" accept="image/png, image/jpeg, image/gif" name="foto" id="foto" /> 
													</div>
													<div id="foto_foto" class="f">
														<img class="zoom" src="<?php echo base_url();?>assets/foto/<?php echo $profil['foto'];?>"/>
														<a href="javascript:ubah('upload_foto','foto_foto');"  class="btn btn-warning n">Ubah Foto</a>
													</div>
											</div>
											<div class="form-group col-xs-12 col-sm-12 col-md-12" >
													<label for="usr" class="lb" style="height:6vh;">Kartu KKS
														&emsp;&emsp; 
														<span data-toggle="modal" data-target="#KKS_modal" style="color:#4398ed">(info)</span>
													<br>
													<p style="color:blue;font-size:1.9vh">Max 1Mb<br> <span  style="color:red;font-size:1.9vh">(Upload jpg/png)</span></p>
													</label>
													<div id="upload_kjs1" class="input-group image-preview preview1 fc3">
														<input type="file" accept="image/png, image/jpeg, image/gif" name="kjs" id="kjs"  />
													</div>
													<div id="foto_kjs" class="f">
														<img class="zoom" src="<?php echo base_url();?>assets/foto/<?php echo $profil['upload_kjs'];?>"/>
														<a href="javascript:ubah('upload_kjs1','foto_kjs');" class="btn btn-warning n">Ubah Foto</a>
													</div>

												</div>
											<div class="form-group col-xs-12 col-sm-12 col-md-12">
											  <label for="usr" class="lb"style="height:6vh;">Bukti Pembayaran PBB 
												&emsp;&emsp; 
														<span data-toggle="modal" data-target="#PBB_modal" style="color:#4398ed">(info)</span>
												<br>
											  <p style="color:blue;font-size:1.9vh">Max 1Mb<br>
											  <span style="color:red;font-size:1.9vh">(Upload jpg/png)</span></p>
													</label>
												<div id="upload_pbb"  class="input-group image-preview preview1 fc3">
													<input type="file" accept="image/png, image/jpeg, image/gif" name="pbb" id="pbb" /> 
												</div>
												<div id="foto_pbb" class="f">
														<img class="zoom" src="<?php echo base_url();?>assets/foto/<?php echo $profil['upload_pbb'];?>"/>
														<a href="javascript:ubah('upload_pbb','foto_pbb');" class="btn btn-warning n">Ubah Foto</a>
													</div>
											</div>
											<div class="form-group col-xs-12 col-sm-12 col-md-12">
											  <label for="usr" class="lb" style="height:6vh;">STNK / BPKB</br>
											  <p style="color:blue;font-size:1.9vh">Max 1Mb<br>
											  <span style="color:red;font-size:1.9vh">(Upload jpg/png)</span></p>
													</label>
												<div id="upload_stnk"  class="input-group image-preview preview1 fc3">
													<input type="file" accept="image/png, image/jpeg, image/gif" name="stnk" id="stnk" /> 
												</div>
													<div id="foto_stnk" class="f">
														<img class="zoom" src="<?php echo base_url();?>assets/foto/<?php echo $profil['upload_stnk'];?>"/>
														<a href="javascript:ubah('upload_stnk','foto_stnk');" class="btn btn-warning n">Ubah Foto</a>
													</div>
											</div>
											<div class="form-group col-xs-12 col-sm-12 col-md-12">
											  <label for="usr" class="lb" style="height:6vh;">Foto Atap Rumah</br>
											  <p style="color:blue;font-size:1.9vh">Max 1Mb<br>
											  <span style="color:red;font-size:1.9vh">(Upload jpg/png)</span></p>
													</label>
												<div id="upload_atap" class="input-group image-preview preview1 fc3">

													<input type="file" accept="image/png, image/jpeg, image/gif" name="atap_rumah" id="atap_rumah" /> 
												</div>
													<div id="foto_atap" class="f">
														<img class="zoom" src="<?php echo base_url();?>assets/foto/<?php echo $profil['upload_atap'];?>"/>
														<a href="javascript:ubah('upload_atap','foto_atap');" class="btn btn-warning n">Ubah Foto</a>
													</div>
											</div>
											<div class="form-group col-xs-12 col-sm-12 col-md-12">
											  <label for="usr" class="lb" style="height:6vh;">Foto Lantai Rumah</br>
											  <p style="color:blue;font-size:1.9vh">Max 1Mb<br>
											  <span style="color:red;font-size:1.9vh">(Upload jpg/png)</span></p>
													</label>
													<div id="upload_lantai"  class="input-group image-preview preview1 fc3">

														<input type="file" accept="image/png, image/jpeg, image/gif" name="lantai_rumah" id="lantai_rumah" /> 
													</div>
													<div id="foto_lantai" class="f">
														<img class="zoom" src="<?php echo base_url();?>assets/foto/<?php echo $profil['upload_lantai'];?>"/>
														<a href="javascript:ubah('upload_lantai','foto_lantai');" class="btn btn-warning n">Ubah Foto</a>
													</div>
											</div>
											<div class="form-group col-xs-12 col-sm-12 col-md-12">
											  <label for="usr" class="lb" style="height:6vh;">Bukti Pembayaran PLN</br>
											  <p style="color:blue;font-size:1.9vh">Max 1Mb<br>
											  <span style="color:red;font-size:1.9vh">(Upload jpg/png)</span></p>
													</label>
													<div id="upload_pln"  class="input-group image-preview preview1 fc3">
														<input type="file" accept="image/png, image/jpeg, image/gif" name="pln" id="pln"/> 
													</div>
													<div id="foto_pln" class="f">
														<img class="zoom" src="<?php echo base_url();?>assets/foto/<?php echo $profil['upload_pln'];?>" />
														<a href="javascript:ubah('upload_pln','foto_pln');" class="btn btn-warning n">Ubah Foto</a>
													</div>
											</div>
											<div class="form-group col-xs-12 col-sm-12 col-md-12">
											  <label for="usr" class="lb" style="height:6vh;">Bukti Pembayaran PDAM</br>
											  <p style="color:blue;font-size:1.9vh">Max 1Mb<br>
											  <span style="color:red;font-size:1.9vh">(Upload jpg/png)</span></p>
													</label>
													<div id="upload_pdam"  class="input-group image-preview preview1 fc3">
														<input type="file" accept="image/png, image/jpeg, image/gif" name="pdam" id="pdam" /> 
													</div>
													<div id="foto_pdam" class="f">
														<img class="zoom" src="<?php echo base_url();?>assets/foto/<?php echo $profil['upload_pdam'];?>"/>
														<a href="javascript:ubah('upload_pdam','foto_pdam');" class="btn btn-warning n">Ubah Foto</a>
													</div>
											</div>
											<div class="form-group col-xs-12 col-sm-12 col-md-12">
											  <label for="usr" class="lb" style="height:6vh;">Bukti Ket Penghasilan/Gaji Ayah</br>
											  <p style="color:blue;font-size:1.9vh">Max 1Mb<br>
											  <span style="color:red;font-size:1.9vh">(Upload jpg/png)</span></p>
													</label>
													<div id="upload_gaji"  class="input-group image-preview preview1 fc3">
														<input type="file" accept="image/png, image/jpeg, image/gif" name="gaji" id="gaji_ayah" /> 
													</div>
													<div id="foto_gaji" class="f">
														<img class="zoom" src="<?php echo base_url();?>assets/foto/<?php echo $profil['upload_gaji_ayah'];?>"/>
														<a href="javascript:ubah('upload_gaji','foto_gaji');" class="btn btn-warning n">Ubah Foto</a>
													</div>
											</div>
											<div class="form-group col-xs-12 col-sm-12 col-md-12">
											  <label for="usr" class="lb" style="height:6vh;">Bukti Ket Penghasilan/Gaji Ibu</br>
											  <p style="color:blue;font-size:1.9vh">Max 1Mb<br>
											  <span style="color:red;font-size:1.9vh">(Upload jpg/png)</span></p>
													</label>
													<div id="upload_gaji_ibu"  class="input-group image-preview preview1 fc3">
														<input type="file" accept="image/png, image/jpeg, image/gif" name="gaji_ibu" id="gaji_ibu" /> 
													</div>
													<div id="foto_gaji_ibu" class="f">
														<img class="zoom" src="<?php echo base_url();?>assets/foto/<?php echo $profil['upload_gaji_ibu'];?>"/>
														<a href="javascript:ubah('upload_gaji_ibu','foto_gaji_ibu');" class="btn btn-warning n">Ubah Foto</a>
													</div>
											</div>
											<div class="form-group col-xs-12 col-sm-12 col-md-12">
											  <label for="usr" class="lb" style="height:6vh;">Kartu Keluarga</br>
											  <p style="color:blue;font-size:1.9vh">Max 1Mb<br>
											  <span style="color:red;font-size:1.9vh">(Upload jpg/png)</span></p>
													</label>
													<div id="upload_kk"  class="input-group image-preview preview1 fc3">
														<input type="file" accept="image/png, image/jpeg, image/gif" name="kk" id="kk" /> 
													</div>
													<div id="foto_kk" class="f">
														<img class="zoom" src="<?php echo base_url();?>assets/foto/<?php echo $profil['upload_kk'];?>"/>
														<a href="javascript:ubah('upload_kk','foto_kk');" class="btn btn-warning n">Ubah Foto</a>
													</div>
											</div>
											<div class="form-group col-xs-12 col-sm-12 col-md-12">
											  <label for="usr" class="lb" style="height:6vh;">Depan Rumah</br>
											  <p style="color:blue;font-size:1.9vh">Max 1Mb<br>
											  <span style="color:red;font-size:1.9vh">(Upload jpg/png)</span></p>
													</label>
													<div id="upload_rmh"  class="input-group image-preview preview1 fc3">
														<input type="file" accept="image/png, image/jpeg, image/gif" name="rmh" id="rmh" /> 
													</div>
													<div id="foto_rmh" class="f">
														<img class="zoom" src="<?php echo base_url();?>assets/foto/<?php echo $profil['upload_rmh'];?>"/>
														<a href="javascript:ubah('upload_rmh','foto_rmh');" class="btn btn-warning n">Ubah Foto</a>
													</div>
											</div>
											<div class="form-group col-xs-12 col-sm-12 col-md-12">
											  <label for="usr" class="lb" style="height:6vh;">Upload Surat Pernyataan <br> Kebenaran data</br>
											  <p style="color:blue;font-size:1.9vh">Max 1Mb<br>
											  <span style="color:red;font-size:1.9vh">(Upload pdf)</span></p>
											  <a href="<?php echo base_url();?>assets/document/SURAT PERNYATAAN KEBENARAN DATA.docx">Download Template</a></p>
													</label>
													<div id="upload_datavalid"  class="input-group image-preview preview1 fc3">
														<input type="file" accept="application/pdf" name="upload_datavalid" id="upload_datavalid" />
													</div>
													<div id="foto_datavalid" class="f">
														<a href="<?php echo base_url();?>assets/foto/<?php echo $profil['upload_datavalid'];?>">Klik disini</a>
														<a href="javascript:ubah('upload_datavalid','foto_datavalid');" class="btn btn-warning n">Ubah Dokument</a>
													</div>
											</div>
												<button type="submit" class="btn btn-success pull-right" >Simpan</button>
											</form>
										</div>
                                    </div>
								</div>
								</div>
							</div>
						</div>
					</div>
				</div>
		</div>

	</div>


  </body>
  <script>
	var base_url = '<?php echo base_url();?>';

function cek(){
	var simpan = $('#provinsi').val();
	$('#kota').load("<?php echo base_url(); ?>home/getKota/"+simpan);
}
$(function() {
	cekawal(); // memanggil function mengcek apakah harus ada file upload ada atau tidak
	var ayah = $("#peker_ayah5").val();
	var ibu = $("#peker_ibu5").val();
	if(ayah == ""){
		$("#ayah_lainnya").hide();
	}
	if(ibu == ""){
		$("#ibu_lainnya").hide();
	}

});

function ubah(nama,nama_foto){
	var upload = "#"+nama;
	var foto = "#"+nama_foto;
	$(foto).hide();
	$(upload).show();
}

function as(){
	if (document.getElementById('peker_ayah2').checked) {
		$("#ayah_lainnya").show();
	}else {
		$("#ayah_lainnya").hide();
		$("#peker_ayah5").val("");
	}
}

function ibu(){
	if (document.getElementById('peker_ibu').checked) {
		$("#ibu_lainnya").show();
	}else {
		$("#ibu_lainnya").hide();
		$("#peker_ibu5").val("");
	}
}

function cek_finalisasi(){
	let pbb = '<?php echo $profil['upload_pbb'];?>'
	let pln = '<?php echo $profil['upload_pln'];?>'
	let pdam = '<?php echo $profil['upload_pdam'];?>'
	let gaji_ayah = '<?php echo $profil['upload_gaji_ayah'];?>'
	let gaji_ibu = '<?php echo $profil['upload_gaji_ibu'];?>'
	let validasi_data = '<?php echo $profil['upload_datavalid'];?>'
	let kk = '<?php echo $profil['upload_kk'];?>'

	if(kk == ''){
		alert ("File Upload Kartu Keluarga Belum diupload")
	}else if (validasi_data == '') {
		alert ("File Upload Surat Pernyataan Kebenaran data Belum diupload")
	}else if (gaji_ayah == '') {
		alert ("File Upload gaji Ayah Belum diupload")
	}else if(gaji_ibu == ''){
		alert ("File upload Gaji Ibu Belum diupload");
	}else if (pdam == '') {
		alert ("File upload PDAM Belum diupload");
	}else if (pln == '') {
		alert ("File upload PLN Belum diupload");
	}else if (pbb == '') {
		alert ("File upload PBB Belum diupload");
	}else {
		var pilih = confirm("Apakah Anda Sudah Yakin akan Finalisasi ?");
		if(pilih){
			$.ajax({
				type:"POST",
				url:base_url+"finalisasi/mhs_finalisasi",
				data:"status=1",
				success: function(kembali){
					if(kembali > 0){
						alert ('Finalisasi Sukses Silahkan Tunggu Pengumuman Selanjutnya');
					}else {
						alert ('Ada Kesalahan Silahkan Finalisasi Ulang');
					}
					location.href=base_url;

				}
			});
		}
	}

}


// function untuk melihat apakah ada hasilnya atau tidak
function cekawal(){
	    <?php
		if($profil['upload_pbb'] == ""){ ?>
			$('#foto_pbb').hide();
			$("#upload_pbb").show();
		<?php
		}else { ?>
			$('#foto_pbb').show();
			$("#upload_pbb").hide();
		<?php
		}
		if($profil['upload_stnk'] == ""){ ?>
			$('#foto_stnk').hide();
			$("#upload_stnk").show();
		<?php
		}else { ?>
		$('#foto_stnk').show();
		$("#upload_stnk").hide();
		<?php
		}
		if($profil['upload_atap'] == ""){ ?>
		$('#foto_atap').hide();
		$("#upload_atap").show();
		<?php
		}else { ?>
		$('#foto_atap').show();
		$("#upload_atap").hide();
		<?php
		}
		if($profil['upload_lantai'] == ""){ ?>
		$('#foto_lantai').hide();
		$("#upload_lantai").show();
		<?php
		}else { ?>
		$('#foto_lantai').show();
		$("#upload_lantai").hide();
		<?php
		}
		if($profil['upload_pln'] == ""){ ?>
		$('#foto_pln').hide();
		$("#upload_pln").show();
		<?php
		}else { ?>
		$('#foto_pln').show();
		$("#upload_pln").hide();
		<?php
		}
		if($profil['upload_pdam'] == ""){ ?>
		$('#foto_pdam').hide();
		$("#upload_pdam").show();
		<?php
		}else { ?>
		$('#foto_pdam').show();
		$("#upload_pdam").hide();
		<?php
		}
		if($profil['upload_gaji_ayah'] == ""){ ?>
		$('#foto_gaji').hide();
		$("#upload_gaji").show();
		<?php
		}else { ?>
		$('#foto_gaji').show();
		$("#upload_gaji").hide();
		<?php
		}
		if($profil['upload_gaji_ibu'] == ""){ ?>
		$('#foto_gaji_ibu').hide();
		$("#upload_gaji_ibu").show();
		<?php
		}else { ?>
		$('#foto_gaji_ibu').show();
		$("#upload_gaji_ibu").hide();
		<?php
		}
			if($profil['upload_kjs'] == "1" || $profil['upload_kjs'] == ""){ ?>
			$('#foto_kjs').hide();
			$("#upload_kjs1").show();
		<?php
		}else { ?>
			$('#foto_kjs').show();
			$("#upload_kjs1").hide();
		<?php
		}
			if($profil['upload_alat_kom'] == "1" || $profil['upload_alat_kom'] == ""){ ?>
			$('#foto_alat_kom').hide();
			$("#upload_alat_kom").show();
		<?php
		}else { ?>
			$('#foto_alat_kom').show();
			$("#upload_alat_kom").hide();
		<?php
		}
		if($profil['upload_kk'] == ""){ ?>
		$('#foto_kk').hide();
		$("#upload_kk").show();
		<?php
		}else { ?>
		$('#foto_kk').show();
		$("#upload_kk").hide();
		<?php
		}
		if($profil['upload_rmh'] == ""){ ?>
		$('#foto_rmh').hide();
		$("#upload_rmh").show();
		<?php
		}else { ?>
		$('#foto_rmh').show();
		$("#upload_rmh").hide();
		<?php
		}
		if($profil['foto'] == "" ){ ?>
		$('#foto_foto').hide();
		$("#upload_foto").show();
		<?php
		}else { ?>
			$('#foto_foto').show();
			$("#upload_foto").hide();
		<?php
		}
		if($profil['upload_srt_mrk'] == "" ){ ?>
			$('#foto_srt_mrk').hide();
			$("#upload_srt_mrk").show();
		<?php
		}else { ?>
			$('#foto_srt_mrk').show();
			$("#upload_srt_mrk").hide();
		<?php
		}
		if($profil['upload_penyakit'] == "" ){ ?>
			$('#foto_penyakit').hide();
			$("#upload_penyakit").show();
		<?php
		}else { ?>
			$('#foto_penyakit').show();
			$("#upload_penyakit").hide();
		<?php
		}
		if($profil['upload_panti_yatim'] == "" ){ ?>
			$('#foto_panti_yatim').hide();
			$("#upload_panti_yatim").show();
		<?php
		}else { ?>
			$('#foto_panti_yatim').show();
			$("#upload_panti_yatim").hide();
		<?php
		}
		if($profil['upload_tetangga'] == "" ){ ?>
			$('#foto_tetangga').hide();
			$("#upload_tetangga").show();
		<?php
		}else { ?>
			$('#foto_tetangga').show();
			$("#upload_tetangga").hide();
		<?php
		}
		if($profil['upload_surat_asli'] == "" ){ ?>
			$('#foto_surat_asli').hide();
			$("#upload_surat_asli").show();
		<?php
		}else { ?>
			$('#foto_surat_asli').show();
			$("#upload_surat_asli").hide();
		<?php
		}
		if($profil['upload_sktm'] == "" ){ ?>
			$('#foto_sktm').hide();
			$("#upload_sktm").show();
		<?php
		}else { ?>
			$('#foto_sktm').show();
			$("#upload_sktm").hide();
		<?php
		}
		if($profil['upload_anakkuliah'] == "" ){ ?>
			$('#foto_anakkuliah').hide();
			$("#upload_anakkuliah").show();
		<?php
		}else { ?>
			$('#foto_anakkuliah').show();
			$("#upload_anakkuliah").hide();
		<?php
		}
		if($profil['upload_tetangga'] == "" ){ ?>
			$('#foto_tetangga2').hide();
			$("#upload_tetangga2").show();
		<?php
		}else { ?>
			$('#foto_tetangga2').show();
			$("#upload_tetangga2").hide();
		<?php
		}
		if($profil['upload_tetangga'] == "" ){ ?>
			$('#foto_tetangga3').hide();
			$("#upload_tetangga3").show();
		<?php
		}else { ?>
			$('#foto_tetangga3').show();
			$("#upload_tetangga3").hide();
		<?php
		}
		if($profil['upload_skl'] == "" ){ ?>
			$('#foto_skl').hide();
			$("#upload_skl").show();
		<?php
		}else { ?>
			$('#foto_skl').show();
			$("#upload_skl").hide();
		<?php
		}
		if($profil['upload_datavalid'] == "" ){ ?>
			$('#foto_datavalid').hide();
			$("#upload_datavalid").show();
		<?php
		}else { ?>
			$('#foto_datavalid').show();
			$("#upload_datavalid").hide();
		<?php
		}
		?>
}


function form2(){

	let cek = 1;
	let peng_ayah = $("#peng_ayah").val()
	let peng_ibu = $("#peng_ibu").val()
	if(peng_ayah > 0 && peng_ayah < 100000){
		cek = 0
	}
	if(peng_ibu > 0 && peng_ibu < 100000){
		cek = 0
	}
	if(cek == 0){
		alert ("Penghasilan boleh 0 atau harus diatas 100000");
	}else {
		$("#formkeluarga").submit();
	}

}
  </script>
 </html>
