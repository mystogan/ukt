<?php
foreach($profile as $profil);
 ?>

<html>
  <head><meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Pengajuan UKT</title>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>assets/images/uin.png" />

  	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/reset.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/css.css" type="text/css "/>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/proses.js"></script>
	   <!-- CKeditor CSS -->
	    <script src="<?php echo base_url();?>assets/js/ckeditor.js"></script>

  </head>
  <body>
	<div class="header">
		<div class="container">
				<div class="col-xs-12 col-sm-12 col-md-12" >
					<div class="row">
						<div class="head">
							FORM PENGAJUAN UKT</br> Mahasiswa UIN Sunan Ampel Surabaya
						</div>
						<div class="kotak">
							<div class="kanan">
								<div class="foto"><img src="<?php echo base_url();?>assets/foto/<?php echo $profil['foto'];?>"/></div>
								<div class="tempatNama">
									<div class="nama1">Nama :</div>
									<div class="nama"><?php echo $profil['nama'];?></div>
								</div>
								<div class="tempatNama">
									<div class="nama1">Kode :</div>
									<div class="nama"><?php echo $profil['kode'];?></div>
								</div>
								<div class="tempatNama">
									<div class="nama1">Jenis Kelamin :</div>
									<div class="nama"><?php
									if($profil['jk'] == "L"){
										echo "Laki-Laki";
									}else {
										echo "Perempuan";
									}
									?></div>
								</div>
								<div class="tempatNama">
									<div class="nama1">Program Studi :</div>
									<div class="nama"><?php echo $profil['nama_prodi'];?></div>
								</div>
								<div class="tempatNama">
									<div class="nama1">Tahun Masuk :</div>
									<div class="nama"><?php echo $profil['tahun_masuk'];?></div>
								</div>
								<div class="tempatNama">
									<a href="<?php echo base_url()?>home/logout" class="btn btn-danger" style="margin-top:2vw;">Logout</a>
								</div>

                <?php
                if( $profil['mhs_finalisasi'] == 0){ ?>


								<div class="tempatNama">
                  <hr>
                  <hr>
                </div>
								<div class="tempatNama" class="alert alert-danger" role="alert">
                  Pastikan Data yang akan diisi benar dan Valid Sebelum Finalisasi<br/>

									<a href="javascript:cek_finalisasi();" class="btn btn-info" style="margin-top:2vw;">Finalisasi</a>
								</div>
                <?php
                }
                ?>

							</div>
