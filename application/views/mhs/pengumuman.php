<?php
error_reporting(0);
 foreach($profile as $profil);
 $user = $this->session->userdata('kode');
 $o=0;
 $p=0;


 ?>

<html>
  <head><meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Pengajuan UKT</title>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>assets/images/uin.png" />

  	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/reset.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/cetak.css" type="text/css "/>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/proses.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	<!-- CKeditor CSS -->
	<script src="<?php echo base_url();?>assets/js/ckeditor.js"></script>

  </head>
  <body>
	<div class="header">
		<div class="container">
				<div class="col-xs-12 col-sm-12 col-md-12" >
					<div class="row">
						<div class="head">
							FORM PENGAJUAN UKT</br> Mahasiswa UIN Sunan Ampel Surabaya
						</div>
						<div class="kotak">
							<div class="kanan">
								<div class="foto"><img src="<?php echo base_url();?>assets/foto/<?php echo $profil['foto'];?>"/></div>
								<div class="tempatNama">
									<div class="nama1">Nama :</div>
									<div class="nama"><?php echo $profil['nama'];?></div>

								</div>
								<div class="tempatNama">
									<div class="nama1">Kode :</div>
									<div class="nama"><?php echo $profil['kode'];?></div>

								</div>
								<div class="tempatNama">
									<div class="nama1">Jenis Kelamin :</div>
									<div class="nama"><?php
									if($profil['jk'] == "L"){
										echo "Laki-Laki";
									}else {
										echo "Perempuan";
									}
									?></div>
								</div>
								<div class="tempatNama">
									<div class="nama1">Program Studi :</div>
									<div class="nama"><?php echo $profil['nama_prodi'];?></div>
								</div>
								<div class="tempatNama">
									<div class="nama1">Tahun Masuk :</div>
									<div class="nama"><?php echo $profil['tahun_masuk'];?></div>
								</div>
								<div class="tempatNama">
									<a href="<?php echo base_url()?>home/logout" class="btn btn-danger" style="margin-top:2vw;font-size:1.5vw;">Logout</a>
								</div>
							</div>
							<div class="kiri">
								<div class="col-xs-12 col-sm-12 col-md-12 ">
								<div class="kotJ">
									<!--<a href="<?php echo base_url()?>home/logout" class="hp3" >Logout</a>-->
									<div class="jud col-xs-12 col-sm-12 col-md-12 ">
										<div class="jud1">
											<?php echo date("d-m-Y");?>

										</div>
									</div>
									<div class="jud col-xs-12 col-sm-12 col-md-12 ">
											<div class="jud">
												PENGUMUMAN
											</div>
									</div>
								</div>
								<div class="content">
								<div class="alert alert-danger" role="alert">
										<span class="glyphicon glyphicon-info-sign" style="margin-right:1vw;"></span> - Untuk download Template Silahkan menggunakan Komputer atau laptop<br/>
									</div>
									<?php 
									if ($profil['ver_rapor'] == 1 && $profil['ver_kesanggupan'] == 1) {
										echo '<span class="btn btn-success" style="margin-top:2vw;font-size:1.5vw;">Terverifikasi</span>';
									}else {
										echo '<span class="btn btn-danger" style="margin-top:2vw;font-size:1.5vw;">Belum Terverifikasi</span>';
									}
									?>
									
									<br>
									<br>
									  <!-- Nav tabs -->
									<div class="card">
									
									
										<ul class="nav nav-tabs" style="" role="tablist">
											<li role="presentation" class="active"><a href="#home" data-toggle="tab" aria-controls="home" role="tab" >Pengumuman</a></li>
											<li role="presentation" ><a href="#profile" data-toggle="tab" aria-controls="profile" role="tab" >Upload</a></li>
										</ul>
										<div class="tab-content" style="border:1px solid #2b975b;padding-bottom:8vh; border-top:none;border-radius:5px;">
										<div role="tabpanel" class="tab-pane active" id="home" >
										<div class="content" style="padding:3vw;">
													<div class="textpeng">Hasil verifikasi pengajuan UKT Mahasiswa sebesar</div>
													<div class="nominal"> Rp <?php echo number_format($ukt['hasil']);?> </div>
													<div class="ketpeng">
													<?php 
													if ($profil['jalur'] == 1) { 
														
														$pengumuman1 = '8 - 9 April 2019';
														$pengumuman2 = '9 - 10 April 2019';
														$pengumuman3 = '10 sampai 12 April 2019';

													}else if ($profil['jalur'] == 2) { 

														$pengumuman1 = '8 - 9 Juli 2019';
														$pengumuman2 = '9 - 10 Juli 2019';
														$pengumuman3 = '10 sampai 12 Juli 2019';

													}else if ($profil['jalur'] == 3) { 
														
														$pengumuman1 = '11 - 12 April 2019';
														$pengumuman2 = '12 - 15 April 2019';
														$pengumuman3 = '15,16 dan 18 April 2019';

													}else if ($profil['jalur'] == 4) { 

														$pengumuman1 = '8 - 9 Juli 2019';
														$pengumuman2 = '9 - 10 Juli 2019';
														$pengumuman3 = '10 sampai 12 Juli 2019';

													} ?>
														<div class="form-group">
															<h4>1.	Unggah copy raport semester 1 s/d semester 5 dalam 
															bentuk PDF pada tanggal <?php echo $pengumuman1; ?> pada laman ukt.uinsby.ac.id</h4>
														</div>
														<div class="form-group">
															<h4>2.	a. 	Verifikasi faktual dengan menunjukkan raport asli bagi Calon Mahasiswa Baru (CAMABA) yang berasal dari wilayah Surabaya, Gresik, Sidoarjo, Bangkalan, Mojokerto, Lamongan (Gerbang Kertasusila), Jombang, dan Pasuruan pada tanggal <?php echo $pengumuman2; ?> di Ruang Layanan Terpadu Twin Tower B Lantai 1.
																	<br>&emsp; b.	Bagi CAMABA yang berasal dari selain wilayah tersebut pada poin 2.a. di atas diwajibkan untuk menunjukkan raport asli pada hari pertama perkuliahan.
																	</h4>
														</div>
														<div class="form-group">
															<h4>3.	Unggah Surat Pernyataan Kesanggupan Membayar UKT <a target="_blank" href="<?php echo base_url();?>prin/cekprinttemplate/<?php echo $profil['kode'];?>">(klik di sini template-nya)</a> 
																	ke laman ukt.uinsby.ac.id tanggal <?php echo $pengumuman1; ?>
																	</h4>
														</div>
														<div class="form-group">
															<h4>4.	Membayar Uang Kuliah Tunggal (UKT) dengan nominal yang telah ditentukan di seluruh 
																	Cabang Bank BTN pada tanggal <?php echo $pengumuman3; ?>
																	setelah dokumen raport dan Surat Pernyataan Kasanggunapan 
																	Membayar UKT di verifikasi.</h4>
														</div>
														<div>
															<p style="font-weight:bold;padding-top:2vw;">Registrasi dilaksanakan tanggal <?php echo $pengumuman3; ?></p>
														</div>
													</div>
												</div>
										</div>
										<div role="tabpanel" class="tab-pane" id="profile">
										<form action="<?php echo base_url()?>home/upload" method="post" id="upload_all" enctype="multipart/form-data">
											<input type="hidden" name="pilih_ukt_" id="pilih_ukt_" value="<?php echo $profil['pilih_ukt']; ?>">
											<div class="form-group v col-xs-12 col-sm-12 col-md-12">
											  <label for="usr" class="lb" style="height:6vh;">Upload Rapor </br>
													<p style="color:blue;font-size:1.9vh">Max 1Mb<br> <span  style="color:red;font-size:1.9vh">(Upload pdf/jpg)</span></p>
													</label>
													<div id="upload_rapor" class="input-group image-preview preview fc3">
														<input type="file" name="rapor" id="rapor" /> 
													</div>
													<div id="foto_rapor" class="f">
														<a href="<?php echo base_url();?>assets/foto/<?php echo $profil['upload_rapor'];?>">Klik disini</a>
														<a href="javascript:ubah('upload_rapor','foto_rapor');"  class="btn btn-warning n">Ubah Upload</a>
													</div>
											</div>
											<div class="form-group v col-xs-12 col-sm-12 col-md-12">
											  <label for="usr" class="lb" style="height:6vh;">Upload Surat Pernyataan </br>
													<p style="color:blue;font-size:1.9vh">Max 1Mb<br> <span  style="color:red;font-size:1.9vh">(Upload pdf/jpg)</span></p>
													</label>
													<div id="upload_kesanggupan" class="input-group image-preview preview fc3">
														<input type="file" name="kesanggupan" id="kesanggupan" /> 
													</div>
													<div id="foto_kesanggupan" class="f">
													<a href="<?php echo base_url();?>assets/foto/<?php echo $profil['upload_kesanggupan'];?>">Klik disini</a>
														<a href="javascript:ubah('upload_kesanggupan','foto_kesanggupan');"  class="btn btn-warning n">Ubah Upload</a>
													</div>
											</div>
												<button type="submit" class="btn btn-success pull-right" >Simpan</button>
											</form>
										
										</div>
                  </div>
								</div>
								
							</div>
						</div>
				</div>
		</div>

	<!--div class="footer">

	</div>-->
	</div>
  </body>
  <script>
		cekawal();
   $(document).on('click', '#close-preview', function(){
    $('.image-preview').popover('hide');
    // Hover befor close the preview
});

function ubah(nama,nama_foto){
	var upload = "#"+nama;
	var foto = "#"+nama_foto;
	$(foto).hide();
	$(upload).show();
}

// function untuk melihat apakah ada hasilnya atau tidak
function cekawal(){
	    <?php
		if($profil['upload_rapor'] == ""){ ?>
			$('#foto_rapor').hide();
			$("#upload_rapor").show();
		<?php
		}else { ?>
			$('#foto_rapor').show();
			$("#upload_rapor").hide();
		<?php
		}
		if($profil['upload_kesanggupan'] == ""){ ?>
			$('#foto_kesanggupan').hide();
			$("#upload_kesanggupan").show();
		<?php
		}else { ?>
			$('#foto_kesanggupan').show();
			$("#upload_kesanggupan").hide();
		<?php
		}
		?>
}

  </script>
 </html>
