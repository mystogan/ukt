

<!-- start modal -->
<div id="pdam_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">Informasi PDAM</h5>
      </div>
      <div class="modal-body" id="isimodal">
				<div class="accordion">
					<div class="accordion-item">
						<a>Bagaimana kalau saya tidak menggunakan PDAM ?</a>
						<div class="content">
							<span>untuk form PDAMnya bisa diisi dengan "0" dengan melampirkan (Opsi bisa pilih salah satu)</span>
							<ul>
								<li>1. Melampirkan Surat pernyataan dari desa/kelurahan jika tidak menggunakan PDAM </li>
								<li>2. Membuat Surat Pernyataan bahwa tidak menggunakan PDAM <button class="btn btn-success btn-xs" onclick="javascript:downloadsp();">contohnya</button> </li>
							</ul>
						</div>
					</div>
					<div class="accordion-item">
						<a>Bagaimana kalau pembayaran PDAM gabung dengan tetangga ?</a>
						<div class="content">
							<span>Untuk Form PDAMnya diisi sesuai pembayaran dengan melampirkan (Opsi bisa pilih salah satu)</span>
							<ul>
								<li>1. Melampirkan Surat pernyataan dari desa/kelurahan jika pembayaran gabung dengan tetangga dan disertai dengan jumlah pembayaran </li>
								<li>2. Membuat Surat Pernyataan bahwa pembayaran gabung dengan tetangga dan disertai dengan jumlah pembayaran <button class="btn btn-success btn-xs" onclick="javascript:downloadsp();">contohnya</button> </li>
							</ul>
						</div>
					</div>
					<div class="accordion-item">
						<a>Bagaimana jika saya bertempat tinggal di Panti Asuhan ?</a>
						<div class="content">
							<ul>
								<li>untuk form PDAMnya bisa diisi dengan "0" dengan melampirkan Surat pernyataan dari desa/kelurahan jika tinggal dipanti asuhan</li>
							</ul>
						</div>
					</div>
				</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div id="luasbangunan_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">Informasi Luas Bangunan</h5>
      </div>
      <div class="modal-body" id="isimodal">
				<div class="accordion">
					<div class="accordion-item">
						<a>Bagaimana kalau saya masih menyewa rumah/KOS ?</a>
						<div class="content">
							<span>untuk form Luas Bangunannya bisa diisi dengan "0" dengan melampirkan pada PBB (Opsi bisa pilih salah satu)</span>
							<ul>
								<li>1. Melampirkan Surat pernyataan dari desa/kelurahan jika masih bertempat tinggal sewa/kos </li>
								<li>2. Membuat Surat Pernyataan bahwa masih bertempat tinggal sewa/kos <button class="btn btn-success btn-xs" onclick="javascript:downloadsp();">contohnya</button> </li>
							</ul>
						</div>
					</div>
				</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<div id="luastanah_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">Informasi Luas Tanah</h5>
      </div>
      <div class="modal-body" id="isimodal">
				<div class="accordion">
					<div class="accordion-item">
						<a>Bagaimana kalau saya masih menyewa rumah/KOS ?</a>
						<div class="content">
							<span>untuk form Luas Tanahnya bisa diisi dengan "0" dengan melampirkan pada PBB (Opsi bisa pilih salah satu)</span>
							<ul>
								<li>1. Melampirkan Surat pernyataan dari desa/kelurahan jika masih bertempat tinggal sewa/kos </li>
								<li>2. Membuat Surat Pernyataan bahwa masih bertempat tinggal sewa/kos <button class="btn btn-success btn-xs" onclick="javascript:downloadsp();">contohnya</button> </li>
							</ul>
						</div>
					</div>
				</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div id="PLN_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">Informasi PLN</h5>
      </div>
      <div class="modal-body" id="isimodal">
				<div class="accordion">
					<div class="accordion-item">
						<a>Bagaimana jika menggunakan Pulsa ?</a>
						<div class="content">
							<span>untuk form PLN diisi dengan daya PLN untuk lampiran melampirkan struk pulsa PLN </span>
						</div>
					</div>
				</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div id="PBB_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">Informasi PBB</h5>
      </div>
      <div class="modal-body" id="isimodal">
				<div class="accordion">
					<div class="accordion-item">
						<a>Bagaimana kalau saya masih menyewa rumah/KOS ?</a>
						<div class="content">
							<span>untuk form PBB bisa diisi dengan "0" dengan melampirkan pada PBB (Opsi bisa pilih salah satu)</span>
							<ul>
								<li>1. Melampirkan Surat pernyataan dari desa/kelurahan jika masih bertempat tinggal sewa/kos </li>
								<li>2. Membuat Surat Pernyataan bahwa masih bertempat tinggal sewa/kos <button class="btn btn-success btn-xs" onclick="javascript:downloadsp();">contohnya</button> </li>
							</ul>
						</div>
					</div>
				</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<div id="ortu_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">Informasi Orang Tua</h5>
      </div>
      <div class="modal-body" id="isimodal">
				<div class="accordion">
					<div class="accordion-item">
						<a>Bagaimana kalau Ayah saya sudah meninggal ?</a>
						<div class="content">
							<span>untuk form Pekerjaan dipilih "Lainnya" dan penghasilan diisi dengan "0" dengan melampirkan (Opsi bisa pilih salah satu) </span>
							<ul>
								<li>1. Melampirkan Surat Kematian Ayah </li>
								<li>2. Melampirkan Surat pernyataan dari desa/kelurahan jika Ayah Sudah Meninggal</li>
							</ul>
						</div>
					</div>
					<div class="accordion-item">
						<a>Bagaimana kalau Ibu saya sudah meninggal ?</a>
						<div class="content">
							<span>untuk form Pekerjaan dipilih "Lainnya" dan penghasilan diisi dengan "0" dengan melampirkan (Opsi bisa pilih salah satu) </span>
							<ul>
								<li>1. Melampirkan Surat Kematian Ibu </li>
								<li>2. Melampirkan Surat pernyataan dari desa/kelurahan jika Ibu Sudah Meninggal</li>
							</ul>
						</div>
					</div>
				</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<div id="KKS_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">Informasi KKS</h5>
      </div>
      <div class="modal-body" id="isimodal">
				<div class="accordion">
					<div class="accordion-item">
						<a>Bagaimana Kalau tidak mempunyai KKS (Kartu Keluarga Sejahtera) ?</a>
						<div class="content">
							<span>Jika tidak mempunyai KKS tidak perlu mengupload KKS, kosongi saja</span>
						</div>
					</div>
					<div class="accordion-item">
						<a>Apakah Bisa diganti dengan Surat Keterangan Tidak Mampu ?</a>
						<div class="content">
							<span>Tidak Bisa, hanya keluarga yang mempunyai KKS</span>
						</div>
					</div>
					<div class="accordion-item">
						<a>Seperti Apa KKS (Kartu Keluarga Sejahtera) itu  ?</a>
						<div class="content">
							<span>Berikut contoh KKS <button target="_blank" onclick="javascript:checkKKS();">
                (disini)
              </button> </span>
						</div>
					</div>
					
				</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- end modal -->

<script>

function downloadsp(){
	window.open('<?php echo base_url()?>assets/document/templateSP.pdf','_blank');
}
function checkKKS(){
	window.open('https://www.google.com/search?safe=strict&client=firefox-b-d&biw=1536&bih=750&tbm=isch&sa=1&ei=-L-XXPjwKoHHvgSGhKOABA&q=kartu+kks&oq=kartu+kks&gs_l=img.3..0l3j0i30j0i5i30l4j0i8i30j0i24.9736.10384..10672...0.0..0.70.193.3......1....1..gws-wiz-img.Ai8I9d5_vTw','_blank');
}
const items = document.querySelectorAll(".accordion a");

function toggleAccordion(){
  this.classList.toggle('active');
  this.nextElementSibling.classList.toggle('active');
}

items.forEach(item => item.addEventListener('click', toggleAccordion));
</script>

<style>
@import url('https://fonts.googleapis.com/css?family=Open+Sans');

.accordion a {
  position: relative;
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
  -webkit-flex-direction: column;
  -ms-flex-direction: column;
  flex-direction: column;
  width: 100%;
  padding: 1rem 3rem 1rem 1rem;
  color: #7288a2;
  font-size: 1.15rem;
  font-weight: 400;
  border-bottom: 1px solid #e5e5e5;
}

.accordion a:hover,
.accordion a:hover::after {
  cursor: pointer;
  color: #03b5d2;
}

.accordion a:hover::after {
  border: 1px solid #03b5d2;
}

.accordion a.active {
  color: #03b5d2;
  border-bottom: 1px solid #03b5d2;
}

.accordion a::after {
  font-family: 'Ionicons';
  content: '\f218';
  position: absolute;
  float: right;
  right: 1rem;
  font-size: 1rem;
  color: #7288a2;
  padding: 5px;
  width: 30px;
  height: 30px;
  -webkit-border-radius: 50%;
  -moz-border-radius: 50%;
  border-radius: 50%;
  border: 1px solid #7288a2;
  text-align: center;
}

.accordion a.active::after {
  font-family: 'Ionicons';
  content: '\f209';
  color: #03b5d2;
  border: 1px solid #03b5d2;
}

.accordion .content {
  opacity: 0;
  padding: 0 1rem;
  max-height: 0;
  border-bottom: 1px solid #e5e5e5;
  overflow: hidden;
  clear: both;
  -webkit-transition: all 0.2s ease 0.15s;
  -o-transition: all 0.2s ease 0.15s;
  transition: all 0.2s ease 0.15s;
}

.accordion .content p {
  font-size: 1rem;
  font-weight: 300;
}

.accordion .content.active {
  opacity: 1;
  padding: 1rem;
  max-height: 100%;
  -webkit-transition: all 0.35s ease 0.15s;
  -o-transition: all 0.35s ease 0.15s;
  transition: all 0.35s ease 0.15s;
}
</style>