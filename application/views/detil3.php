<?php foreach($profile as $profil);
?>
<html>
  <head><meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>

  	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/reset.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/css.css" type="text/css "/>

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/proses.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	<!-- CKeditor CSS -->
	<script src="<?php echo base_url();?>assets/js/ckeditor.js"></script>
	
  </head>
  <body>
  <div class="atas col-xs-12 col-sm-12 col-md-12" >
			<a href="<?php echo base_url();?>uin">
			<div class="btn btn-default pull-left" style="background-color:#eee;border:none;color:red;"> <span class="glyphicon glyphicon-arrow-left" style="margin-right:.5vw;"></span>Kembali</div>
			</a>
		</div>
	<div class="header">
		<div class="container">
				<div class="col-xs-12 col-sm-12 col-md-12" >
					<div class="row">
						<div class="head2">
							PENGAJUAN UKT</br> Mahasiswa UIN Sunan Ampel Surabaya
						</div>
						<div class="kotak">
							<div class="kanan">
								<div class="foto"><img src="<?php echo base_url()?>assets/foto/<?php echo $profil['foto'];?>"/></div>
								<div class="tempatNama">
									<div class="nama1">Nama :</div>
									<div class="nama"><?php echo $profil['nama'];?></div>
								</div>
								<div class="tempatNama">
									<div class="nama1">Kode :</div>
									<div class="nama"><?php echo $profil['kode'];?></div>
								</div>
								<div class="tempatNama">
									<div class="nama1">Program Studi :</div>
									<div class="nama"><?php echo $profil['nama_prodi'];?></div>
								</div>
								<div class="tempatNama">
									<div class="nama1">Tahun Masuk :</div>
									<div class="nama"><?php echo $profil['tahun_masuk'];?></div>
								</div>
							</div>
							<div class="kiri">
								<div class="kotJ">
									<div class="jud1">
										<?php print_r ($tgl[0]['tgl']);?>
									</div>
									<div class="jud">
										DATA PENDUKUNG
									</div>
								</div>
								<div class="content">
									  <!-- Nav tabs -->
									<div class="card">
                                    <ul class="nav nav-tabs" style="" role="tablist">
                                        <li role="presentation" ><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Biodata Mahasiswa</a></li>
                                        <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Validasi</a></li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane " id="home">
										<div class="well well-sm" style="background-color:2b975b;color:#fff">I. Data Diri</div>
											<div class="hp">
												<div class="form-group">
													<label for="nama" class="lb">Nama</label>
													<div for="nama" class="fc2">Laili Nur Qomary</div>
												</div>
												<div class="form-group">
													<label for="nama" class="lb">Kode</label>
												<div for="nama" class="fc2">H96214021</div>
												</div>
												<div class="form-group">
													<label for="nama" class="lb">Program Studi</label>
												<div for="nama" class="fc2">Sistem Informasi</div>
												</div>
											</div>
											<div class="form-group">
												<label for="nama" class="lb">Alamat</label>
												<div for="nama" class="fc2"><?php if($profil['alamat'] != null){echo $profil['alamat'];}else {echo "-";} ?></div>
											</div>
											<div class="form-group">
												<label for="nama" class="lb">Provinsi</label>
												<div for="nama" class="fc2"><?php if($profil['prov'] != null){echo $profil['prov'];}else {echo "-";} ?></div>
											</div>
											<div class="form-group">
												<label for="nama" class="lb">Kota</label>
												<div for="nama" class="fc2"><?php if($profil['kab'] != null){echo $profil['kab'];}else {echo "-";} ?></div>
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Kode Pos</label>
											  <div for="nama" class="fc2"><?php if($profil['kode_pos'] != null){echo $profil['kode_pos'];}else {echo "-";} ?></div>
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Telp Rumah</label>
											  <div for="nama" class="fc2"> <?php if($profil['tlp_rmh'] != null){echo $profil['tlp_rmh'];}else {echo "-";}?></div>
											</div>
											
											<div class="form-group">
											  <label for="usr" class="lb">No HP</label>
											  <div for="nama" class="fc2"> <?php if($profil['hp'] != null){echo $profil['hp'];}else {echo "-";} ?></div>
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Email</label>
											  <div for="nama" class="fc2"> <?php if($profil['email'] != null){echo $profil['email'];}else {echo "-";}?></div>
											</div>
											<div class="form-group">
												<label for="nama" class="lb">Kewarganegaraan</label>
												<div for="nama" class="fc2"><?php 
												if($profil['warganegara'] == "1"){
													echo "Warga Negara Indonesia";
												}else {
													echo "Warga Negara Asing";
												}
												?></div>
											</div>
											
										<div class="well well-sm" style="background-color:2b975b;color:#fff">II. Keluarga</div>
											<div class="form-group">
											  <label for="usr" class="lb">Nama Ayah/Wali</label>
											  <div for="nama" class="fc2"> <?php if($profil['nama_ayah'] != null){echo $profil['nama_ayah'];}else {echo "-";} ?></div>
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Pekerjaan Ayah/Wali</label>
											  <div for="nama" class="fc2"> <?php if($profil['peker_ayah'] != null){echo $profil['peker_ayah'];}else {echo "-";} ?></div>
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Nama Ibu</label>
											  <div for="nama" class="fc2"> <?php if($profil['nama_ibu'] != null){echo $profil['nama_ibu'];}else {echo "-";} ?></div>
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Pekerjaan Ibu</label>
											  <div for="nama" class="fc2"> <?php if($profil['peker_ibu'] != null){echo $profil['peker_ibu'];}else {echo "-";} ?></div>
											</div>
											<div class="form-group">
												<label for="nama" class="lb">Jumlah Tanggungan</label>
												<div for="nama" class="fc2"> <?php if($profil['anggota'] != null){echo $profil['anggota'];}else {echo "-";} ?></div>
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">No HP Orang Tua</label>
											  <div for="nama" class="fc2"> <?php if($profil['hp_ortu'] != null){echo $profil['hp_ortu'];}else {echo "-";} ?></div>
											</div>
											<div class="well well-sm" style="background-color:2b975b;color:#fff">III. Aset</div>
											<div class="form-group">
											  <label for="usr" class="lb">Kepemilikan</label>
											  <div for="nama" class="fc2"> <?php if($profil['kepemilikan'] != null){echo $profil['kepemilikan'];}else {echo "-";} ?></div>
											</div>
											
											<div class="form-group">
											  <label for="usr" class="lb">Luas Tanah</label>
											  <div for="nama" class="fc2"> <?php if($profil['luas_tanah'] != null){echo $profil['luas_tanah'];}else {echo "-";} ?></div>
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Luas Bangunan</label>
											  <div for="nama" class="fc2"> <?php if($profil['luas_bangunan'] != null){echo $profil['luas_bangunan'];}else {echo "-";} ?></div>
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Sumber Listrik</label>
											  <div for="nama" class="fc2"> <?php if($profil['sum_listrik'] != null){echo $profil['sum_listrik'];}else {echo "-";} ?></div>
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Sumber Air</label>
											  <div for="nama" class="fc2"> <?php if($profil['sum_air'] != null){echo $profil['sum_air'];}else {echo "-";} ?></div>
											</div>
											
										</div>
                                        <div role="tabpanel" class="tab-pane active" id="profile">
											
										
										<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
		                                <thead>
		                                    <tr>
		                                        <th style="text-align:center;">Keterangan</th>
		                                        <th style="text-align:center;">Input</th>
		                                        <th style="text-align:center;">Bukti</th>
		                                    </tr>
		                                </thead>
		                                <tbody>
											
											<tr>
		                                        <td style="text-align:center;vertical-align: middle;">Luas Tanah</td>
		                                        <td style="text-align:center;vertical-align: middle;"><?php if($profil['luas_tanah'] != null){echo $profil['luas_tanah'];}else {echo "-";} ?></td>
		                                        <td style="text-align:center;">
													<a href="javascript:modal_foto('<?php echo $profil['upload_pbb'];?>');">
													<img data-toggle="modal" data-target="#myModal" class="thumbnail zoom" style="height:15vw;width:12vw;"src="<?php echo base_url()?>assets/foto/<?php if($profil['upload_pbb'] != null){echo $profil['upload_pbb'];}else {echo "default2.jpg";} ?>"/>
													</a>
												</td>
													
		                                    </tr>
											
											<tr>
		                                        <td style="text-align:center;vertical-align: middle;">Luas Bangunan</td>
		                                        <td style="text-align:center;vertical-align: middle;" ><?php if($profil['luas_bangunan'] != null){echo $profil['luas_bangunan'];}else {echo "-";} ?></td>
		                                        <td style="text-align:center;">
													<a href="javascript:modal_foto('<?php echo $profil['upload_pbb'];?>');">
													<img data-toggle="modal" data-target="#myModal" class="thumbnail zoom" style="height:15vw;width:12vw;"src="<?php echo base_url()?>assets/foto/<?php if($profil['upload_pbb'] != null){echo $profil['upload_pbb'];}else {echo "default2.jpg";} ?>"/>
													</a>
												</td>	
													                                  
											</tr>
											<tr>
		                                        <td style="text-align:center;vertical-align: middle;">Memiliki Sepeda Motor</td>
		                                        <td style="text-align:center;vertical-align: middle;">Ya</td>
		                                        <td style="text-align:center;">
													<a href="javascript:modal_foto('<?php echo $profil['upload_stnk'];?>');">
														<img data-toggle="modal" data-target="#myModal" class="thumbnail zoom" style="height:15vw;width:12vw;"src="<?php echo base_url()?>assets/foto/<?php if($profil['upload_stnk'] != null){echo $profil['upload_stnk'];}else {echo "default2.jpg";} ?>"/>
													</a>
												</td>
													
		                                    </tr>
											<tr>
		                                        <td style="text-align:center;vertical-align: middle;">Bahan Atap</td>
		                                        <td style="text-align:center;vertical-align: middle;">Genteng</td>
		                                        <td style="text-align:center;">
													<a href="javascript:modal_foto('<?php echo $profil['upload_atap'];?>');">
														<img data-toggle="modal" data-target="#myModal" class="thumbnail zoom" style="height:15vw;width:12vw;"src="<?php echo base_url()?>assets/foto/<?php if($profil['upload_atap'] != null){echo $profil['upload_atap'];}else {echo "default2.jpg";} ?>"/>
													</a>
												</td>
		                                    </tr>
											<tr>
		                                        <td style="text-align:center;vertical-align: middle;">Bahan Lantai</td>
		                                        <td style="text-align:center;vertical-align: middle;">Keramik</td>
		                                        <td style="text-align:center;">
													<a href="javascript:modal_foto('<?php echo $profil['upload_lantai'];?>');">
														<img data-toggle="modal" data-target="#myModal" class="thumbnail zoom" style="height:15vw;width:12vw;"src="<?php echo base_url()?>assets/foto/<?php if($profil['upload_lantai'] != null){echo $profil['upload_lantai'];}else {echo "default2.jpg";} ?>"/>
													</a>
												</td>
		                                    </tr>
											<tr>
		                                        <td style="text-align:center;vertical-align: middle;">Pembayaran PBB/Tahun</td>
		                                        <td style="text-align:center;vertical-align: middle;"><?php echo $profil['pbb'];?></td>
		                                        <td style="text-align:center;">
													<a href="javascript:modal_foto('<?php echo $profil['upload_pbb'];?>');">
														<img data-toggle="modal" data-target="#myModal" class="thumbnail zoom" style="height:15vw;width:12vw;"src="<?php echo base_url()?>assets/foto/<?php if($profil['upload_pbb'] != null){echo $profil['upload_pbb'];}else {echo "default2.jpg";} ?>"/>
													</a>
												</td>
													 
		                                    </tr>
											<tr>
		                                        <td style="text-align:center;vertical-align: middle;">Daya Pln</td>
		                                        <td style="text-align:center;vertical-align: middle;"><?php echo $profil['pln'];?></td>
		                                        <td style="text-align:center;">
													<a href="javascript:modal_foto('<?php echo $profil['upload_pln'];?>');">
														<img data-toggle="modal" data-target="#myModal" class="thumbnail zoom" style="height:15vw;width:12vw;"src="<?php echo base_url()?>assets/foto/<?php if($profil['upload_pln'] != null){echo $profil['upload_pln'];}else {echo "default2.jpg";} ?>"/>
													</a>
												</td>
													 
		                                    </tr>
											<tr>
		                                        <td style="text-align:center;vertical-align: middle;">Pembayaran PDAM/Bulan</td>
		                                        <td style="text-align:center;vertical-align: middle;"><?php echo $profil['pdam'];?></td>
		                                        <td style="text-align:center;">
													<a href="javascript:modal_foto('<?php echo $profil['upload_pdam'];?>');">
														<img data-toggle="modal" data-target="#myModal" class="thumbnail zoom" style="height:15vw;width:12vw;"src="<?php echo base_url()?>assets/foto/<?php if($profil['upload_pdam'] != null){echo $profil['upload_pdam'];}else {echo "default2.jpg";} ?>"/>
													</a>
												</td>
													
		                                    </tr>
											<tr>
		                                        <td style="text-align:center;vertical-align: middle;">Penghasilan Ayah</td>
		                                        <td style="text-align:center;vertical-align: middle;"><?php echo $profil['peng_ayah'];?></td>
		                                        <td style="text-align:center;">
													<a href="javascript:modal_foto('<?php echo $profil['upload_gaji_ayah'];?>');">
														<img data-toggle="modal" data-target="#myModal" class="thumbnail zoom" style="height:15vw;width:12vw;"src="<?php echo base_url()?>assets/foto/<?php if($profil['upload_gaji_ayah'] != null){echo $profil['upload_gaji_ayah'];}else {echo "default2.jpg";} ?>"/>
													</a>
												</td>
													
		                                    </tr>
											<tr>
		                                        <td style="text-align:center;vertical-align: middle;">Penghasilan Ibu</td>
		                                        <td style="text-align:center;vertical-align: middle;"><?php echo $profil['peng_ibu'];?></td>
		                                        <td style="text-align:center;">
													<a href="javascript:modal_foto('<?php echo $profil['upload_gaji_ibu'];?>');">
														<img data-toggle="modal" data-target="#myModal" class="thumbnail zoom" style="height:15vw;width:12vw;"src="<?php echo base_url()?>assets/foto/<?php if($profil['upload_gaji_ibu'] != null){echo $profil['upload_gaji_ibu'];}else {echo "default2.jpg";} ?>"/>
													</a>
												</td>
													
		                                    </tr>
											<tr>
		                                        <td style="text-align:center;vertical-align: middle;">Jumlah Anggota KK</td>
		                                        <td style="text-align:center;vertical-align: middle;"><?php echo $profil['anggota'];?></td>
		                                        <td style="text-align:center;">
													<a href="javascript:modal_foto('<?php echo $profil['upload_kk'];?>');">
														<img data-toggle="modal" data-target="#myModal" class="thumbnail zoom" style="height:15vw;width:12vw;"src="<?php echo base_url()?>assets/foto/<?php if($profil['upload_kk'] != null){echo $profil['upload_kk'];}else {echo "default2.jpg";} ?>"/>
													</a>
												</td>
													
		                                    </tr>
											
		                                </tbody>
		                            </table>
											
											<!-- Modal -->
											<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
											  <div class="modal-dialog" role="document">
												<div class="modal-content">
												  <div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													<h4 style="font-size:1.5vw; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Bukti Data Penghasilan</h4>
												  </div>
												  <div class="modal-body" id="modal_foto">
													<img style="width:100%;" src="images/we.png"/>
												  </div>
												  <div class="modal-footer">
												  </div>
												</div>
											  </div>
											</div>
										</div>
                                    </div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
		</div>
	
	<!--div class="footer">
	
	</div>-->
	</div>
  </body>

 </html>
 
 <script>
 function modal_foto(foto){
	 if(foto == ""){
		 foto = "default2.jpg";
	 }
	$("#modal_foto").empty();
	$("#modal_foto").append('<img  style="width:100%;" src="<?php echo base_url()?>assets/foto/'+foto+'" />');
}
 
 </script>