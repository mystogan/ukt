

            <!-- start: Content -->
            <div id="content">
               <div class="panel box-shadow-none content-header">
                  <div class="panel-body">
                    <div class="col-md-12">
                        <h3 class="animated fadeInLeft">Data Mahasiswa Jalur <?php echo $jalur['ket']; ?></h3>
                    </div>
                  </div>
              </div>
              <div class="col-md-12 top-20 padding-0">
                <div class="col-md-12">
                  <div class="panel">
                    <!-- <div class="panel-heading"><h3>UKT Kelompok <?php echo $ukt; ?></h3></div> -->
                    <div class="panel-body">
                      <div class="responsive-table">
                      <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Kode Mahasiswa</th> 
                          <th>Nama Mahasiswa</th>
                          <th>Tanggal Lahir</th>
                          <th>Program Studi</th>
                          <th>Finalisasi</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                          $c = 1;
                          foreach ($mhs as $Hmhs) {
                            ?>
                            <tr>
                              <td><?php echo $c; ?></td>
                               <td><?php echo $Hmhs['kode']; ?></td> 
                              <td><?php echo $Hmhs['nama']; ?></td>
                              <td><?php echo $Hmhs['tgl_lhr']; ?></td>
                              <td><?php echo $Hmhs['prodi']; ?></td>
                              <td><?php 
                              if($Hmhs['mhs_finalisasi'] == 1){
                                echo '<a href="'.base_url().'helpdesk/updatefinalisasi/'.$Hmhs['kode'].'" class="btn btn-danger" >Batal Finalisasi</a>';
                              }else {
                                echo "Belum Finalisasi";
                              }
                              echo $Hmhs['finalisasi']; ?></td>
                              <td style="text-align:center;">
                                <a href="javascript:kode('<?php echo $Hmhs['id_mhs'];?>','<?php echo $Hmhs['kode'];?>','<?php echo $Hmhs['tgl_lhr'];?>');">
                                <button type="button" class="btn btn-3d btn-info" data-toggle="modal" data-target="#myModal">Edit</button>
                                </a>
                              </td>

                            </tr>

                        <?php
                        $c++;
                          }
                         ?>

                      </tbody>
                        </table>
                      </div>
                  </div>
                </div>
              </div>
              </div>
            </div>
          <!-- end: content -->

          <!-- start Modal  -->
          <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
              <div class="modal-content">
                <form action="<?php echo base_url()?>helpdesk/updatetgl_lahir" method="post">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" style="text-align:center;"id="myModalLabel">Pilih UKT Mahasiswa</h4>
                </div>
                <input type="hidden" name="kode" id="id"  />
                <div class="modal-body col-xs-12 col-sm-12 col-md-12" style="text-align:center;">
                  <div class="col-xs-6 col-sm-4 col-md-4" >
                    <div class="form-animate-radio">
                      <label class="radio">
                        Kode
                      </label>
                    </div>
                  </div>
                  <div class="col-xs-6 col-sm-4 col-md-6" >
                    <div class="form-animate-radio">
                      <input type="text" class="form-control" name="kodemhs" id="kodemhs" readonly  />
                    </div>
                  </div>
                </div>
                <div class="modal-body col-xs-12 col-sm-12 col-md-12" style="text-align:center;">
                  <div class="col-xs-6 col-sm-4 col-md-4" >
                    <div class="form-animate-radio">
                      <label class="radio">
                        Tanggal Lahir
                      </label>
                    </div>
                  </div>
                  <div class="col-xs-6 col-sm-4 col-md-6" >
                    <div class="form-animate-radio">
                      <input type="text" class="form-control" name="tgl_lhr" id="tgl_lhr"  />
                    </div>
                  </div>
                </div>

                <div class="modal-footer"style="">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
                </form>
              </div>
              </div>
          </div>

          <!-- End Modal  -->

          <!-- start: right menu -->
            <div id="right-menu">
              <ul class="nav nav-tabs">
                <li class="active">
                 <a data-toggle="tab" href="#right-menu-user">
                  <span class="fa fa-comment-o fa-2x"></span>
                 </a>
                </li>
                <li>
                 <a data-toggle="tab" href="#right-menu-notif">
                  <span class="fa fa-bell-o fa-2x"></span>
                 </a>
                </li>
                <li>
                  <a data-toggle="tab" href="#right-menu-config">
                   <span class="fa fa-cog fa-2x"></span>
                  </a>
                 </li>
              </ul>

            </div>
          <!-- end: right menu -->

      </div>


<!-- start: Javascript -->
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/bootstrap.min.js"></script>



<!-- plugins -->
<script src="<?php echo base_url(); ?>assets/admin/js/plugins/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/plugins/jquery.datatables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/plugins/datatables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/plugins/jquery.nicescroll.js"></script>


<!-- custom -->
<script src="<?php echo base_url(); ?>assets/admin/js/main.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#datatables-example').DataTable();

  });

  function kode(id,kode,tgl){
    $("#idMHS").val(id);
    $("#kodemhs").val(kode);
    $("#tgl_lhr").val(tgl);
  }
</script>
<!-- end: Javascript -->
</body>
</html>
