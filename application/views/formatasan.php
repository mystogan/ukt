<html>
  <head><meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>

  	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/reset.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/circle.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/css.css" type="text/css "/>
	
	<!-- DataTables CSS -->
	<link href="<?php echo base_url();?>assets/css/dataTables.bootstrap.css" rel="stylesheet">

	<!-- DataTables Responsive CSS -->
	<link href="<?php echo base_url();?>assets/css/dataTables.responsive.css" rel="stylesheet">

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<!-- CKeditor CSS -->
	<script src="http://cdn.ckeditor.com/4.5.9/standard/ckeditor.js"></script>
	<!-- Custom Theme JavaScript -->
	<script src="<?php echo base_url();?>assets/js/jquery.dataTables.min.js" type="text/javascript" ></script>
	<script src="<?php echo base_url();?>assets/js/dataTables.bootstrap.min.js" type="text/javascript" ></script>
	<script src="<?php echo base_url();?>assets/js/dataTables.responsive.js" type="text/javascript" ></script>
	
	<script>

	function setJalur(){
		var id = $("#jalur").val();
		window.location.assign("<?php echo base_url();?>atasan/setjalur/"+id);
	}
	</script>
  </head>
  <body>
  
		<div class="atas col-xs-12 col-sm-12 col-md-12" >
			<a href="<?php echo base_url();?>atasan/logout">
				<div class="btn btn-default pull-right" style="background-color:#eee;border:none;color:red;"> <span class="glyphicon glyphicon-off" style="margin-right:.5vw;"></span>Keluar</div>
			</a>
		</div>
	<div class="headerK">
	<div class="container">
		<div class="col-xs-12 col-sm-12 col-md-12" >
			<div class="row">
				<div class="logo"><a href="<?php echo base_url();?>atasan"><img src="<?php echo base_url();?>assets/images/aan.png"/></a></div>
				<div class="judulK">Validasi Pengajuan UKT </br>Mahasiswa Sistem Informasi UIN Sunan Ampel Surabaya</div>
			</div>
		</div>
	</div>
	</div>
		<div class="col-xs-12 col-sm-12 col-md-12" style="margin-top:2vw; margin-bottom:4vw;">
			<div class="row">
			<div class="kotpres1 col-xs-2 col-sm-2 col-md-2">
				<select class="form-control" id="jalur" name="jalur" onclick="javascript:setJalur();">
					<option value="2" >SBMPTN</option>												
					<option value="4" >UMPTAIN</option>												
				</select>
			</div>
			<br/>
			<br/>
			<br/>
			<div class="kotpres1 col-xs-6 col-sm-6 col-md-6">
				<a href="<?php echo base_url();?>atasan/prodi">Untuk Melihat Rekap Klik disini</a>
			</div>
			<br/>
			<br/>
			<div class="panel-heading e">
		        Presentase Rekomendasi Kelompok UKT
		    </div>
			<div class="m col-xs-12 col-sm-12 col-md-12">
				<div class="col-xs-6 col-sm-4 col-md-2" style="text-align:center;">
					<div class="kotpres col-xs-12 col-sm-12 col-md-12">
						<div class="kotpres1 col-xs-12 col-sm-12 col-md-12">
							UKT 1
						</div>
						<div class="kotpres2 col-xs-12 col-sm-12 col-md-12">
							   <div class="c100 p<?php print_r (number_format(($satu['hasil']/$all['hasil']),3));?> big" style="">
									<span><?php print_r (number_format(($satu['hasil']/$all['hasil'])*100,1));?>%</span>
									<div class="slice">
										<div class="bar"></div>
										<div class="fill"></div>
									</div>
								</div>
						</div>
						<a href="<?php echo base_url()?>atasan/lihatdata/1"><div class="kotpres3 col-xs-12 col-sm-12 col-md-12">
							<?php echo $satu['hasil'];?> Mahasiswa <span class="glyphicon glyphicon-chevron-right"></span>
						</div></a>
					</div>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-2" style="text-align:center;">
					<div class="kotpres col-xs-12 col-sm-12 col-md-12">
						<div class="kotpres1 col-xs-12 col-sm-12 col-md-12">
							UKT 2
						</div>
						<div class="kotpres2 col-xs-12 col-sm-12 col-md-12">
							   <div class="c100 p<?php print_r (number_format(($dua['hasil']/$all['hasil']),3));?> big green" style="">
									<span><?php print_r (number_format(($dua['hasil']/$all['hasil'])*100,1));?>%</span>
									<div class="slice">
										<div class="bar"></div>
										<div class="fill"></div>
									</div>
								</div>
						</div>
						<a href="<?php echo base_url()?>atasan/lihatdata/2"><div class="kotpres31 col-xs-12 col-sm-12 col-md-12">
							<?php echo $dua['hasil'];?> Mahasiswa <span class="glyphicon glyphicon-chevron-right"></span>
						</div></a>
					</div>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-2" style="text-align:center;">
					<div class="kotpres col-xs-12 col-sm-12 col-md-12">
						<div class="kotpres1 col-xs-12 col-sm-12 col-md-12">
							UKT 3
						</div>
						<div class="kotpres2 col-xs-12 col-sm-12 col-md-12">
							   <div class="c100 p<?php print_r (number_format(($tiga['hasil']/$all['hasil']),3));?> big orange" style="">
									<span><?php print_r (number_format(($tiga['hasil']/$all['hasil'])*100,1));?>%</span>
									<div class="slice">
										<div class="bar"></div>
										<div class="fill"></div>
									</div>
								</div>
						</div>
						<a href="<?php echo base_url()?>atasan/lihatdata/3"><div class="kotpres32 col-xs-12 col-sm-12 col-md-12">
							<?php echo $tiga['hasil'];?> Mahasiswa <span class="glyphicon glyphicon-chevron-right"></span>
						</div></a>
					</div>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-2" style="text-align:center;">
					<div class="kotpres col-xs-12 col-sm-12 col-md-12">
						<div class="kotpres1 col-xs-12 col-sm-12 col-md-12">
							UKT 4
						</div>
						<div class="kotpres2 col-xs-12 col-sm-12 col-md-12">
							   <div class="c100 p<?php print_r (number_format(($empat['hasil']/$all['hasil']),3));?> big " style="">
									<span><?php print_r (number_format(($empat['hasil']/$all['hasil'])*100,1));?>%</span>
									<div class="slice">
										<div class="bar"></div>
										<div class="fill"></div>
									</div>
								</div>
						</div>
						<a href="<?php echo base_url()?>atasan/lihatdata/4"><div class="kotpres3 col-xs-12 col-sm-12 col-md-12">
							<?php echo $empat['hasil'];?> Mahasiswa <span class="glyphicon glyphicon-chevron-right"></span>
						</div></a>
					</div>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-2" style="text-align:center;">
					<div class="kotpres col-xs-12 col-sm-12 col-md-12">
						<div class="kotpres1 col-xs-12 col-sm-12 col-md-12">
							UKT 5
						</div>
						<div class="kotpres2 col-xs-12 col-sm-12 col-md-12">
							   <div class="c100 p<?php print_r (number_format(($lima['hasil']/$all['hasil']),3));?> big green" style="">
									<span><?php print_r (number_format(($lima['hasil']/$all['hasil'])*100,1));?>%</span>
									<div class="slice">
										<div class="bar"></div>
										<div class="fill"></div>
									</div>
								</div>
						</div>
						<a href="<?php echo base_url()?>atasan/lihatdata/5"><div class="kotpres31 col-xs-12 col-sm-12 col-md-12">
							<?php echo $lima['hasil'];?> Mahasiswa <span class="glyphicon glyphicon-chevron-right"></span>
						</div></a>
					</div>
				</div>
			</div>
			</div>
		</div>
  
  </body>
 </html>