<?php 
error_reporting(0);
 foreach($profile as $profil);
 $user = $this->session->userdata('kode');
 $o=0;
 $p=0;
 ?>

<html>
  <head><meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
  	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/reset.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/css.css" type="text/css "/>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/proses.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	<!-- CKeditor CSS -->
	<script src="<?php echo base_url();?>assets/js/ckeditor.js"></script>
	
  </head>
  <body>
	<div class="header">
		<div class="container">
				<div class="col-xs-12 col-sm-12 col-md-12" >
					<div class="row">
						<div class="head">
							FORM PENGAJUAN UKT</br> Mahasiswa UIN Sunan Ampel Surabaya
						</div>
						<div class="kotak">
							<div class="kanan">
								<div class="foto"><img src="<?php echo base_url();?>assets/foto/<?php echo $profil['foto'];?>"/></div>
								<div class="tempatNama">
									<div class="nama1">Nama :</div>
									<div class="nama"><?php echo $profil['nama'];?></div>
								</div>
								<div class="tempatNama">
									<div class="nama1">Kode :</div>
									<div class="nama"><?php echo $profil['kode'];?></div>
								</div>
								<div class="tempatNama">
									<div class="nama1">Jenis Kelamin :</div>
									<div class="nama"><?php 
									if($profil['jk'] == "L"){
										echo "Laki-Laki";
									}else {
										echo "Perempuan";
									}
									?></div>
								</div>
								<div class="tempatNama">
									<div class="nama1">Program Studi :</div>
									<div class="nama"><?php echo $profil['nama_prodi'];?></div>
								</div>
								<div class="tempatNama">
									<div class="nama1">Tahun Masuk :</div>
									<div class="nama"><?php echo $profil['tahun_masuk'];?></div>
								</div>
								<div class="tempatNama">	
									<a href="<?php echo base_url()?>home/logout" class="btn btn-danger" style="margin-top:2vw;">Logout</a>
								</div>
							</div>
							<div class="kiri">
								<div class="kotJ">
									<!--<a href="<?php echo base_url()?>home/logout" class="hp3" >Logout</a>-->
									<div class="jud1">
										<?php print_r ($tgl[0]['tgl']);?>
									</div>
									<div class="jud">
										DATA PENDUKUNG
									</div>
								</div>
								<div class="content">
									  <!-- Nav tabs -->
									<div class="card">
									<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-info-sign" style="margin-right:1vw;"></span>Data Yang ditulis dan dilampirkan harus benar dan lengkap, jika tidak maka kemungkinan dapat digugurkan</div>
                                <section>
								<div class="wizard">	
									<div class="wizard-inner">
                                    <ul class="nav nav-tabs" style="" role="tablist">
                                        <li role="presentation" class="active"><a href="#home" data-toggle="tab" aria-controls="home" role="tab" >Data Diri</a></li>
                                        <li role="presentation" class="disabled"><a href="#profile" data-toggle="tab" aria-controls="profile" role="tab" >Keluarga</a></li>
                                        <li role="presentation" class="disabled"><a href="#messages" data-toggle="tab" aria-controls="messages" role="tab" >Rumah Tinggal</a></li>
                                        <li role="presentation" class="disabled"><a href="#settings" data-toggle="tab" aria-controls="settings" role="tab" >Lampiran</a></li>
                                    </ul>
									</div>
                                    <!-- Tab panes -->
                                    <div class="tab-content" style="border:1px solid #eee;padding-bottom:8vh; border-top:none;border-radius:5px;">
                                        <div role="tabpanel" class="tab-pane active" id="home" >
											<form action="<?php echo base_url()?>home/save1" method="post">
											<input type="hidden" id="kode_mhs" name="kode_mhs" value="<?php echo $user;?>" />
											<div class="hp">
												<div class="form-group">
													<label for="nama" class="lb">Nama</label>
													<input type="text" class="form-control fc"id="usr" value="Laili Nur Qomary" disabled>
												</div>
												<div class="form-group">
													<label for="nama" class="lb">NIM</label>
													<input type="text" class="form-control fc"id="usr" value="H96214021" disabled>
												</div>
												<div class="form-group">
													<label for="nama" class="lb">Program Studi</label>
													<input type="text" class="form-control fc"id="usr" value="Sistem Informasi" disabled>
												</div>
											</div>
											<div class="form-group">
											  <label for="nama" class="lb">Tanggal Lahir</label>
											  <input type="text" class="form-control fc" style="height:8vh;" id="tgl_lhr" name="tgl_lhr" disabled value="<?php echo $profil['tgl_lhr'];?>" />
											</div>
											<div class="form-group">
												<label for="nama" class="lb">Alamat</label>
												<textarea  style="height:8vh;" class="form-control fc" id="alamat" name="alamat" ><?php echo $profil['alamat'];?></textarea>
											</div>
											<div class="form-group">
												<label for="nama" class="lb">Gol Darah</label>
												<div class="form-group fc">
												  <select class="form-control" id="gol_darah" name="gol_darah">
													<option value="A" <?php if($profil['gol_dar'] == "A"){ echo "selected";}?> >A</option>												
													<option value="B" <?php if($profil['gol_dar'] == "B"){ echo "selected";}?>>B</option>												
													<option value="AB" <?php if($profil['gol_dar'] == "AB"){ echo "selected";}?>>AB</option>												
													<option value="O" <?php if($profil['gol_dar'] == "O"){ echo "selected";}?>>O</option>												
												  </select>
												</div>
											</div>
											<div class="form-group">
												<label for="nama" class="lb">Provinsi</label>
												<div class="form-group fc">
												  <select onclick="javascript:cek();" class="form-control" id="provinsi" name="provinsi">
												  <?php
												  foreach($prov as $provinsi){ ?>
													<option value="<?php echo $provinsi['id']; ?>" <?php if($provinsi['name'] == $profil['prov']){ echo "selected";}?> ><?php echo $provinsi['name'];?></option>	
												  <?php
												  }
												  ?>
												  
												  </select>
												</div>
											</div>
											<div class="form-group">
												<label for="nama" class="lb">Kota</label>
												<div class="form-group fc">
												  <select class="form-control" id="kota" name="kota">
													<?php
													if($profil['kab'] == "" || $profil['kab'] == "0"){ ?>
													<option>-- Pilih Provinsi --</option>	
													<?php
													}else { ?>
													<option><?php echo $profil['kab'];?></option>	
													<?php
													}
													?>
												  </select>
												</div>
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Kode Pos</label>
											  <input type="number" class="form-control fc" id="kode_pos" name="kode_pos" value="<?php echo $profil['kode_pos'];?>" required />
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Telp Rumah</label>
											  <input type="text" class="form-control fc" id="telp_rumah" name="telp_rumah" value="<?php echo $profil['tlp_rmh'];?>" required />
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">No HP</label>
											  <input type="number" class="form-control fc" id="hp" name="hp" value="<?php echo $profil['hp'];?>" required />
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Email</label>
											  <input type="text" class="form-control fc" id="email" name="email" value="<?php echo $profil['email'];?>" required />
											</div>
											<div class="form-group">
												<label for="nama" class="lb">Agama</label>
												<div class="form-group fc">
												  <select class="form-control" id="agama" name="agama">
													<option <?php  if($profil['agama'] == '1'){ echo "selected";}?> value="1">Islam</option>
													<option <?php  if($profil['agama'] == '2'){ echo "selected";}?> value="2">Katholik</option>
													<option <?php  if($profil['agama'] == '3'){ echo "selected";}?> value="3">Kristen</option>
													<option <?php  if($profil['agama'] == '4'){ echo "selected";}?> value="4">Hindu</option>
													<option <?php  if($profil['agama'] == '5'){ echo "selected";}?> value="5">Buddha</option>
												  </select>
												</div>
											</div>
											<div class="form-group">
												<label for="nama" class="lb" >Kewarganegaraan</label>
												<div class="form-group fc">
												  <select class="form-control" id="warganegara" name="warganegara">
													<option value="1" <?php if($profil['warganegara'] == "1"){ echo "selected";}?> >Warga Negara Indonesia</option>
													<option value="2" <?php if($profil['warganegara'] == "2"){ echo "selected";}?> >Warga Negara Asing</option>
												  </select>
												</div>
											</div>
											<button type="submit" class="btn btn-success pull-right next-step" >Simpan</button>
											</form>
										</div>
                                        <div role="tabpanel" class="tab-pane" id="profile">
										<form action="<?php echo base_url()?>home/save2" method="post">
											<div class="form-group">
											  <label for="usr" class="lb">Nama Ayah/Wali</label>
											  <input type="text" class="form-control fc2 w"  id="nama_ayah" name="nama_ayah" value="<?php echo $profil['nama_ayah'];?>" required />
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Pekerjaan Ayah/Wali</label>
											  <div class="checkbox fc2" >
													<label class="checkbox-inline"><input type="radio" name="peker_ayah" class="peker_ayah1" onclick="javascript:as();" <?php if($profil['peker_ayah'] == "" || $profil['peker_ayah'] == "PNS"){echo "checked"; $o=1;} ?>  value="PNS">PNS</label>
													<label class="checkbox-inline"><input type="radio" name="peker_ayah" class="peker_ayah1" onclick="javascript:as();" <?php if($profil['peker_ayah'] == "Pegawai Swasta"){echo "checked";$o=1;} ?> value="Pegawai Swasta">Pegawai Swasta</label>
													<label class="checkbox-inline"><input type="radio" name="peker_ayah" class="peker_ayah1" onclick="javascript:as();" <?php if($profil['peker_ayah'] == "Wirausaha"){echo "checked";$o=1;} ?> value="Wirausaha">Wirausaha</label>
													<label class="checkbox-inline"><input type="radio" name="peker_ayah" class="peker_ayah1" onclick="javascript:as();" <?php if($profil['peker_ayah'] == "TNI/PORLI"){echo "checked";$o=1;} ?> value="TNI/PORLI">TNI/PORLI</label>
													<label class="checkbox-inline"><input type="radio" name="peker_ayah" id="peker_ayah2" onclick="javascript:as();" value="1" <?php if($o == 0){echo "checked";} ?> >Lainnya</label>
											  </div>
												<div class="form-group" id="ayah_lainnya">
												  <input type="text" class="form-control fc2" name="pekeer_ayah" id="peker_ayah5" style="width:40vh;" placeholder="Lainnya" value="<?php if($o == 0){echo $profil['peker_ayah'];} ?>" />
												</div>
											</div>
											<div class="form-group">
												<label for="nama" class="lb">Penghasilan Ayah</label>
												<div class="form-group fc2">
												  <select class="form-control" id="peng_ayah" name="peng_ayah">
													<option <?php if($profil['peng_ayah'] == "750000"){echo "selected";} ?> value="750000" >500.000 - 1.000.000</option>
													<option <?php if($profil['peng_ayah'] == "1500000"){echo "selected";} ?> value="1500000">1.000.000 - 2.000.000</option>
													<option <?php if($profil['peng_ayah'] == "2400000"){echo "selected";} ?> value="2400000">2.000.000 - 2.750.000</option>
													<option <?php if($profil['peng_ayah'] == "3200000"){echo "selected";} ?> value="3200000">2.750.000 - 3.500.000</option>
													<option <?php if($profil['peng_ayah'] == "4500000"){echo "selected";} ?> value="4500000"> Lebih Dari 3.500.000</option>
												  </select>
												</div>
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Nama Ibu</label>
											  <input type="text" class="form-control fc2 w" id="nama_ibu" name="nama_ibu" value="<?php echo $profil['nama_ibu'];?>" required />
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Pekerjaan Ibu</label>
											  <div class="checkbox fc2">
													<label class="checkbox-inline"><input type="radio" name="peker_ibu" onclick="javascript:ibu();" <?php if($profil['peker_ibu'] == "" || $profil['peker_ibu'] == "PNS"){echo "checked"; $p=1;} ?> value="PNS">PNS</label>
													<label class="checkbox-inline"><input type="radio" name="peker_ibu" onclick="javascript:ibu();" <?php if($profil['peker_ibu'] == "Pegawai Swasta"){echo "checked"; $p=1;} ?> value="Pegawai Swasta">Pegawai Swasta</label>
													<label class="checkbox-inline"><input type="radio" name="peker_ibu" onclick="javascript:ibu();" <?php if($profil['peker_ibu'] == "Wirausaha"){echo "checked"; $p=1;} ?> value="Wirausaha">Wirausaha</label>
													<label class="checkbox-inline"><input type="radio" name="peker_ibu" onclick="javascript:ibu();" <?php if($profil['peker_ibu'] == "TNI/PORLI"){echo "checked"; $p=1;} ?> value="TNI/PORLI">TNI/PORLI</label>
													<label class="checkbox-inline"><input type="radio" name="peker_ibu" id="peker_ibu" onclick="javascript:ibu();" value="1" <?php if($p == 0){echo "checked";} ?> >Lainnya</label>
											  </div>
												<div class="form-group" id="ibu_lainnya">
												  <input type="text" class="form-control fc2" style="width:20vw;margin-left:13vw; " placeholder="Lainnya" id="peker_ibu5" name="pekeer_ibu" value="<?php if($p == 0){echo $profil['peker_ibu'];} ?>" />
												</div>
											</div>
											<div class="form-group">
												<label for="nama" class="lb">Penghasilan Ibu</label>
												<div class="form-group fc2">
												  <select class="form-control" id="peng_ibu" name="peng_ibu">
													<option <?php if($profil['peng_ibu'] == "750000"){echo "selected";} ?> value="750000" >500.000 - 1.000.000</option>
													<option <?php if($profil['peng_ibu'] == "1500000"){echo "selected";} ?> value="1500000">1.000.000 - 2.000.000</option>
													<option <?php if($profil['peng_ibu'] == "2400000"){echo "selected";} ?> value="2400000">2.000.000 - 2.750.000</option>
													<option <?php if($profil['peng_ibu'] == "3200000"){echo "selected";} ?> value="3200000">2.750.000 - 3.500.000</option>
													<option <?php if($profil['peng_ibu'] == "4500000"){echo "selected";} ?> value="4500000"> Lebih Dari 3.500.000</option>
												  </select>
												</div>
											</div>
											
											<div class="form-group">
												<label for="nama" class="lb">Alamat</label>
												<textarea  style="height:8vh;"class="form-control fc2 w" name="alamat_ortu" ><?php echo $profil['alamat_ortu'];?></textarea>
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Jumlah Anggota KK</label>
											  <input type="text" class="form-control fc2 w" id="saudara" name="saudara" value="<?php echo $profil['anggota'];?>" required />
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">No HP Orang Tua</label>
											  <input type="text" class="form-control fc2 w" name="hp_ortu" value="<?php echo $profil['hp_ortu'];?>" required >
											</div>
											
											<button type="submit" class="btn btn-success pull-right" >Simpan</button>
											</form>
										</div>
                                        <div role="tabpanel" class="tab-pane" id="messages">
											<form action="<?php echo base_url()?>home/save3" method="post">
											<div class="form-group">
											  <label for="usr" style=" float:left;">Pilih UKT</label>
											  <div class="checkbox" style="margin-left:13vw;" >
													<label class="checkbox-inline"><input type="radio" name="ukt" id="ukt1" onclick="javascript:op();" <?php if($profil['pilih_ukt'] == "ukt1"){echo "checked"; } ?> value="ukt1" >UKT 1 <a style="color:red;">(harus mempunyai KJS/Kartu Miskin)</a></label><br/><br/>
													<label class="checkbox-inline"><input type="radio" name="ukt" id="ukt5" data-toggle="tooltip" data-placement="right" title="Tooltip on right" onclick="javascript:op();" <?php if($profil['pilih_ukt'] == "ukt5"){echo "checked"; } ?> value="ukt5"><a data-toggle="tooltip" data-placement="right" title="Tooltip on right" style="text-decoration:none;">UKT 5</a></label><br/><br/>
													<label class="checkbox-inline"><input type="radio" name="ukt" id="ukt3" data-toggle="tooltip" data-placement="right" title="Tooltip on right" onclick="javascript:op();" <?php if($profil['pilih_ukt'] == "" || $profil['pilih_ukt'] == "dll"){echo "checked"; } ?>  value="dll"><a data-toggle="tooltip" data-placement="right" title="Tooltip on right" style="text-decoration:none;">Dll</a></label>
											  </div>
											</div>

											<div class="form-group">
											  <label for="usr" class="lb">Pembayaran PBB/Tahun</label>
											  <div class="fc2 w">
												  <div class="form-group" >
												  <input type="text" class="form-control" id="pbb" name="pbb" value="<?php echo $profil['pbb'];?>" required />
												  </div>
											  </div>
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Daya PLN</label>
											  <div class="fc2 w">
												  <div class="form-group" >
												  <input type="text" class="form-control" style="width:50%;float:left;"id="pln" name="pln" value="<?php echo $profil['pln'];?>" required /><div style="margin-left:52%;padding-top:.5vw;font-weight:bold;">VA</div>
												  </div>
											  </div>
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Luas Tanah</label>
											  <div class="fc2 w">
												  <div class="form-group" >
												  <input type="text" class="form-control" style="width:50%;float:left;" id="luas_tanah" name="luas_tanah" value="<?php echo $profil['luas_tanah'];?>" required /><div style="margin-left:52%;padding-top:.5vw;font-weight:bold;">M<sup>2</sup></div>											  
												  </div>											  
											   </div>											
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Luas Bangunan</label>											  
												<div class="fc2 w">
												  <div class="form-group" >
													<input type="text" class="form-control" style="width:50%;float:left;" id="luas_bangunan" name="luas_bangunan" value="<?php echo $profil['luas_bangunan'];?>" required /><div style="margin-left:52%;padding-top:.5vw;font-weight:bold;">M<sup>2</sup></div>
												  </div>											  
												</div>
											</div>	
											<div class="form-group">
											  <label for="usr" class="lb">Pembayaran PDAM/Bulan</label>											  
												<div class="fc2 w">
												  <div class="form-group" >
													<input type="text" class="form-control" id="pdam" name="pdam" value="<?php echo $profil['pdam'];?>" required />
												  </div>											  
												</div>
											</div>											
											<div class="form-group">
											  <label for="usr" class="lb">Kepemilikan</label>
											  <div class="checkbox fc2">
													<label class="checkbox-inline"><input type="radio" name="kepemilikan" <?php if($profil['kepemilikan'] == "" || $profil['kepemilikan'] == "Sendiri"){echo "checked"; } ?> value="Sendiri" >Sendiri</label>
													<label class="checkbox-inline"><input type="radio" name="kepemilikan" <?php if($profil['kepemilikan'] == "Sewa"){echo "checked"; } ?> value="Sewa">Sewa</label>
													<label class="checkbox-inline"><input type="radio" name="kepemilikan" <?php if($profil['kepemilikan'] == "Menumpang"){echo "checked"; } ?> value="Menumpang">Menumpang</label>
													<label class="checkbox-inline"><input type="radio" name="kepemilikan" <?php if($profil['kepemilikan'] == "Tidak Memiliki"){echo "checked"; } ?> value="Tidak Memiliki">Tidak Memiliki</label>
											  </div>
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Sumber Listrik</label>
											  <div class="checkbox fc2" >
													<label class="checkbox-inline"><input type="radio" name="listrik" <?php if($profil['sum_listrik'] == "" || $profil['sum_listrik'] == "PLN"){echo "checked"; } ?> value="PLN">PLN</label>
													<label class="checkbox-inline"><input type="radio" name="listrik" <?php if($profil['sum_listrik'] == "Genset Mandiri"){echo "checked"; } ?> value="Genset Mandiri">Genset Mandiri</label>
													<label class="checkbox-inline"><input type="radio" name="listrik" <?php if($profil['sum_listrik'] == "PLN dan Genset"){echo "checked"; } ?>  value="PLN dan Genset">PLN dan Genset</label>
													<label class="checkbox-inline"><input type="radio" name="listrik" <?php if($profil['sum_listrik'] == "Tidak Ada"){echo "checked"; } ?>  value="Tidak Ada">Tidak Ada</label>
											  </div>
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Sumber Air</label>
											  <div class="checkbox fc2" >
													<label class="checkbox-inline"><input type="radio" name="sum_air" <?php if($profil['sum_air'] == "" || $profil['sum_air'] == "PDAM"){echo "checked"; } ?>  value="PDAM">PDAM</label> 
													<label class="checkbox-inline"><input type="radio" name="sum_air" <?php if($profil['sum_air'] == "Kemasan"){echo "checked"; } ?> value="Kemasan">Kemasan</label>
													<label class="checkbox-inline"><input type="radio" name="sum_air" <?php if($profil['sum_air'] == "Sumur"){echo "checked"; } ?> value="Sumur">Sumur</label>
													<label class="checkbox-inline"><input type="radio" name="sum_air" <?php if($profil['sum_air'] == "Sungai/Mata Air Gunung"){echo "checked"; } ?> value="Sungai/Mata Air Gunung">Sungai/Mata Air Gunung</label>
											  </div>
											</div>
											
											<button type="submit" class="btn btn-success pull-right" >Simpan</button>
											</form>
										</div>
                                        <div role="tabpanel" class="tab-pane" id="settings">
										<form action="<?php echo base_url()?>home/upload" method="post" id="upload_all" enctype="multipart/form-data">
											<div class="form-group v">
											  <label for="usr" class="lb">Upload Foto </br><p style="color:blue;font-size:1.9vh">Max 1Mb</p></label>
													<div id="upload_foto" class="input-group image-preview preview fc3">
													<input type="text" class="form-control form-control2 image-preview-filename preview-filename"> <!-- don't give a name === doesn't send on POST/GET -->
													<span class="input-group-btn">
														<!-- image-preview-clear button -->
														<button type="button" class="btn btn-default image-preview-clear preview-clear" style="display:none;">
															<span class="glyphicon glyphicon-remove"></span>Hapus
														</button>
														<!-- image-preview-input -->
														<div class="btn btn-default image-preview-input preview-input">
														 <span class="glyphicon glyphicon-folder-open"></span>
															<span class="image-preview-input-title preview-input-title">Pilih File</span>
															<input type="file" accept="image/png, image/jpeg, image/gif" name="foto" id="foto" /> <!-- rename it -->
														</div>
													</span>	
													</div>
													<div id="foto_foto" class="f">
														<img class="zoom" src="<?php echo base_url();?>assets/foto/<?php echo $profil['foto'];?>"/>
														<a href="javascript:ubah('upload_foto','foto_foto');"  class="btn btn-warning n">Ubah Foto</a>
													</div>
											</div>
											
											<div class="form-group" id="jks">
											  <label for="usr" class="lb">Karu KJS</br><p style="color:blue;font-size:1.9vh">Max 1Mb</p></label>
													<div id="upload_kjs1" class="input-group image-preview preview1 fc3">
													<input type="text" class="form-control form-control2 image-preview-filename preview-filename9"> <!-- don't give a name === doesn't send on POST/GET -->
													<span class="input-group-btn" >
														<!-- image-preview-clear button -->
														<button type="button" class="btn btn-default image-preview-clear preview-clear9" style="display:none;">
															<span class="glyphicon glyphicon-remove"></span>Hapus
														</button>
														<!-- image-preview-input -->
														<div class="btn btn-default image-preview-input preview-input9">
														 <span class="glyphicon glyphicon-folder-open"></span>
															<span class="image-preview-input-title preview-input-title9">Pilih File</span>
															<input type="file" accept="image/png, image/jpeg, image/gif" name="kjs" id="kjs" /> <!-- rename it -->
														</div>
															
													</span>	
													</div>
													<div id="foto_kjs" class="f">
														<img class="zoom" src="<?php echo base_url();?>assets/foto/<?php echo $profil['upload_kjs'];?>"/>
														<a href="javascript:ubah('upload_kjs1','foto_kjs');" class="btn btn-warning n">Ubah Foto</a>
													</div>
												
											</div>
											<br/>
											<div class="form-group">
											  <label for="usr" class="lb">Bukti Pembayaran PBB</br><p style="color:blue;font-size:1.9vh">Max 1Mb</p></label>														
												<div id="upload_pbb"  class="input-group image-preview preview1 fc3">
													<input type="text" class="form-control form-control2  image-preview-filename preview-filename1"> <!-- don't give a name === doesn't send on POST/GET -->
													<span class="input-group-btn" >
														<!-- image-preview-clear button -->
														<button type="button" class="btn btn-default image-preview-clear preview-clear1" style="display:none;">
															<span class="glyphicon glyphicon-remove"></span>Hapus
														</button>
														<!-- image-preview-input -->
														<div class="btn btn-default image-preview-input preview-input1">
														 <span class="glyphicon glyphicon-folder-open"></span>
															<span class="image-preview-input-title preview-input-title1">Pilih File</span>
															<input type="file" accept="image/png, image/jpeg, image/gif" name="pbb" id="pbb" /> <!-- rename it -->
														</div>
															
													</span>	
												</div>
													<div id="foto_pbb" class="f">
														<img class="zoom" src="<?php echo base_url();?>assets/foto/<?php echo $profil['upload_pbb'];?>"/>
														<a href="javascript:ubah('upload_pbb','foto_pbb');" class="btn btn-warning n">Ubah Foto</a>
													</div>
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">STNK / BPKB</br><p style="color:blue;font-size:1.9vh">Max 1Mb</p></label>
												<div id="upload_stnk"  class="input-group image-preview preview1 fc3">
													<input type="text" class="form-control form-control2 image-preview-filename preview-filename2" > <!-- don't give a name === doesn't send on POST/GET -->
													<span class="input-group-btn" >
														<!-- image-preview-clear button -->
														<button type="button" class="btn btn-default image-preview-clear preview-clear2" style="display:none;">
															<span class="glyphicon glyphicon-remove"></span>Hapus
														</button>
														<!-- image-preview-input -->
														<div class="btn btn-default image-preview-input preview-input2">
														 <span class="glyphicon glyphicon-folder-open"></span>
															<span class="image-preview-input-title preview-input-title2">Pilih File</span>
															<input type="file" accept="image/png, image/jpeg, image/gif" name="stnk" id="stnk"/> <!-- rename it -->
														</div>
													</span>	
												</div>
													<div id="foto_stnk" class="f">
														<img class="zoom" src="<?php echo base_url();?>assets/foto/<?php echo $profil['upload_stnk'];?>"/>
														<a href="javascript:ubah('upload_stnk','foto_stnk');" class="btn btn-warning n">Ubah Foto</a>
													</div>
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Foto Atap Rumah</br><p style="color:blue;font-size:1.9vh">Max 1Mb</p></label>
												<div id="upload_atap" class="input-group image-preview preview1 fc3">
													<input type="text" class="form-control form-control2 image-preview-filename preview-filename3" > <!-- don't give a name === doesn't send on POST/GET -->
													<span class="input-group-btn" >
														<!-- image-preview-clear button -->
														<button type="button" class="btn btn-default image-preview-clear preview-clear3" style="display:none;">
															<span class="glyphicon glyphicon-remove"></span>Hapus
														</button>
														<!-- image-preview-input -->
														<div class="btn btn-default image-preview-input preview-input3">
														 <span class="glyphicon glyphicon-folder-open"></span>
															<span class="image-preview-input-title preview-input-title3">Pilih File</span>
															<input type="file" accept="image/png, image/jpeg, image/gif" name="atap_rumah" id="atap_rumah"/> <!-- rename it -->
														</div>
													</span>	
												</div>
													<div id="foto_atap" class="f">
														<img class="zoom" src="<?php echo base_url();?>assets/foto/<?php echo $profil['upload_atap'];?>"/>
														<a href="javascript:ubah('upload_atap','foto_atap');" class="btn btn-warning n">Ubah Foto</a>
													</div>
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Foto Lantai Rumah</br><p style="color:blue;font-size:1.9vh">Max 1Mb</p></label>
													<div id="upload_lantai"  class="input-group image-preview preview1 fc3">
													<input type="text" class="form-control form-control2 image-preview-filename preview-filename4" > <!-- don't give a name === doesn't send on POST/GET -->
													<span class="input-group-btn" >
														<!-- image-preview-clear button -->
														<button type="button" class="btn btn-default image-preview-clear preview-clear4" style="display:none;">
															<span class="glyphicon glyphicon-remove"></span>Hapus
														</button>
														<!-- image-preview-input -->
														<div class="btn btn-default image-preview-input preview-input4">
														 <span class="glyphicon glyphicon-folder-open"></span>
															<span class="image-preview-input-title preview-input-title4">Pilih File</span>
															<input type="file" accept="image/png, image/jpeg, image/gif" name="lantai_rumah" id="lantai_rumah"/> <!-- rename it -->
														</div>
													</span>		
													</div>
													<div id="foto_lantai" class="f">
														<img class="zoom" src="<?php echo base_url();?>assets/foto/<?php echo $profil['upload_lantai'];?>"/>
														<a href="javascript:ubah('upload_lantai','foto_lantai');" class="btn btn-warning n">Ubah Foto</a>
													</div>
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Bukti Pembayaran PLN</br><p style="color:blue;font-size:1.9vh">Max 1Mb</p></label>
													<div id="upload_pln"  class="input-group image-preview preview1 fc3">
													<input type="text" class="form-control form-control2 image-preview-filename preview-filename5" > <!-- don't give a name === doesn't send on POST/GET -->
													<span class="input-group-btn" >
														<!-- image-preview-clear button -->
														<button type="button" class="btn btn-default image-preview-clear preview-clear5" style="display:none;">
															<span class="glyphicon glyphicon-remove"></span>Hapus
														</button>
														<!-- image-preview-input -->
														<div class="btn btn-default image-preview-input preview-input5">
														 <span class="glyphicon glyphicon-folder-open"></span>
															<span class="image-preview-input-title preview-input-title5">Pilih File</span>
															<input type="file" accept="image/png, image/jpeg, image/gif" name="pln" id="pln"/> <!-- rename it -->
														</div>
													</span>	
													</div>
													<div id="foto_pln" class="f">
														<img class="zoom" src="<?php echo base_url();?>assets/foto/<?php echo $profil['upload_pln'];?>"/>
														<a href="javascript:ubah('upload_pln','foto_pln');" class="btn btn-warning n">Ubah Foto</a>
													</div>
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Bukti Pembayaran PDAM</br><p style="color:blue;font-size:1.9vh">Max 1Mb</p></label>
													<div id="upload_pdam"  class="input-group image-preview preview1 fc3">
													<input type="text" class="form-control form-control2 image-preview-filename preview-filename6" > <!-- don't give a name === doesn't send on POST/GET -->
													<span class="input-group-btn" >
														<!-- image-preview-clear button -->
														<button type="button" class="btn btn-default image-preview-clear preview-clear6" style="display:none;">
															<span class="glyphicon glyphicon-remove"></span>Hapus
														</button>
														<!-- image-preview-input -->
														<div class="btn btn-default image-preview-input preview-input6">
														 <span class="glyphicon glyphicon-folder-open"></span>
															<span class="image-preview-input-title preview-input-title6">Pilih File</span>
															<input type="file" accept="image/png, image/jpeg, image/gif" name="pdam"/> <!-- rename it -->
														</div>
													</span>	
													</div>
													<div id="foto_pdam" class="f">
														<img class="zoom" src="<?php echo base_url();?>assets/foto/<?php echo $profil['upload_pdam'];?>"/>
														<a href="javascript:ubah('upload_pdam','foto_pdam');" class="btn btn-warning n">Ubah Foto</a>
													</div>
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Bukti Ket Penghasilan/Gaji Ayah</br><p style="color:blue;font-size:1.9vh">Max 1Mb</p></label>
													<div id="upload_gaji"  class="input-group image-preview preview1 fc3">
													<input type="text" class="form-control form-control2 image-preview-filename preview-filename7" > <!-- don't give a name === doesn't send on POST/GET -->
													<span class="input-group-btn" >
														<!-- image-preview-clear button -->
														<button type="button" class="btn btn-default image-preview-clear preview-clear7" style="display:none;">
															<span class="glyphicon glyphicon-remove"></span>Hapus
														</button>
														<!-- image-preview-input -->
														<div class="btn btn-default image-preview-input preview-input7">
														 <span class="glyphicon glyphicon-folder-open"></span>
															<span class="image-preview-input-title preview-input-title7">Pilih File</span>
															<input type="file" accept="image/png, image/jpeg, image/gif" name="gaji"/> <!-- rename it -->
														</div>
													</span>	
													</div>
													<div id="foto_gaji" class="f">
														<img class="zoom" src="<?php echo base_url();?>assets/foto/<?php echo $profil['upload_gaji_ayah'];?>"/>
														<a href="javascript:ubah('upload_gaji','foto_gaji');" class="btn btn-warning n">Ubah Foto</a>
													</div>
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Bukti Ket Penghasilan/Gaji Ibu</br><p style="color:blue;font-size:1.9vh">Max 1Mb</p></label>
												<div id="upload_gaji_ibu"  class="input-group image-preview preview1 fc3">
													<input type="text" class="form-control form-control2 image-preview-filename preview-filename10" > <!-- don't give a name === doesn't send on POST/GET -->
													<span class="input-group-btn" >
														<!-- image-preview-clear button -->
														<button type="button" class="btn btn-default image-preview-clear preview-clear10" style="display:none;">
															<span class="glyphicon glyphicon-remove"></span>Hapus
														</button>
														<!-- image-preview-input -->
														<div class="btn btn-default image-preview-input preview-input10">
														 <span class="glyphicon glyphicon-folder-open"></span>
															<span class="image-preview-input-title preview-input-title10">Pilih File</span>
															<input type="file" accept="image/png, image/jpeg, image/gif" name="gaji_ibu"/> <!-- rename it -->
														</div>
													</span>	
													</div>
													<div id="foto_gaji_ibu" class="f">
														<img class="zoom" src="<?php echo base_url();?>assets/foto/<?php echo $profil['upload_gaji_ibu'];?>"/>
														<a href="javascript:ubah('upload_gaji_ibu','foto_gaji_ibu');" class="btn btn-warning n">Ubah Foto</a>
													</div>
											</div>
											<div class="form-group">
											  <label for="usr" class="lb">Kartu Keluarga</br><p style="color:blue;font-size:1.9vh">Max 1Mb</p></label>
												<div id="upload_kk"  class="input-group image-preview preview1 fc3">
													<input type="text" class="form-control form-control2 image-preview-filename preview-filename8" > <!-- don't give a name === doesn't send on POST/GET -->
													<span class="input-group-btn" >
														<!-- image-preview-clear button -->
														<button type="button" class="btn btn-default image-preview-clear preview-clear8" style="display:none;">
															<span class="glyphicon glyphicon-remove"></span>Hapus
														</button>
														<!-- image-preview-input -->
														<div class="btn btn-default image-preview-input preview-input8">
														 <span class="glyphicon glyphicon-folder-open"></span>
															<span class="image-preview-input-title preview-input-title8">Pilih File</span>
															<input type="file" accept="image/png, image/jpeg, image/gif" name="kk" id="kk"/> <!-- rename it -->
														</div>
													</span>	
													</div>
													<div id="foto_kk" class="f">
														<img class="zoom" src="<?php echo base_url();?>assets/foto/<?php echo $profil['upload_kk'];?>"/>
														<a href="javascript:ubah('upload_kk','foto_kk');" class="btn btn-warning n">Ubah Foto</a>
													</div>
											</div>
												<button type="submit" class="btn btn-success pull-right" >Simpan</button>
											</form>
										</div>
                                    </div>
								</div>
								</section>
								</div>
								</div>
							</div>
						</div>
					</div>
				</div>
		</div>
	
	<!--div class="footer">
	
	</div>-->
	</div>
  </body>
  <script>
   $(document).on('click', '#close-preview', function(){ 
    $('.image-preview').popover('hide');
    // Hover befor close the preview    
});

// $(function() {
    // // Create the close button
    // var closebtn = $('<button/>', {
        // type:"button",
        // text: 'x',
        // id: 'close-preview',
        // style: 'font-size: initial;',
    // });
    // closebtn.attr("class","close pull-right");

    // // Clear event
    // $('.image-preview-clear').click(function(){
        // $('.image-preview').attr("data-content","").popover('hide');
        // $('.image-preview-filename').val("");
        // $('.image-preview-clear').hide();
        // $('.image-preview-input input:file').val("");
        // $(".image-preview-input-title").text("Cari File"); 
    // }); 
    // // Create the preview image
    // $(".image-preview-input input:file").change(function (){     
        // var img = $('<img/>', {
            // id: 'dynamic',
            // width:250,
            // height:200
        // });      
        // var file = this.files[0];
        // var reader = new FileReader();
        // // Set preview image into the popover data-content
        // reader.onload = function (e) {
            // $(".image-preview-input-title").text("Ubah");
            // $(".image-preview-clear").show();
            // $(".image-preview-filename").val(file.name);
        // }        
        // reader.readAsDataURL(file);
    // });  
// });
$(function() {
    // Create the close button
    var closebtn = $('<button/>', {
        type:"button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");

    // Clear event
    $('.preview-clear').click(function(){
        $('.preview').attr("data-content","").popover('hide');
        $('.preview-filename').val("");
        $('.preview-clear').hide();
        $('.preview-input input:file').val("");
        $(".preview-input-title").text("Cari File"); 
    }); 
    // Create the preview image
    $(".preview-input input:file").change(function (){     
        var img = $('<img/>', {
            id: 'dynamic',
            width:250,
            height:200
        });      
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".preview-input-title").text("Ubah");
            $(".preview-clear").show();
            $(".preview-filename").val(file.name);
        }        
        reader.readAsDataURL(file);
    });  
});

$(function() {
    // Create the close button
    var closebtn = $('<button/>', {
        type:"button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");

    // Clear event
    $('.preview-clear1').click(function(){
        $('.preview1').attr("data-content","").popover('hide');
        $('.preview-filename1').val("");
        $('.preview-clear1').hide();
        $('.preview-input1 input:file').val("");
        $(".preview-input-title1").text("Cari File"); 
    }); 
    // Create the preview image
    $(".preview-input1 input:file").change(function (){     
        var img = $('<img/>', {
            id: 'dynamic',
            width:250,
            height:200
        });      
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".preview-input-title1").text("Ubah");
            $(".preview-clear1").show();
            $(".preview-filename1").val(file.name);
        }        
        reader.readAsDataURL(file);
    });  
});


$(function() {
    // Create the close button
    var closebtn = $('<button/>', {
        type:"button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");

    // Clear event
    $('.preview-clear2').click(function(){
        $('.preview2').attr("data-content","").popover('hide');
        $('.preview-filename2').val("");
        $('.preview-clear2').hide();
        $('.preview-input2 input:file').val("");
        $(".preview-input-title2").text("Cari File"); 
    }); 
    // Create the preview image
    $(".preview-input2 input:file").change(function (){     
        var img = $('<img/>', {
            id: 'dynamic',
            width:250,
            height:200
        });      
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".preview-input-title2").text("Ubah");
            $(".preview-clear2").show();
            $(".preview-filename2").val(file.name);
        }        
        reader.readAsDataURL(file);
    });  
});
$(function() {
    // Create the close button
    var closebtn = $('<button/>', {
        type:"button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");

    // Clear event
    $('.preview-clear3').click(function(){
        $('.preview3').attr("data-content","").popover('hide');
        $('.preview-filename3').val("");
        $('.preview-clear3').hide();
        $('.preview-input3 input:file').val("");
        $(".preview-input-title3").text("Cari File"); 
    }); 
    // Create the preview image
    $(".preview-input3 input:file").change(function (){     
        var img = $('<img/>', {
            id: 'dynamic',
            width:250,
            height:200
        });      
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".preview-input-title3").text("Ubah");
            $(".preview-clear3").show();
            $(".preview-filename3").val(file.name);
        }        
        reader.readAsDataURL(file);
    });  
});
$(function() {
    // Create the close button
    var closebtn = $('<button/>', {
        type:"button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");

    // Clear event
    $('.preview-clear4').click(function(){
        $('.preview4').attr("data-content","").popover('hide');
        $('.preview-filename4').val("");
        $('.preview-clear4').hide();
        $('.preview-input4 input:file').val("");
        $(".preview-input-title4").text("Cari File"); 
    }); 
    // Create the preview image
    $(".preview-input4 input:file").change(function (){     
        var img = $('<img/>', {
            id: 'dynamic',
            width:250,
            height:200
        });      
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".preview-input-title4").text("Ubah");
            $(".preview-clear4").show();
            $(".preview-filename4").val(file.name);
        }        
        reader.readAsDataURL(file);
    });  
});
$(function() {
    // Create the close button
    var closebtn = $('<button/>', {
        type:"button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");

    // Clear event
    $('.preview-clear5').click(function(){
        $('.preview5').attr("data-content","").popover('hide');
        $('.preview-filename5').val("");
        $('.preview-clear5').hide();
        $('.preview-input5 input:file').val("");
        $(".preview-input-title5").text("Cari File"); 
    }); 
    // Create the preview image
    $(".preview-input5 input:file").change(function (){     
        var img = $('<img/>', {
            id: 'dynamic',
            width:250,
            height:200
        });      
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".preview-input-title5").text("Ubah");
            $(".preview-clear5").show();
            $(".preview-filename5").val(file.name);
        }        
        reader.readAsDataURL(file);
    });  
});
$(function() {
    // Create the close button
    var closebtn = $('<button/>', {
        type:"button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");

    // Clear event
    $('.preview-clear6').click(function(){
        $('.preview6').attr("data-content","").popover('hide');
        $('.preview-filename6').val("");
        $('.preview-clear6').hide();
        $('.preview-input6 input:file').val("");
        $(".preview-input-title6").text("Cari File"); 
    }); 
    // Create the preview image
    $(".preview-input6 input:file").change(function (){     
        var img = $('<img/>', {
            id: 'dynamic',
            width:250,
            height:200
        });      
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".preview-input-title6").text("Ubah");
            $(".preview-clear6").show();
            $(".preview-filename6").val(file.name);
        }        
        reader.readAsDataURL(file);
    });  
});
$(function() {
    // Create the close button
    var closebtn = $('<button/>', {
        type:"button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");

    // Clear event
    $('.preview-clear7').click(function(){
        $('.preview7').attr("data-content","").popover('hide');
        $('.preview-filename7').val("");
        $('.preview-clear7').hide();
        $('.preview-input7 input:file').val("");
        $(".preview-input-title7").text("Cari File"); 
    }); 
    // Create the preview image
    $(".preview-input7 input:file").change(function (){     
        var img = $('<img/>', {
            id: 'dynamic',
            width:250,
            height:200
        });      
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".preview-input-title7").text("Ubah");
            $(".preview-clear7").show();
            $(".preview-filename7").val(file.name);
        }        
        reader.readAsDataURL(file);
    });  
});
$(function() {
    // Create the close button
    var closebtn = $('<button/>', {
        type:"button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");

    // Clear event
    $('.preview-clear8').click(function(){
        $('.preview8').attr("data-content","").popover('hide');
        $('.preview-filename8').val("");
        $('.preview-clear8').hide();
        $('.preview-input8 input:file').val("");
        $(".preview-input-title8").text("Cari File"); 
    }); 
    // Create the preview image
    $(".preview-input8 input:file").change(function (){     
        var img = $('<img/>', {
            id: 'dynamic',
            width:250,
            height:200
        });      
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".preview-input-title8").text("Ubah");
            $(".preview-clear8").show();
            $(".preview-filename8").val(file.name);
        }        
        reader.readAsDataURL(file);
    });  
});
$(function() {
    // Create the close button
    var closebtn = $('<button/>', {
        type:"button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");

    // Clear event
    $('.preview-clear9').click(function(){
        $('.preview9').attr("data-content","").popover('hide');
        $('.preview-filename9').val("");
        $('.preview-clear9').hide();
        $('.preview-input9 input:file').val("");
        $(".preview-input-title9").text("Cari File"); 
    }); 
    // Create the preview image
    $(".preview-input9 input:file").change(function (){     
        var img = $('<img/>', {
            id: 'dynamic',
            width:250,
            height:200
        });      
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".preview-input-title9").text("Ubah");
            $(".preview-clear9").show();
            $(".preview-filename9").val(file.name);
        }        
        reader.readAsDataURL(file);
    });  
});
$(function() {
    // Create the close button
    var closebtn = $('<button/>', {
        type:"button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");

    // Clear event
    $('.preview-clear10').click(function(){
        $('.preview10').attr("data-content","").popover('hide');
        $('.preview-filename10').val("");
        $('.preview-clear10').hide();
        $('.preview-input10 input:file').val("");
        $(".preview-input-title10").text("Cari File"); 
    }); 
    // Create the preview image
    $(".preview-input10 input:file").change(function (){     
        var img = $('<img/>', {
            id: 'dynamic',
            width:250,
            height:200
        });      
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".preview-input-title10").text("Ubah");
            $(".preview-clear10").show();
            $(".preview-filename10").val(file.name);
        }        
        reader.readAsDataURL(file);
    });  
});

function cek(){
	var simpan = $('#provinsi').val();
	$('#kota').load("<?php echo base_url(); ?>home/getKota/"+simpan);
}
$(function() {
	
    <?php
	if($profil['upload_pbb'] == ""){ ?>
	$('#foto_pbb').hide();
	$("#upload_pbb").show();
	<?php
	}else { ?>
	$('#foto_pbb').show();
	$("#upload_pbb").hide();
	<?php
	}
	if($profil['upload_stnk'] == ""){ ?>
	$('#foto_stnk').hide();
	$("#upload_stnk").show();
	<?php
	}else { ?>
	$('#foto_stnk').show();
	$("#upload_stnk").hide();
	<?php
	}
	if($profil['upload_atap'] == ""){ ?>
	$('#foto_atap').hide();
	$("#upload_atap").show();
	<?php
	}else { ?>
	$('#foto_atap').show();
	$("#upload_atap").hide();
	<?php
	}
	if($profil['upload_lantai'] == ""){ ?>
	$('#foto_lantai').hide();
	$("#upload_lantai").show();
	<?php
	}else { ?>
	$('#foto_lantai').show();
	$("#upload_lantai").hide();
	<?php
	}
	if($profil['upload_pln'] == ""){ ?>
	$('#foto_pln').hide();
	$("#upload_pln").show();
	<?php
	}else { ?>
	$('#foto_pln').show();
	$("#upload_pln").hide();
	<?php
	}
	if($profil['upload_pdam'] == ""){ ?>
	$('#foto_pdam').hide();
	$("#upload_pdam").show();
	<?php
	}else { ?>
	$('#foto_pdam').show();
	$("#upload_pdam").hide();
	<?php
	}
	if($profil['upload_gaji_ayah'] == ""){ ?>
	$('#foto_gaji').hide();
	$("#upload_gaji").show();
	<?php
	}else { ?>
	$('#foto_gaji').show();
	$("#upload_gaji").hide();
	<?php
	}
	if($profil['upload_gaji_ibu'] == ""){ ?>
	$('#foto_gaji_ibu').hide();
	$("#upload_gaji_ibu").show();
	<?php
	}else { ?>
	$('#foto_gaji_ibu').show();
	$("#upload_gaji_ibu").hide();
	<?php
	}
	if($profil['upload_kjs'] == "1" || $profil['upload_kjs'] == ""){ ?>
	$('#foto_kjs').hide();
	$("#upload_kjs1").show();
	<?php
	}else { ?>
	$('#foto_kjs').show();
	$("#upload_kjs1").hide();
	<?php
	}
	if($profil['upload_kk'] == ""){ ?>
	$('#foto_kk').hide();
	$("#upload_kk").show();
	<?php
	}else { ?>
	$('#foto_kk').show();
	$("#upload_kk").hide();
	<?php
	}
	if($profil['foto'] == "" ){ ?>
	$('#foto_foto').hide();
	$("#upload_foto").show();
	<?php
	}else { ?>
	$('#foto_foto').show();
	$("#upload_foto").hide();
	<?php
	}
	?>
	var ayah = $("#peker_ayah5").val();
	var ibu = $("#peker_ibu5").val();
	if(ayah == ""){
		$("#ayah_lainnya").hide();		
	}
	if(ibu == ""){
		$("#ibu_lainnya").hide();		
	}
	if (document.getElementById('ukt1').checked) {
		$("#upload_all").show();
		$("#jks").show();	  
		$("#upload_kjs1").hide();
		var jks = '<?php echo $profil['upload_kjs'];?>';
		if(jks == "1" || jks == ""){
			$('#foto_kjs').hide();
			$("#upload_kjs1").show();		
		}else {
			$('#foto_kjs').show();
			$("#upload_kjs1").hide();			
		}		
			
	}else if (document.getElementById('ukt5').checked){
		$("#jks").hide();	  
		$("#upload_all").hide();	  
	} else {
		$("#upload_all").show();	  
		$("#jks").hide();	  
	}
	
});

function ubah(nama,nama_foto){
	var upload = "#"+nama;
	var foto = "#"+nama_foto;
	$(foto).hide();
	$(upload).show();
}

function as(){
	if (document.getElementById('peker_ayah2').checked) {
		$("#ayah_lainnya").show();	  
	}else {
		$("#ayah_lainnya").hide();	  
		$("#peker_ayah5").val("");	  
	}
}

function ibu(){
	if (document.getElementById('peker_ibu').checked) {
		$("#ibu_lainnya").show();	  
	}else {
		$("#ibu_lainnya").hide();	  
		$("#peker_ibu5").val("");	  
	}
}

function op(){
	if (document.getElementById('ukt1').checked) {
		$("#upload_all").show();
		$("#jks").show();	  
		var jks = '<?php echo $profil['upload_kjs'];?>';
		if(jks == "1" || jks == ""){
			$('#foto_kjs').hide();
			$("#upload_kjs1").show();		
		}else {
			$('#foto_kjs').show();
			$("#upload_kjs1").hide();			
		}				
	}else if (document.getElementById('ukt5').checked){
		$("#jks").hide();	  
		$("#upload_all").hide();	  
	} else {
		$("#upload_all").show();	  
		$("#jks").hide();	  
	}
}

  </script>
 </html>