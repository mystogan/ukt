<html>
  <head><meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>

  	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/reset.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/css.css" type="text/css "/>

	<!-- DataTables CSS -->
	<link href="<?php echo base_url();?>assets/css/dataTables.bootstrap.css" rel="stylesheet">

	<!-- DataTables Responsive CSS -->
	<link href="<?php echo base_url();?>assets/css/dataTables.responsive.css" rel="stylesheet">

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<!-- CKeditor CSS -->
	<script src="http://cdn.ckeditor.com/4.5.9/standard/ckeditor.js"></script>
	<!-- Custom Theme JavaScript -->
	<script src="<?php echo base_url();?>assets/js/jquery.dataTables.min.js" type="text/javascript" ></script>
	<script src="<?php echo base_url();?>assets/js/dataTables.bootstrap.min.js" type="text/javascript" ></script>
	<script src="<?php echo base_url();?>assets/js/dataTables.responsive.js" type="text/javascript" ></script>

	<script>
	$(document).ready(function() {
		$('#dataTables-example').DataTable({
				responsive: true
		});
	});
	function kode(id){
		$("#kodeMHS").val(id);
	}
	</script>
  </head>
  <body>

		<div class="atas col-xs-12 col-sm-12 col-md-12" >
			<a href="<?php echo base_url();?>atasan">
			<div class="btn btn-default pull-left" style="background-color:#eee;border:none;color:red;"> <span class="glyphicon glyphicon-arrow-left" style="margin-right:.5vw;"></span>Kembali</div>
			</a>
		</div>
	<div class="headerK">
	<div class="container">
		<div class="col-xs-12 col-sm-12 col-md-12" >
			<div class="row">
				<div class="logo"><img src="<?php echo base_url();?>assets/images/aan.png"/></div>
				<div class="judulK">Validasi Pengajuan UKT </br>Mahasiswa Sistem Informasi UIN Sunan Ampel Surabaya</div>
			</div>
		</div>
	</div>
	</div>
	<div class="container">
		<div class="col-xs-12 col-sm-12 col-md-12" >
			<div class="row">
				 <div class="panel panel-green"style="margin-top:2vw;">
		                        <div class="panel-heading jd" style="">
		                            Daftar Nama Pengajuan UKT <?php echo $id;?> Mahasiswa
		                        </div>
		                        <!-- /.panel-heading -->
		                        <div class="panel-body">

		                            <div id="dataTables-example_wrapper" class="dataTables_wrapper from-inline dt-bootstrap no-footer">

		                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
		                                <thead>
		                                    <tr>
		                                        <th style="text-align:center;">Kode</th>
		                                        <th style="text-align:center;">Nama Mahasiswa</th>
		                                        <th style="text-align:center;">Program Studi</th>
		                                        <th style="text-align:center;">Rekomendasi ke 2</th>
		                                        <th style="text-align:center;">Rekomendasi ke 3</th>
		                                        <th style="text-align:center;">Lainnya</th>
		                                        <th style="text-align:center;">Detil</th>
		                                    </tr>
		                                </thead>
		                                <tbody>
											<?php foreach ($all as $Hall){ ?>
											<tr>
		                                        <td style="text-align:center;"><?php echo $Hall['kode'];?></td>
		                                        <td style="text-align:center;"><?php echo $Hall['nama'];?></td>
		                                        <td style="text-align:center;"><?php echo $Hall['prodi'];?></td>
		                                        <td style="text-align:center;">
                    													<form action="<?php echo base_url()?>atasan/kel_ukt2" method="post" >
                                            		<input type="hidden" id="kelompok2" name="kelompok2" value="<?php echo $id;?>" />
                                            		<input type="hidden" id="kode" name="kode" value="<?php echo $Hall['kode'];?>" />
                                            		<input type="hidden" id="kel_ukt2" name="kel_ukt2" value="<?php echo $Hall['kel_2'];?>" />
                                            		<input type="hidden" id="pers_ukt2" name="pers_ukt2" value="<?php echo $Hall['rekom_2'];?>" />
                    														<button type="submit" class="btn btn-success"><?php echo $Hall['rekom_2'];?>% Kelompok UKT ke <?php echo $Hall['kel_2'];?></button>
                    													</form>
                    												</td>
		                                        <td style="text-align:center;">
                    													<form action="<?php echo base_url()?>atasan/kel_ukt3" method="post" >
                    														<input type="hidden" id="kelompok3" name="kelompok3" value="<?php echo $id;?>" />
                                            		<input type="hidden" id="kode" name="kode" value="<?php echo $profil['kode'];?>" />
                                            		<input type="hidden" id="kel_ukt3" name="kel_ukt3" value="<?php echo $Hall['kel_3'];?>" />
                                            		<input type="hidden" id="pers_ukt3" name="pers_ukt3" value="<?php echo $Hall['rekom_3'];?>" />
                    														<button type="submit" class="btn btn-success"><?php echo $Hall['rekom_3'];?>% Kelompok UKT ke <?php echo $Hall['kel_3'];?></button>
                    													</form>
                    												</td>
		                                        <td style="text-align:center;">
                    													<a href="javascript:kode('<?php echo $Hall['kode'];?>');">
                    													<button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">Pilih</button>
                    													</a>
                    												</td>
		                                        <td style="text-align:center;">
													<a type="button" class="btn btn-success" href="<?php echo base_url();?>atasan/detil2/<?php echo $Hall['kode'];?>">Detail</a>
												</td>

		                                    </tr>
											<?php
											}
											?>


		                                </tbody>
		                            </table>

							<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
								  <div class="modal-dialog" role="document">
									<div class="modal-content">
									  <form action="<?php echo base_url()?>atasan/ubahUKT" method="post">
									  <div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" style="text-align:center;"id="myModalLabel">Pilih UKT Mahasiswa</h4>
									  </div>
									  <input type="hidden" name="kelompok21" value="<?php echo $id;?>"  />
									  <input type="hidden" name="kode" id="kodeMHS"  />
									  <div class="modal-body col-xs-12 col-sm-12 col-md-12" style="text-align:center;">
											<div class="col-xs-6 col-sm-4 col-md-4" >
												<div class="radio">
												  <label><input type="radio" name="ukt" value="1">UKT 1</label>
												</div>
											</div>
											<div class="col-xs-6 col-sm-4 col-md-4" >
												<div class="radio">
												  <label><input type="radio" name="ukt" value="2">UKT 2</label>
												</div>
											</div>
											<div class="col-xs-6 col-sm-4 col-md-4" >
												<div class="radio">
												  <label><input type="radio" name="ukt" value="3">UKT 3</label>
												</div>
											</div>
											<div class="col-xs-6 col-sm-4 col-md-4" >
												<div class="radio">
												  <label><input type="radio" name="ukt" value="4" >UKT 4</label>
												</div>
											</div>
											<div class="col-xs-6 col-sm-4 col-md-4" >
												<div class="radio">
												  <label><input type="radio" name="ukt" value="5">UKT 5</label>
												</div>
											</div>
									  </div>
									  <div class="modal-footer"style="margin-top:8vw;">
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										<button type="submit" class="btn btn-primary">Save changes</button>
									  </div>
									  </form>
									</div>
								  </div>
							</div>
		                            <!-- /.table-responsive -->
		                            </div>
		                        </div>
		                        <!-- /.panel-body -->
		                    </div>
		                    <!-- /.panel -->
			</div>
		</div>
	</div>

  </body>
 </html>
