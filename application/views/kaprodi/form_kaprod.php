<html>
  <head><meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Form Kaprodi <?php print_r ($all[0]['prodi']); ?></title>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>assets/images/uin.png" />

  	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/reset.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/css.css" type="text/css "/>

	<!-- DataTables CSS -->
	<link href="<?php echo base_url();?>assets/css/dataTables.bootstrap.css" rel="stylesheet">

	<!-- DataTables Responsive CSS -->
	<link href="<?php echo base_url();?>assets/css/dataTables.responsive.css" rel="stylesheet">

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
	<!-- Custom Theme JavaScript -->
	<script src="<?php echo base_url();?>assets/js/jquery.dataTables.min.js" type="text/javascript" ></script>
	<script src="<?php echo base_url();?>assets/js/dataTables.bootstrap.min.js" type="text/javascript" ></script>
	<script src="<?php echo base_url();?>assets/js/dataTables.responsive.js" type="text/javascript" ></script>

	<script>
	$(document).ready(function() {
		$('#dataTables-example').DataTable({
				responsive: true
		});
	});
	</script>
  </head>
  <body>

		<div class="atas col-xs-12 col-sm-12 col-md-12" >
			<a href="<?php echo base_url()?>uin/logout">
			<div class="btn btn-default pull-right" style="background-color:#eee;border:none;color:red;"> <span class="glyphicon glyphicon-off" style="margin-right:.5vw;"></span>Keluar</div>
			</a>
		</div>
	<div class="headerK">
	<div class="container">
		<div class="col-xs-12 col-sm-12 col-md-12" >
			<div class="row">
				<div class="logo"><img src="<?php echo base_url();?>assets/images/aan.png"/></div>
				<div class="judulK">Validasi Pengajuan UKT </br>Mahasiswa <?php print_r ($all[0]['prodi']); ?> UIN Sunan Ampel Surabaya</div>
			</div>
		</div>
	</div>
	</div>
	<div class="container">
		<div class="col-xs-12 col-sm-12 col-md-12" >
			<div class="row">
				 <div class="panel panel-green"style="margin-top:2vw;">
		                        <div class="panel-heading jd" style="">
		                            Daftar Nama Pengajuan UKT <br/> Mahasiswa <?php print_r ($all[0]['prodi']); ?>
		                        </div>
		                        <!-- /.panel-heading -->
		                        <div class="panel-body">

		                            <div id="dataTables-example_wrapper" class="dataTables_wrapper from-inline dt-bootstrap no-footer">
		                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
		                                <thead>
		                                    <tr>
		                                        <th style="text-align:center;">Kode</th>
		                                        <th style="text-align:center;">Nama Mahasiswa</th>
		                                        <th style="text-align:center;">Program Studi</th>
		                                        <th style="text-align:center;"></th>
		                                    </tr>
		                                </thead>
		                                <tbody>
											<?php foreach ($all as $Hall){ ?>
											<tr>
		                                        <td style="text-align:center;"><?php echo $Hall['kode'];?></td>
		                                        <td style="text-align:center;"><?php echo $Hall['nama'];?></td>
		                                        <td style="text-align:center;"><?php echo $Hall['prodi'];?></td>
		                                        <td style="text-align:center;">
													<?php
													// proses penentuan apakah mahasiswa sudah diproses apa bellum
													  if ($Hall['mhs_finalisasi'] == 0) { ?>
														<a class="btn btn-warning" >Belum Finalisasi</a>
													  <?php
													  }else if ($Hall['ver_tanah'] != '' && $Hall['ver_bangunan'] != '' && $Hall['ver_spd'] != ''
														&& $Hall['ver_atap'] != '' && $Hall['ver_lantai'] != '' && $Hall['ver_pbb'] != ''
														&& $Hall['ver_pln'] != '' && $Hall['ver_pdam'] != '' && $Hall['ver_ayah'] != ''
														&& $Hall['ver_ibu'] != '' && $Hall['ver_anggota'] != ''){
														  if($Hall['pilih_ukt'] == 1){
															  if($Hall['ver_alat_kom'] != '' && $Hall['ver_srt_mrk'] != '' &&
																$Hall['ver_penyakit'] != '' && $Hall['ver_panti_yatim'] != '' &&
																$Hall['ver_surat_asli'] != '' ){
															  ?>
															  <a class="btn btn-default" >Sudah diverifikasi</a>
															  <?php
															  }else {
															  ?>
															  <a class="btn btn-success" href="<?php echo base_url()?>uin/detil/<?php echo $Hall['kode'];?>">Proses</a>
															  <?php																
															  }
														  }else if($Hall['pilih_ukt'] == 2) {
															    if($Hall['ver_tetangga'] != '' && $Hall['ver_sktm'] != '' &&
																$Hall['ver_anakkuliah'] != ''  ){
																	echo '<a class="btn btn-default" >Sudah diverifikasi</a>';
																}else {
																	?>
																	<a class="btn btn-success" href="<?php echo base_url()?>uin/detil/<?php echo $Hall['kode'];?>">Proses</a>
																	<?php
																}
														 
														  }else if($Hall['pilih_ukt'] == 3){
																if($Hall['ver_tetangga'] != ''  ){
																	echo '<a class="btn btn-default" >Sudah diverifikasi</a>';
																}else {
																	?>
																	<a class="btn btn-success" href="<?php echo base_url()?>uin/detil/<?php echo $Hall['kode'];?>">Proses</a>
																	<?php
																}
														  }
														  ?>
													  <?php
													  }else { ?>
														<a class="btn btn-success" href="<?php echo base_url()?>uin/detil/<?php echo $Hall['kode'];?>">Proses</a>

													  <?php
													  }
													
													?>
												</td>
		                                    </tr>
										<?php
										}
										?>

		                                </tbody>
		                            </table>

		                            <!-- /.table-responsive -->
		                            </div>
		                        </div>
		                        <!-- /.panel-body -->
		                    </div>
		                    <!-- /.panel -->
			</div>
		</div>
	</div>

  </body>
 </html>
