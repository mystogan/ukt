<html>
  <head><meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>

  	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/reset.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/css.css" type="text/css "/>

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/proses.js"></script>
	<script src="http://cdn.ckeditor.com/4.5.9/standard/ckeditor.js"></script>
	
  </head>
  <body>
  <div class="headerK">
	<div class="container">
		<div class="col-xs-12 col-sm-12 col-md-12" >
			<div class="row">
				<div class="logo"><img src="<?php echo base_url();?>assets/images/aan.png"/></div>
				<div class="judulK">Pengajuan UKT </br>Mahasiswa Sistem Informasi UIN Sunan Ampel Surabaya</div>
			</div>
		</div>
	</div>
	</div>
	<div class="container">
				<div class="col-xs-12 col-sm-12 col-md-12" style="width:40vw;margin-left:23vw; margin-top:4vw;" >
			<div class="row"align="center">
					<form action="<?php echo base_url();?>atasan/cek" method="POST">
					<div class="modal-header">
						<h2 class="modal-title" >Form Login</h2>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label for="nim" style="text-align:left;">Username</label>
							<input type="text" class="form-control" id="username"  placeholder="Masukkan Username" name="username" required />
						</div>
						<div class="form-group">
							<label for="nama"  style="margin-bottom:10px;">Password</label>
							<input type="password" class="form-control" id="password" placeholder="Masukkan Password" name="password" required />
						</div>
					</div>
					<div class="modal-footer">
						<input style="" class="btn btn-success" type="submit" name="masuk" id="masuk" value="Masuk" />
					</div>
					</form>    
				</div>
			</div>
		</div>
  </body>
 </html>