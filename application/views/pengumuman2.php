<?php 
error_reporting(0);
 foreach($profile as $profil);
 $user = $this->session->userdata('kode');
 $o=0;
 $p=0;
 

 ?>

<html>
  <head><meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
  	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/reset.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/cetak.css" type="text/css "/>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/proses.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	<!-- CKeditor CSS -->
	<script src="<?php echo base_url();?>assets/js/ckeditor.js"></script>
	
  </head>
  <body>
	<div class="header">
		<div class="container">
				<div class="col-xs-12 col-sm-12 col-md-12" >
					<div class="row">
						<div class="head">
							FORM PENGAJUAN UKT</br> Mahasiswa UIN Sunan Ampel Surabaya
						</div>
						<div class="kotak">
							<div class="kanan">
								<div class="foto"><img src="<?php echo base_url();?>assets/foto/<?php echo $profil['foto'];?>"/></div>
								<div class="tempatNama">
									<div class="nama1">Nama :</div>
									<div class="nama"><?php echo $profil['nama'];?></div>
									
								</div>
								<div class="tempatNama">
									<div class="nama1">Kode :</div>
									<div class="nama"><?php echo $profil['kode'];?></div>
									
								</div>
								<div class="tempatNama">
									<div class="nama1">Jenis Kelamin :</div>
									<div class="nama"><?php 
									if($profil['jk'] == "L"){
										echo "Laki-Laki";
									}else {
										echo "Perempuan";
									}
									?></div>
								</div>
								<div class="tempatNama">
									<div class="nama1">Program Studi :</div>
									<div class="nama"><?php echo $profil['nama_prodi'];?></div>
								</div>
								<div class="tempatNama">
									<div class="nama1">Tahun Masuk :</div>
									<div class="nama"><?php echo $profil['tahun_masuk'];?></div>
								</div><!-- 
								<div class="tempatNama">
									<div class="nama1">Print Pengumuman :</div>
									<div class="nama"><a style="cursor:pointer">PDF <span class="glyphicon glyphicon-print" style="color:red"></span></a></div>
								</div> -->
								<div class="tempatNama">	
									<a href="<?php echo base_url()?>home/logout" class="btn btn-danger" style="margin-top:2vw;font-size:1.5vw;">Logout</a>
								</div>
							</div>
							<div class="kiri">
								<div class="col-xs-12 col-sm-12 col-md-12 ">
								<div class="kotJ">
									<!--<a href="<?php echo base_url()?>home/logout" class="hp3" >Logout</a>-->
									<div class="jud col-xs-12 col-sm-12 col-md-12 ">
										<div class="jud1">
											<?php print_r ($tgl[0]['tgl']);?>
											
										</div>
									</div>
									<div class="jud col-xs-12 col-sm-12 col-md-12 ">
											<div class="jud">
												PENGUMUMAN UMPTKAIN
											</div>
									</div>
								</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-12 ">
									<div class="content" style="padding-top:6vw;">
<style>
	.nominal1 {
    text-align: center;
    font-weight: bold;
    font-size: 1.5vw;
    border-bottom: 1px solid #000;
    color: red;
}
</style>
	
										<?php
										
										if($profile[0]['prodi'] == "ZW" && $kolom == "kel4"){ ?>
										
											<div class="nominal1"> Ralat nominal Uang Kuliah Tunggal (UKT) yang semula tertulis Rp.190.000 yang benar Rp.1.900.000. Mohon maaf atas ketidaknyamanan ini.Terima kasih </div>
										<?php
										}
										?>
										<div class="textpeng">Hasil verifikasi pengajuan UKT Mahasiswa sebesar</div>
										<div class="nominal"> Rp <?php echo number_format($ukt[0][$kolom]);?>
										
										</div>
										<div class="ketpeng">
										Silahkan untuk melakukan Registrasi (membayar biaya masuk mahasiswa baru) di Bank BTN se-Indonesia 
										dengan menunjukkan kartu peserta. Biaya kuliah dibayar secara tunai (tidak dapat diangsur). 
										Calon mahasiswa yang sudah melakukan registrasi, biaya UKT tidak dapat ditarik kembalidengan alasan apapun.
										<p style="font-weight:bold;padding-top:2vw;">Registrasi dilaksanakan pada tanggal 14 s.d. 24 Juli 2017</p>
										</div>
									</div>
								</div>
							</div>
						</div>
				</div>
		</div>
	
	<!--div class="footer">
	
	</div>-->
	</div>
  </body>
  <script>
   $(document).on('click', '#close-preview', function(){ 
    $('.image-preview').popover('hide');
    // Hover befor close the preview    
});

function cek(){
	var simpan = $('#provinsi').val();
	$('#kota').load("<?php echo base_url(); ?>home/getKota/"+simpan);
}
$(function() {
	
    <?php
	if($profil['upload_pbb'] == ""){ ?>
	$('#foto_pbb').hide();
	$("#upload_pbb").show();
	<?php
	}else { ?>
	$('#foto_pbb').show();
	$("#upload_pbb").hide();
	<?php
	}
	if($profil['upload_stnk'] == ""){ ?>
	$('#foto_stnk').hide();
	$("#upload_stnk").show();
	<?php
	}else { ?>
	$('#foto_stnk').show();
	$("#upload_stnk").hide();
	<?php
	}
	if($profil['upload_atap'] == ""){ ?>
	$('#foto_atap').hide();
	$("#upload_atap").show();
	<?php
	}else { ?>
	$('#foto_atap').show();
	$("#upload_atap").hide();
	<?php
	}
	if($profil['upload_lantai'] == ""){ ?>
	$('#foto_lantai').hide();
	$("#upload_lantai").show();
	<?php
	}else { ?>
	$('#foto_lantai').show();
	$("#upload_lantai").hide();
	<?php
	}
	if($profil['upload_pln'] == ""){ ?>
	$('#foto_pln').hide();
	$("#upload_pln").show();
	<?php
	}else { ?>
	$('#foto_pln').show();
	$("#upload_pln").hide();
	<?php
	}
	if($profil['upload_pdam'] == ""){ ?>
	$('#foto_pdam').hide();
	$("#upload_pdam").show();
	<?php
	}else { ?>
	$('#foto_pdam').show();
	$("#upload_pdam").hide();
	<?php
	}
	if($profil['upload_gaji_ayah'] == ""){ ?>
	$('#foto_gaji').hide();
	$("#upload_gaji").show();
	<?php
	}else { ?>
	$('#foto_gaji').show();
	$("#upload_gaji").hide();
	<?php
	}
	if($profil['upload_gaji_ibu'] == ""){ ?>
	$('#foto_gaji_ibu').hide();
	$("#upload_gaji_ibu").show();
	<?php
	}else { ?>
	$('#foto_gaji_ibu').show();
	$("#upload_gaji_ibu").hide();
	<?php
	}
	if($profil['upload_kjs'] == "1" || $profil['upload_kjs'] == ""){ ?>
	$('#foto_kjs').hide();
	$("#upload_kjs1").show();
	<?php
	}else { ?>
	$('#foto_kjs').show();
	$("#upload_kjs1").hide();
	<?php
	}
	if($profil['upload_kk'] == ""){ ?>
	$('#foto_kk').hide();
	$("#upload_kk").show();
	<?php
	}else { ?>
	$('#foto_kk').show();
	$("#upload_kk").hide();
	<?php
	}
	if($profil['foto'] == "" ){ ?>
	$('#foto_foto').hide();
	$("#upload_foto").show();
	<?php
	}else { ?>
	$('#foto_foto').show();
	$("#upload_foto").hide();
	<?php
	}
	?>
	var ayah = $("#peker_ayah5").val();
	var ibu = $("#peker_ibu5").val();
	if(ayah == ""){
		$("#ayah_lainnya").hide();		
	}
	if(ibu == ""){
		$("#ibu_lainnya").hide();		
	}
	if (document.getElementById('ukt1').checked) {
		$("#upload_all").show();
		$("#jks").show();	  
		$("#upload_kjs1").hide();
		var jks = '<?php echo $profil['upload_kjs'];?>';
		if(jks == "1" || jks == ""){
			$('#foto_kjs').hide();
			$("#upload_kjs1").show();		
		}else {
			$('#foto_kjs').show();
			$("#upload_kjs1").hide();			
		}		
			
	}else if (document.getElementById('ukt5').checked){
		$("#jks").hide();	  
		$("#upload_all").hide();	  
	} else {
		$("#upload_all").show();	  
		$("#jks").hide();	  
	}
	
});

function ubah(nama,nama_foto){
	var upload = "#"+nama;
	var foto = "#"+nama_foto;
	$(foto).hide();
	$(upload).show();
}

function as(){
	if (document.getElementById('peker_ayah2').checked) {
		$("#ayah_lainnya").show();	  
	}else {
		$("#ayah_lainnya").hide();	  
		$("#peker_ayah5").val("");	  
	}
}

function ibu(){
	if (document.getElementById('peker_ibu').checked) {
		$("#ibu_lainnya").show();	  
	}else {
		$("#ibu_lainnya").hide();	  
		$("#peker_ibu5").val("");	  
	}
}

function op(){
	if (document.getElementById('ukt1').checked) {
		$("#upload_all").show();
		$("#jks").show();	  
		var jks = '<?php echo $profil['upload_kjs'];?>';
		if(jks == "1" || jks == ""){
			$('#foto_kjs').hide();
			$("#upload_kjs1").show();		
		}else {
			$('#foto_kjs').show();
			$("#upload_kjs1").hide();			
		}				
	}else if (document.getElementById('ukt5').checked){
		$("#jks").hide();	  
		$("#upload_all").hide();	  
	} else {
		$("#upload_all").show();	  
		$("#jks").hide();	  
	}
}

  </script>
 </html>