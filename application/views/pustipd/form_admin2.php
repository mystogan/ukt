<html>
  <head><meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>

  	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/reset.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/css.css" type="text/css "/>
	
	<!-- DataTables CSS -->
	<link href="<?php echo base_url();?>assets/css/dataTables.bootstrap.css" rel="stylesheet">

	<!-- DataTables Responsive CSS -->
	<link href="<?php echo base_url();?>assets/css/dataTables.responsive.css" rel="stylesheet">

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<!-- CKeditor CSS -->
	<script src="http://cdn.ckeditor.com/4.5.9/standard/ckeditor.js"></script>
	<!-- Custom Theme JavaScript -->
	<script src="<?php echo base_url();?>assets/js/jquery.dataTables.min.js" type="text/javascript" ></script>
	<script src="<?php echo base_url();?>assets/js/dataTables.bootstrap.min.js" type="text/javascript" ></script>
	<script src="<?php echo base_url();?>assets/js/dataTables.responsive.js" type="text/javascript" ></script>
	
	<script>
	$(document).ready(function() {
		$('#dataTables-example').DataTable({
				responsive: true
		});
	});
	</script>
  </head>
  <body>
  
		<div class="atas col-xs-12 col-sm-12 col-md-12" >
			<a href="<?php echo base_url()?>uin/logout">
			<div class="btn btn-default pull-right" style="background-color:#eee;border:none;color:red;"> <span class="glyphicon glyphicon-off" style="margin-right:.5vw;"></span>Keluar</div>
			</a>
		</div>
	<div class="headerK">
	<div class="container">
		<div class="col-xs-12 col-sm-12 col-md-12" >
			<div class="row">
				<div class="logo"><img src="<?php echo base_url();?>assets/images/aan.png"/></div>
				<div class="judulK">Validasi Pengajuan UKT </br>Mahasiswa UIN Sunan Ampel Surabaya</div>
			</div>
		</div>
	</div>
	</div>
	<div class="container">
		<div class="col-xs-12 col-sm-12 col-md-12" >
			<div class="row">
				 <div class="panel panel-green"style="margin-top:2vw;">
		                        <div class="panel-heading jd" style="">
		                            Daftar Nama Pengajuan UKT Mahasiswa (Bayangan)
		                        </div>
		                        <!-- /.panel-heading -->
		                        <div class="panel-body">

		                            <div id="dataTables-example_wrapper" class="dataTables_wrapper from-inline dt-bootstrap no-footer">

		                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
		                                <thead>
		                                    <tr>
		                                        <th style="text-align:center;">No</th>
		                                        <th style="text-align:center;">Kode</th>
		                                        <th style="text-align:center;">Nama Mahasiswa</th>
		                                        <th style="text-align:center;">Program Studi</th>
		                                        <th style="text-align:center;">Jalur</th>
		                                        <th style="text-align:center;">Salah</th>
		                                        <th style="text-align:center;">Info</th>
		                                    </tr>
		                                </thead>
		                                <tbody>
											<?php
											$j=1;
											foreach ($all as $Hall){ ?>
											<tr>
											<?php 
											$i = 0;
											if($Hall['ver_tanah'] == "2"){
												$i = $i + 1;
											}												
											if($Hall['ver_bangunan'] == "2"){
												$i = $i + 1;
											}
											if($Hall['ver_pbb'] == "2"){
												$i = $i + 1;
											}
											if($Hall['ver_pln'] == "2"){
												$i = $i + 1;
											}
											if($Hall['ver_pdam'] == "2"){
												$i = $i + 1;
											}
											if($Hall['ver_ayah'] == "2"){
												$i = $i + 1;
											}
											if($Hall['ver_ibu'] == "2"){
												$i = $i + 1;
											}
											if($Hall['ver_anggota'] == "2"){
												$i = $i + 1;
											}
											if($Hall['ver_kjs'] == "2"){
												$i = $i + 1;
											}

																						
											?>
		                                        <td style="text-align:center;"><?php echo $j;?></td>
		                                        <td style="text-align:center;">
													<a href="<?php echo base_url()?>uin/detil/<?php echo $Hall['kode'];?>">
														<?php echo $Hall['kode'];?>
													</a>
													<?php
													if($i != 0){ ?>
													<?php 
													}else { ?>
														<?php //echo $Hall['kode'];?>
													<?php } ?>
												</td>
		                                        <td style="text-align:center;"><a <?php if($i != 0){echo 'style="color:red;"';} ?>><?php echo $Hall['nama'];?></a></td>
		                                        <td style="text-align:center;"><?php echo $Hall['prodi'];?></td>
		                                        <td style="text-align:center;"><?php echo $Hall['jalurr'];?></td>
		                                        <td style="text-align:center;">
													<?php 
													
													echo $i;
													
													?>
												</td>
		                                        <td style="text-align:center;"><?php 
												
												if($Hall['ver_tanah'] == "" ||$Hall['ver_spd'] == "" ||$Hall['ver_pbb'] == ""){
													echo "Belum";
												}else {
													echo "Sudah";
													
												}
												
												?></td>
		                                    </tr>
											<?php
											$j++;
											}
											?>
											
		                                </tbody>
		                            </table>
									
		                            <!-- /.table-responsive -->
		                            </div>
		                        </div>
		                        <!-- /.panel-body -->
		                    </div>
		                    <!-- /.panel -->
			</div>
		</div>
	</div>
  
  </body>
 </html>