<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=hasil.xls");


 ?>
                <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Kode Mahasiswa</th>
                          <th>Nama Mahasiswa</th>
                          <th>Kode Prodi</th>
                          <th>Program Studi</th>
                          <th>Tanggal Lahir</th>
                          <th>Jalur</th>
                          <th>Alamat</th>
                          <th>Golongan Darah</th>
                          <th>Provinsi</th>
                          <th>Kabupaten</th>
                          <th>Kode Pos</th>
                          <th>Telepon 1</th>
                          <th>Telepon 2</th>
                          <th>Email</th>
                          <th>Asal Sekolah</th>
                          <th>Nama Ayah</th>
                          <th>Nama Ibu</th>
                          <th>Kelompok UKT</th>
                          <th>Nominal</th>
                          <th>Upload Rapor</th>
                          <th>Verifikasi Rapor</th>
                          <th>Upload Kesanggupan</th>
                          <th>Verifikasi Kesanggupan</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                          $c = 1;
                          foreach ($mhs as $Hmhs) {
                            // cek yang salah berapa untuk yang sudah di validasi kaprodi

                            ?>
                            <tr>
                              <td><?php echo $c; ?></td>
                              <td><?php echo $Hmhs['kode']; ?></td>
                              <td><?php echo $Hmhs['nama']; ?></td>
                              <td><?php echo $Hmhs['kode_prodi']; ?></td>
                              <td><?php echo $Hmhs['prodi']; ?></td>
                              <td><?php echo "'".$Hmhs['tgl_lhr']; ?></td>
                              <td><?php echo $Hmhs['jalurr']; ?></td>
                              <td><?php echo $Hmhs['alamat']; ?></td>
                              <td><?php echo $Hmhs['gol_dar']; ?></td>
                              <td><?php echo $Hmhs['nama_prov']; ?></td>
                              <td><?php echo $Hmhs['nama_kab']; ?></td>
                              <td><?php echo $Hmhs['kode_pos']; ?></td>
                              <td><?php echo $Hmhs['tlp_rmh']; ?></td>
                              <td><?php echo $Hmhs['hp']; ?></td>
                              <td><?php echo $Hmhs['email']; ?></td>
                              <td><?php echo $Hmhs['asal_sekolah']; ?></td>
                              <td><?php echo $Hmhs['nama_ayah']; ?></td>
                              <td><?php echo $Hmhs['nama_ibu']; ?></td>
                              <td><?php echo $Hmhs['kel_1']; ?></td>
                              <td><?php
                                  $ukt = $Hmhs['kel_1'];
                                  $prodi = $Hmhs['kode_prodi'];
                                  $strSql = "select kel$ukt as hasil from besar_ukt where prodi = '$prodi' ";
                              		$query = $this->db->query($strSql);
                              		$data = $query->result_array();
                                  print_r ($data[0]['hasil']);

                                ?></td>
                                <td>
                                <?php     
                                if ($Hmhs['upload_rapor'] == '') {
                                  echo "Belum Upload";
                                }else {
                                  echo "Sudah Upload";
                                }                
                                ?>
                                </td>
                                <td>
                                <?php     
                                if ($Hmhs['ver_rapor'] == '1') {
                                  echo "Valid";
                                }else if ($Hmhs['ver_rapor'] == '2') {
                                  echo "Valid";
                                }else {
                                  echo "Belum Terverifikasi";
                                }                
                                ?>
                                </td>
                                <td>
                                <?php     
                                if ($Hmhs['upload_kesanggupan'] == '') {
                                  echo "Belum Upload";
                                }else {
                                  echo "Sudah Upload";
                                }                
                                ?>
                                </td>
                                <td>
                                <?php     
                                if ($Hmhs['ver_kesanggupan'] == '1') {
                                  echo "Valid";
                                }else if ($Hmhs['ver_kesanggupan'] == '2') {
                                  echo "Tidak Valid";
                                }else {
                                  echo "Belum Terverifikasi";
                                }                
                                ?>
                                </td>
                            </tr>

                        <?php
                        $c++;
                          }
                         ?>

                      </tbody>
                        </table>
