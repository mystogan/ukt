
      <div class="container-fluid mimin-wrapper">

          <!-- start: content -->
            <div id="content">
                <div class="col-md-12" style="padding:20px;">
                    <div class="col-md-12 padding-0">
                        <div class="col-md-8 padding-0">
                            <div class="col-md-12 padding-0">
                                <div class="col-md-6">
                                    <div class="panel box-v1">
                                      <div class="panel-heading bg-white border-none">
                                        <div class="col-md-12 col-sm-12 col-xs-12 text-left padding-0">
                                          <h4 class="text-left">Buka Website UKT</h4>
                                          <br/>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                                           <h4>
                                           <span class="icon-user icons icon text-right"></span>
                                           </h4>
                                        </div>
                                      </div>
                                        <input type="checkbox" name="buka_ukt" id="buka_ukt" checked data-toggle="toggle"  >
                                        <hr/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="panel box-v1">
                                      <div class="panel-heading bg-white border-none">
                                        <div class="col-md-12 col-sm-12 col-xs-12 text-left padding-0">
                                          <h4 class="text-left">Tanggal Pengumuman UKT</h4>
                                        </div>
                                        <br>
                                        <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                                           <h4>
                                           <span class="icon-basket-loaded icons icon text-right"></span>
                                           </h4>
                                        </div>
                                      </div>
                                      <div class="panel-body text-center">
                                        <input id="tgl_pengumuman_ukt" type="date" value="<?php echo $pengumumanUKT; ?>" class="form-control" >
                                        <hr/>
                                      </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 padding-0">
                                <div class="col-md-6">
                                    <div class="panel box-v1">
                                      <div class="panel-heading bg-white border-none">
                                        <div class="col-md-12 col-sm-12 col-xs-12 text-left padding-0">
                                          <h4 class="text-left">Tanggal Pengisian Berkas Mulai</h4>
                                          <br/>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                                           <h4>
                                           <span class="icon-user icons icon text-right"></span>
                                           </h4>
                                        </div>
                                      </div>
                                      <!--  -->
                                      <div class="panel-body text-center">
                                        <input id="tgl_berkas_mulai" type="date" value="<?php echo $tgl_berkas_mulai; ?>" class="form-control" >
                                        <hr/>
                                      </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="panel box-v1">
                                      <div class="panel-heading bg-white border-none">
                                        <div class="col-md-12 col-sm-12 col-xs-12 text-left padding-0">
                                          <h4 class="text-left">Tanggal Pengisian Berkas Akhir</h4>
                                          <br/>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                                           <h4>
                                           <span class="icon-basket-loaded icons icon text-right"></span>
                                           </h4>
                                        </div>
                                      </div>
                                      <div class="panel-body text-center">
                                        <input id="tgl_berkas_akhir" type="date" value="<?php echo $tgl_berkas_akhir; ?>" class="form-control" >
                                        <hr/>
                                      </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 padding-0">
                                <div class="col-md-6">
                                    <div class="panel box-v1">
                                      <div class="panel-heading bg-white border-none">
                                        <div class="col-md-12 col-sm-12 col-xs-12 text-left padding-0">
                                          <h4 class="text-left">Tanggal Mulai Verifikator</h4>
                                          <br/>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                                           <h4>
                                           <span class="icon-user icons icon text-right"></span>
                                           </h4>
                                        </div>
                                      </div>
                                      <!--  -->
                                      <div class="panel-body text-center">
                                        <input id="verifikator_mulai" type="date" value="<?php echo $verifikator_mulai; ?>" class="form-control" >
                                        <hr/>
                                      </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="panel box-v1">
                                      <div class="panel-heading bg-white border-none">
                                        <div class="col-md-12 col-sm-12 col-xs-12 text-left padding-0">
                                          <h4 class="text-left">Tanggal Akhir Verifikator</h4>
                                          <br/>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                                          <h4>
                                          <span class="icon-basket-loaded icons icon text-right"></span>
                                          </h4>
                                        </div>
                                      </div>
                                      <div class="panel-body text-center">
                                        <input id="verifikator_akhir" type="date" value="<?php echo $verifikator_akhir; ?>" class="form-control" >
                                        <hr/>
                                      </div>
                                    </div>
                                </div>
                            </div>
                            <hr/>

                        </div>
                        <div class="col-md-4">
                            <div class="col-md-12 padding-0">
                              <div class="panel box-v3">
                                <div class="panel-heading bg-white border-none">
                                  <h4>Jalur</h4>
                                </div>
                                    <form id="pilih" class="panel-body text-center">
                                        <div class="form-animate-radio">
                                          <label class="radio">
                                            <input id="sbmptn" type="radio" name="jalur" value="1" <?php if($jalur == 1){ echo 'checked="checked"';} ?> />
                                            <span class="outer">
                                              <span class="inner"></span></span> SNMPTN
                                          </label>
                                        </div>
                                        <div class="form-animate-radio">
                                          <label class="radio">
                                            <input id="sbmptn" type="radio" name="jalur" value="2" <?php if($jalur == 2){ echo 'checked="checked"';} ?> />
                                            <span class="outer">
                                              <span class="inner"></span></span> SBMPTN
                                          </label>
                                        </div>
                                        <div class="form-animate-radio">
                                          <label class="radio">
                                            <input id="umptn" type="radio" name="jalur" value="3" <?php if($jalur == 3){ echo 'checked="checked"';} ?> />
                                            <span class="outer">
                                              <span class="inner"></span></span> SPANPTKIN
                                          </label>
                                        </div>
                                        <div class="form-animate-radio">
                                          <label class="radio">
                                            <input id="umptn" type="radio" name="jalur" value="4" <?php if($jalur == 4){ echo 'checked="checked"';} ?> />
                                            <span class="outer">
                                              <span class="inner"></span></span> UMPTN
                                          </label>
                                        </div>
                                        <div class="form-animate-radio">
                                          <label class="radio">
                                            <input id="umptn" type="radio" name="jalur" value="5" <?php if($jalur == 5){ echo 'checked="checked"';} ?> />
                                            <span class="outer">
                                              <span class="inner"></span></span> Mandiri
                                          </label>
                                        </div>
                                    </form>

                                </div>
                              </div>
                            </div>

                            <div id="igku"></div>
                        </div>
                    </div>

                </div>
      		  </div>
          <!-- end: content -->


      </div>

    <!-- start: Javascript -->
    <script src="<?php echo base_url(); ?>assets/admin/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/jquery.ui.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/bootstrap.min.js"></script>

    <!-- plugins -->
    <script src="<?php echo base_url(); ?>assets/admin/js/plugins/moment.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/plugins/momentum.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/plugins/jquery.nicescroll.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/plugins/bootstrap-material-datetimepicker.js"></script>

    <!-- custom -->
     <script src="<?php echo base_url(); ?>assets/admin/js/main.js"></script>
     <!-- Toogle -->
     <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
     <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
     <!-- End Toogle -->


  </body>
</html>

<script type="text/javascript">
<?php if($buka_ukt == 1){ ?>
  $('#buka_ukt').bootstrapToggle('on');
<?php }else {?>
  $('#buka_ukt').bootstrapToggle('off');
<?php } ?>

   $(document).ready(function(){

  });
  // menyimpan setting jalur yang akan di tentukan
  $('#pilih input').on('change', function() {
    var jalur = $('input[name=jalur]:checked', '#pilih').val();

    $.ajax({
			type:"POST",
			url:base_url+"pustipd/gantiJalur",
			data:"jalur="+jalur,
			success: function(kembali){
        if(kembali > 0){
          alert ("Data Berhasil diubah");
          $("input[name=jalur][value='"+jalur+"']").prop("checked",true);
        }else {
          alert ("Silahkan Ulangi Kembali");
          location.href=base_url+"pustipd";
        }
			}
		});
});

//untuk menyimpan tanggal input mulai
text = document.getElementById('tgl_berkas_mulai');
text.addEventListener('change', function() {
  var tgl_mulai = text.value;
  $.ajax({
    type:"POST",
    url:base_url+"pustipd/gantiTgl",
    data:"id=3&tgl_mulai="+tgl_mulai+"&kolom=tgl_mulai",
    success: function(kembali){
      // alert (kembali);
      if(kembali > 0){
        alert ("Waktu Berhasil diubah");

      }else {
        alert ("Silahkan Ulangi Kembali");
        location.href=base_url+"pustipd";
      }
    }
  });
});
//untuk menyimpan tanggal input Akhir
var tgl_berkas_akhir = document.getElementById('tgl_berkas_akhir');
tgl_berkas_akhir.addEventListener('change', function() {
  var tgl_akhir = tgl_berkas_akhir.value;
  // alert ("tgl akhir"+tgl_akhir);
  $.ajax({
    type:"POST",
    url:base_url+"pustipd/gantiTgl",
    data:"id=3&tgl_mulai="+tgl_akhir+"&kolom=tgl_akhir",
    success: function(kembali){
      if(kembali > 0){
        alert ("Waktu Berhasil diubah");
      }else {
        alert ("Silahkan Ulangi Kembali");
        location.href=base_url+"pustipd";
      }
    }
  });
});
//untuk menyimpan tanggal mulai verifikator
var verifikator_mulai = document.getElementById('verifikator_mulai');
verifikator_mulai.addEventListener('change', function() {
  var tgl_mulai = verifikator_mulai.value;
  // alert ("tgl akhir"+tgl_akhir);
  $.ajax({
    type:"POST",
    url:base_url+"pustipd/gantiTgl",
    data:"id=5&tgl_mulai="+tgl_mulai+"&kolom=tgl_mulai",
    success: function(kembali){
      if(kembali > 0){
        alert ("Waktu Berhasil diubah");
      }else {
        alert ("Silahkan Ulangi Kembali");
        location.href=base_url+"pustipd";
      }
    }
  });
});

//untuk menyimpan tanggal akhir verifikator
var verifikator_akhir = document.getElementById('verifikator_akhir');
verifikator_akhir.addEventListener('change', function() {
  var tgl_akhir = verifikator_akhir.value;
  $.ajax({
    type:"POST",
    url:base_url+"pustipd/gantiTgl",
    data:"id=5&tgl_mulai="+tgl_akhir+"&kolom=tgl_akhir",
    success: function(kembali){
      if(kembali > 0){
        alert ("Waktu Berhasil diubah");
      }else {
        alert ("Silahkan Ulangi Kembali");
        location.href=base_url+"pustipd";
      }
    }
  });
});
//untuk menyimpan tanggal pengumuman UKT
var tgl_pengumuman_ukt = document.getElementById('tgl_pengumuman_ukt');
tgl_pengumuman_ukt.addEventListener('change', function() {
  var tgl = tgl_pengumuman_ukt.value;
  $.ajax({
    type:"POST",
    url:base_url+"pustipd/gantiTgl",
    data:"id=1&tgl_mulai="+tgl+"&kolom=tgl_mulai",
    success: function(kembali){
      if(kembali > 0){
        alert ("Waktu Berhasil diubah");
      }else {
        alert ("Silahkan Ulangi Kembali");
        location.href=base_url+"pustipd";
      }
    }
  });
});

// menyimpan buka website UKT atau tidak
$('input[name=buka_ukt]').change(function(){
    if($(this).is(':checked')) {
      var status = 1;
    } else {
      var status = 0;
    }
    $.ajax({
      type:"POST",
      url:base_url+"pustipd/bukaUKT",
      data:"id=4&status="+status,
      success: function(kembali){
        if (status == 1) {
          alert ("Website UKT Telah dibuka");
        }else {
          alert ("Website UKT Telah ditutup");
        }
      }
    });

});
</script>
