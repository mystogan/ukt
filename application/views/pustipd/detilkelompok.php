

            <!-- start: Content -->
            <div id="content">
               <div class="panel box-shadow-none content-header">
                  <div class="panel-body">
                    <div class="col-md-12">
                        <h3 class="animated fadeInLeft">Data Mahasiswa Jalur <?php echo $jalur['ket']; ?></h3>
                    </div>
                  </div>
              </div>
              <div class="col-md-12 top-20 padding-0">
                <div class="col-md-12">
                  <div class="panel">
                    <!-- <div class="panel-heading"><h3>UKT Kelompok <?php echo $ukt; ?></h3></div> -->
                    <div class="panel-body">
                      <div class="responsive-table">
                      <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Kode Mahasiswa</th> 
                          <th>Nama Mahasiswa</th>
                          <th>Program Studi</th>
                         <!-- <th>Ayah</th>
                          <th>Ibu</th>
                        -->
						  <th>Jumlah</th>
                          <th>UKT Sekarang</th>
                          <th>Besar UKT</th>
                          <!-- <th>Foto Rumah</th> -->
                          <!-- <th>Rekomendasi pindah kelompok</th> -->
                          <th>Pilih UKT</th>
                          <th>Detil Mahasiswa</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                          $c = 1;
                          foreach ($mhs as $Hmhs) {
                            ?>
                            <tr>
                              <td><?php echo $c; ?></td>
                               <td><?php echo $Hmhs['kode']; ?></td> 
                              <td><?php echo $Hmhs['nama']; ?></td>
                              <td><?php echo $Hmhs['prodi']; ?></td>
                             <!-- <td>
                                <?php echo "Rp.".number_format($Hmhs['peng_ayah'],2); ?>
                                <br/>
                                <?php echo $Hmhs['peker_ayah']; ?>
                              </td>
                              <td>
                                <?php echo "Rp.".number_format($Hmhs['peng_ibu'],2); ?>
                                <br>
                                <?php echo $Hmhs['peker_ibu']; ?>
                              </td> -->
                              <td>
                                <?php echo "Rp.".number_format(($Hmhs['peng_ibu']+$Hmhs['peng_ayah']),2); ?>

                              </td>
                              <td>
                                <?php echo $Hmhs['kel_1']; ?>
								
                              </td>
							  
                              <td>
                            <?php 
                            if($Hmhs['kel_1'] != null){
                              $jos = "kel".$Hmhs['kel_1'];
                              echo "Rp.".number_format($Hmhs[$jos],2); 
                            }
                            ?>  
                              </td>
                              <!-- <td>
                                <?php if ($Hmhs['upload_tetangga'] == ""){ ?>
                                  Tidak ada Surat Tetangga
                                <?php }else{ ?>
                                  <a href="<?php echo base_url(); ?>assets/foto/<?php echo $Hmhs['upload_tetangga']; ?>"> disini</a>

                                <?php } ?>
                              </td>
                              <td>
                                <?php if ($Hmhs['upload_anakkuliah'] == ""){ ?>
                                  Tidak ada Anak Kuliah
                                <?php }else{ ?>
                                  <a href="<?php echo base_url(); ?>assets/foto/<?php echo $Hmhs['upload_rmh']; ?>"> disini</a>

                                <?php } ?>
                              </td> -->
                              <!-- <td style="text-align:center;">
                                <form action="<?php echo base_url()?>atasan/kel_ukt2" method="post" >
                                  <input type="hidden" name="kelompok2" value="<?php echo $id;?>" />
                                  <input type="hidden" name="kode" value="<?php echo $Hmhs['kode'];?>" />
                                  <input type="hidden" name="kel_ukt2" value="<?php echo $Hmhs['kel_2'];?>" />
                                  <input type="hidden" name="pers_ukt2" value="<?php echo $Hmhs['rekom_2'];?>" />
                                  <button type="submit" class="btn btn-success"><?php
                                    if ($Hmhs['rekom_2'] == '') {
                                      echo "0";
                                    }else {
                                      echo $Hmhs['rekom_2'];
                                    }
                                  ?>% Kelompok UKT ke <?php echo $Hmhs['kel_2'];?></button>
                                </form>
                              </td> -->
                              <td style="text-align:center;">
                                <a href="javascript:kode('<?php echo $Hmhs['id_mhs'];?>');">
                                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">Pilih</button>
                                </a>
                              </td>
                              <td>
                                <form class="" action="<?php echo base_url(); ?>atasan/mhsdetil" method="post">
                                  <input type="hidden" name="kode_mhs" value="<?php echo $Hmhs['kode']; ?>">
                                  <input type="submit" class="btn btn-3d btn-info" value="Lihat">
                                </form>
                              </td>

                            </tr>

                        <?php
                        $c++;
                          }
                         ?>

                      </tbody>
                        </table>
                      </div>
                  </div>
                </div>
              </div>
              </div>
            </div>
          <!-- end: content -->

          <!-- start Modal  -->
          <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
              <div class="modal-content">
                <form action="<?php echo base_url()?>atasan/ubahUKT" method="post">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" style="text-align:center;"id="myModalLabel">Pilih UKT Mahasiswa</h4>
                </div>
                <input type="hidden" name="kode" id="kodeMHS"  />
                <div class="modal-body col-xs-12 col-sm-12 col-md-12" style="text-align:center;">
                  <div class="col-xs-6 col-sm-4 col-md-4" >
                    <div class="form-animate-radio">
                      <label class="radio">
                        <input type="radio" name="ukt" value="1"  />
                        <span class="outer">
                          <span class="inner"></span></span> Kelompok 1
                      </label>
                    </div>
                  </div>
                  <div class="col-xs-6 col-sm-4 col-md-4" >
                    <div class="form-animate-radio">
                      <label class="radio">
                        <input type="radio" name="ukt" value="2"  />
                        <span class="outer">
                          <span class="inner"></span></span> Kelompok 2
                      </label>
                    </div>
                  </div>
                  <div class="col-xs-6 col-sm-4 col-md-4" >
                    <div class="form-animate-radio">
                      <label class="radio">
                        <input type="radio" name="ukt" value="3"  />
                        <span class="outer">
                          <span class="inner"></span></span> Kelompok 3
                      </label>
                    </div>
                  </div>
                  <div class="col-xs-6 col-sm-4 col-md-6" >
                    <div class="form-animate-radio">
                      <label class="radio">
                        <input type="radio" name="ukt" value="4"  />
                        <span class="outer">
                          <span class="inner"></span></span> Kelompok 4
                      </label>
                    </div>
                  </div>
                  <div class="col-xs-6 col-sm-4 col-md-6" >
                    <div class="form-animate-radio">
                      <label class="radio">
                        <input type="radio" name="ukt" value="5"  />
                        <span class="outer">
                          <span class="inner"></span></span> Kelompok 5
                      </label>
                    </div>
                  </div>
                  <div class="col-xs-6 col-sm-4 col-md-6" >
                    <div class="form-animate-radio">
                      <label class="radio">
                        <input type="radio" name="ukt" value="6"  />
                        <span class="outer">
                          <span class="inner"></span></span> Kelompok 6
                      </label>
                    </div>
                  </div>
                  <div class="col-xs-6 col-sm-4 col-md-6" >
                    <div class="form-animate-radio">
                      <label class="radio">
                        <input type="radio" name="ukt" value="7"  />
                        <span class="outer">
                          <span class="inner"></span></span> Kelompok 7
                      </label>
                    </div>
                  </div>
                </div>

                <div class="modal-footer"style="">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
                </form>
              </div>
              </div>
          </div>

          <!-- End Modal  -->

          <!-- start: right menu -->
            <div id="right-menu">
              <ul class="nav nav-tabs">
                <li class="active">
                 <a data-toggle="tab" href="#right-menu-user">
                  <span class="fa fa-comment-o fa-2x"></span>
                 </a>
                </li>
                <li>
                 <a data-toggle="tab" href="#right-menu-notif">
                  <span class="fa fa-bell-o fa-2x"></span>
                 </a>
                </li>
                <li>
                  <a data-toggle="tab" href="#right-menu-config">
                   <span class="fa fa-cog fa-2x"></span>
                  </a>
                 </li>
              </ul>

            </div>
          <!-- end: right menu -->

      </div>


<!-- start: Javascript -->
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/bootstrap.min.js"></script>



<!-- plugins -->
<script src="<?php echo base_url(); ?>assets/admin/js/plugins/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/plugins/jquery.datatables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/plugins/datatables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/plugins/jquery.nicescroll.js"></script>


<!-- custom -->
<script src="<?php echo base_url(); ?>assets/admin/js/main.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#datatables-example').DataTable();

  });

  function kode(kode){
    $("#kodeMHS").val(kode);
  }
</script>
<!-- end: Javascript -->
</body>
</html>
