

            <!-- start: Content -->
            <div id="content">
               <div class="panel box-shadow-none content-header">
                  <div class="panel-body">
                    <div class="col-md-12">
                        <h3 class="animated fadeInLeft">Data Mahasiswa</h3>

                    </div>
                  </div>
              </div>
              <div class="col-md-12 top-20 padding-0">
                <div class="col-md-12">
                  <div class="panel">
                    <div class="panel-heading"><h3>Data Mahasiswa</h3></div>
                    <div class="panel-body">
                      <div class="responsive-table">
                      <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Kode Mahasiswa</th>
                          <th>Nama Mahasiswa</th>
                          <th>Program Studi</th>
                          <th>Tanggal Lahir</th>
                          <th>Jalur</th>
                          <th>Salah</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                          $c = 1;
                          foreach ($mhs as $Hmhs) {
                            // cek yang salah berapa untuk yang sudah di validasi kaprodi
                            $i = 0;
      											if($Hmhs['ver_tanah'] == "2"){
      												$i = $i + 1;
      											}
      											if($Hmhs['ver_bangunan'] == "2"){
      												$i = $i + 1;
      											}
      											if($Hmhs['ver_pbb'] == "2"){
      												$i = $i + 1;
      											}
      											if($Hmhs['ver_pln'] == "2"){
      												$i = $i + 1;
      											}
      											if($Hmhs['ver_pdam'] == "2"){
      												$i = $i + 1;
      											}
      											if($Hmhs['ver_ayah'] == "2"){
      												$i = $i + 1;
      											}
      											if($Hmhs['ver_ibu'] == "2"){
      												$i = $i + 1;
      											}
      											if($Hmhs['ver_anggota'] == "2"){
      												$i = $i + 1;
      											}

                            ?>
                            <tr>
                              <td><?php echo $c; ?></td>
                              <td><?php echo $Hmhs['kode']; ?></td>
                              <td><?php echo $Hmhs['nama']; ?></td>
                              <td><?php echo $Hmhs['prodi']; ?></td>
                              <td><?php echo $Hmhs['tgl_lhr']; ?></td>
                              <td><?php echo $Hmhs['jalurr']; ?></td>
                              <td><?php
                              $ui = 0;
                              if($Hmhs['ver_tanah'] == "" || $Hmhs['ver_tanah'] == null ){
        												echo "Mahasiswa Tidak Finalisasi";
                                $ui = 1;
        											}else {
                                echo $i;
                              }
                               ?></td>
                              <td>
                                <?php
                                if ($ui == 0 && $i > 0) { ?>
                                  <form class="" action="<?php echo base_url(); ?>pustipd/detil/<?php echo $Hmhs['kode']; ?>" method="post">
                                    <input type="submit" class="btn btn-3d btn-info" value="Proses">
                                  </form>


                                <?php
                                }
                                 ?>
                              </td>

                            </tr>

                        <?php
                        $c++;
                          }
                         ?>

                      </tbody>
                        </table>
                      </div>
                  </div>
                </div>
              </div>
              </div>
            </div>
          <!-- end: content -->



          <!-- start: right menu -->
            <div id="right-menu">
              <ul class="nav nav-tabs">
                <li class="active">
                 <a data-toggle="tab" href="#right-menu-user">
                  <span class="fa fa-comment-o fa-2x"></span>
                 </a>
                </li>
                <li>
                 <a data-toggle="tab" href="#right-menu-notif">
                  <span class="fa fa-bell-o fa-2x"></span>
                 </a>
                </li>
                <li>
                  <a data-toggle="tab" href="#right-menu-config">
                   <span class="fa fa-cog fa-2x"></span>
                  </a>
                 </li>
              </ul>

            </div>
          <!-- end: right menu -->

      </div>


<!-- start: Javascript -->
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/bootstrap.min.js"></script>



<!-- plugins -->
<script src="<?php echo base_url(); ?>assets/admin/js/plugins/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/plugins/jquery.datatables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/plugins/datatables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/plugins/jquery.nicescroll.js"></script>


<!-- custom -->
<script src="<?php echo base_url(); ?>assets/admin/js/main.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#datatables-example').DataTable();

  });
</script>
<!-- end: Javascript -->
</body>
</html>
