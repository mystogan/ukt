<!-- start: content -->
  <div id="content">

      <div class="col-md-12" style="padding:20px;">

        <div class="col-md-12 card-wrap padding-0">
          <div class="col-md-12">
              <div class="panel">

                <div class="panel-body" style="padding-bottom:50px;">
                    <div id="canvas-holder1">
                          <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                    </div>
                </div>
              </div>
          </div>

        </div>
        <div class="col-md-12 card-wrap padding-0">
          <div class="col-md-12">
              <div class="panel">
                <div class="panel-body" style="padding-bottom:50px;">
                    <div id="canvas-holder1">
                      <div id="container2" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                    </div>
                </div>
              </div>
          </div>
      </div>
      </div>
  </div>
<!-- end: content -->

  </div>
<!-- end: right menu -->

</div>


<!-- start: Javascript -->
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/bootstrap.min.js"></script>


<!-- plugins -->
<script src="<?php echo base_url(); ?>assets/admin/js/plugins/moment.min.js"></script>

<script src="<?php echo base_url(); ?>assets/admin/js/plugins/jquery.nicescroll.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/plugins/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/plugins/exporting.js"></script>

<!-- custom -->
<script src="<?php echo base_url(); ?>assets/admin/js/main.js"></script>

<script type="text/javascript">

Highcharts.chart('container', {
chart: {
  type: 'line'
},
title: {
  text: 'Pendaftaran Mahasiswa Dari Berbagai Jalur Tahun'
},
subtitle: {
  text: 'UIN Sunan Ampel Surabaya'
},
xAxis: {

      categories: [
        'Kelompok 1',
        'Kelompok 2',
        'Kelompok 3',
        'Kelompok 4',
        'Kelompok 5',
        'Kelompok 6',
        'Kelompok 7'
      ]
},
yAxis: {
  title: {
    text: 'Jumlah Mahasiswa'
  }
},
plotOptions: {
  line: {
    dataLabels: {
      enabled: true
    },
    enableMouseTracking: false
  }
},
series: [{
    name: 'SNMPTN',
    data: [<?php echo $satu[0]['hasil']; ?>, <?php echo $dua[0]['hasil']; ?>
          , <?php echo $tiga[0]['hasil']; ?>, <?php echo $empat[0]['hasil']; ?>
          , <?php echo $lima[0]['hasil']; ?>, <?php echo $enam[0]['hasil']; ?>
          , <?php echo $tujuh[0]['hasil']; ?>]

},{
    name: 'SBMPTN',
    data: [<?php echo $satu[1]['hasil']; ?>, <?php echo $dua[1]['hasil']; ?>
          , <?php echo $tiga[1]['hasil']; ?>, <?php echo $empat[1]['hasil']; ?>
          , <?php echo $lima[1]['hasil']; ?>, <?php echo $enam[1]['hasil']; ?>
          , <?php echo $tujuh[1]['hasil']; ?>]

}, {
    name: 'SPANPTKIN',
    data: [<?php echo $satu[2]['hasil']; ?>, <?php echo $dua[2]['hasil']; ?>
          , <?php echo $tiga[2]['hasil']; ?>, <?php echo $empat[2]['hasil']; ?>
          , <?php echo $lima[2]['hasil']; ?>, <?php echo $enam[2]['hasil']; ?>
          , <?php echo $tujuh[2]['hasil']; ?>]

}, {
    name: 'UMPTKIN',
    data: [<?php echo $satu[3]['hasil']; ?>, <?php echo $dua[3]['hasil']; ?>
          , <?php echo $tiga[3]['hasil']; ?>, <?php echo $empat[3]['hasil']; ?>
          , <?php echo $lima[3]['hasil']; ?>, <?php echo $enam[3]['hasil']; ?>
          , <?php echo $tujuh[3]['hasil']; ?>]

}, {
    name: 'Mandiri',
    data: [<?php echo $satu[4]['hasil']; ?>, <?php echo $dua[4]['hasil']; ?>
          , <?php echo $tiga[4]['hasil']; ?>, <?php echo $empat[4]['hasil']; ?>
          , <?php echo $lima[4]['hasil']; ?>, <?php echo $enam[4]['hasil']; ?>
          , <?php echo $tujuh[4]['hasil']; ?>]

}]
});
Highcharts.chart('container2', {
chart: {
  type: 'line'
},
title: {
  text: 'Jumlah Mahasiswa '
},
subtitle: {
  text: 'UIN Sunan Ampel Surabaya'
},
xAxis: {

      categories: [
          <?php
          for ($i=(date("Y")-4); $i <= (date("Y")) ; $i++) {
             echo "'".$i."'," ;
          }
           ?>
      ]
},
yAxis: {
  title: {
    text: 'Jumlah Mahasiswa'
  }
},
plotOptions: {
  line: {
    dataLabels: {
      enabled: true
    },
    enableMouseTracking: false
  }
},
series: [{
    name: 'Mahasiswa',
    data: [<?php echo $jumlah[date("Y")-4]; ?>, <?php echo $jumlah[date("Y")-3]; ?>
          , <?php echo $jumlah[date("Y")-2]; ?>, <?php echo $jumlah[date("Y")-1]; ?>
          , <?php echo $jumlah[date("Y")]; ?>]

}]
});
</script>

</body>
</html>
