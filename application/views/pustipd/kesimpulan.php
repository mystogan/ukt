          <div id="content">
          <div class="col-md-12 padding-0" style="margin-top:20px;">

            <div class="col-md-12">
                <div class="panel">
                       <div class="panel-heading-white panel-heading">
                          <h4>Persentase Penerimaan Kelompok UKT
                          </h4>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                            </div>
                        </div>
                  </div>
            </div>

          </div>
    		  </div>
          <!-- end: content -->


      </div>


<!-- end: Content -->
<!-- start: Javascript -->
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/bootstrap.min.js"></script>


    <!-- plugins -->
    <script src="<?php echo base_url(); ?>assets/admin/js/plugins/moment.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/plugins/jquery.nicescroll.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/plugins/highcharts.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/plugins/exporting.js"></script>
    <!-- custom -->
     <script src="<?php echo base_url(); ?>assets/admin/js/main.js"></script>
     <script type="text/javascript">

Highcharts.chart('container', {
          chart: {
              type: 'column'
          },
          title: {
              text: 'Jumlah Penentuan UKT'
          },
          subtitle: {
              text: 'UIN Sunan Ampel Surabaya'
          },
          xAxis: {
              categories: [
                  'Kelompok 1',
                  'Kelompok 2',
                  'Kelompok 3',
                  'Kelompok 4',
                  'Kelompok 5',
                  'Kelompok 6',
                  'Kelompok 7'
              ],
              crosshair: true
          },
          yAxis: {
              min: 0,
              title: {
                  text: 'Rainfall (Orang)'
              }
          },
          tooltip: {
              headerFormat: '<span style="font-size:20px">{point.key}</span><table>',
              pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                  '<td style="padding:0"><b>{point.y:.1f} Orang</b></td></tr>',
              footerFormat: '</table>',
              shared: true,
              useHTML: true
          },
          plotOptions: {
              column: {
                  pointPadding: 0.2,
                  borderWidth: 0
              },
              series: {
                   cursor: 'pointer',
                   point: {
                       events: {
                          click: function() {
                              alert ('Category: '+ this.category +', value: '+ this.x+" "+this.series.name);
                              window.location.href = '../atasan/detil/'+this.x+"/"+this.series.name;
                          }
                      }
                  }
              }
          },
          series: [{
              name: 'SNMPTN',
              data: [<?php echo $satu[0]['hasil']; ?>, <?php echo $dua[0]['hasil']; ?>
                    , <?php echo $tiga[0]['hasil']; ?>, <?php echo $empat[0]['hasil']; ?>
                    , <?php echo $lima[0]['hasil']; ?>, <?php echo $enam[0]['hasil']; ?>
                    , <?php echo $tujuh[0]['hasil']; ?>]

          },{
              name: 'SBMPTN',
              data: [<?php echo $satu[1]['hasil']; ?>, <?php echo $dua[1]['hasil']; ?>
                    , <?php echo $tiga[1]['hasil']; ?>, <?php echo $empat[1]['hasil']; ?>
                    , <?php echo $lima[1]['hasil']; ?>, <?php echo $enam[1]['hasil']; ?>
                    , <?php echo $tujuh[1]['hasil']; ?>]

          }, {
              name: 'SPANPTKIN',
              data: [<?php echo $satu[2]['hasil']; ?>, <?php echo $dua[2]['hasil']; ?>
                    , <?php echo $tiga[2]['hasil']; ?>, <?php echo $empat[2]['hasil']; ?>
                    , <?php echo $lima[2]['hasil']; ?>, <?php echo $enam[2]['hasil']; ?>
                    , <?php echo $tujuh[2]['hasil']; ?>]

          }, {
              name: 'UMPTKIN',
              data: [<?php echo $satu[3]['hasil']; ?>, <?php echo $dua[3]['hasil']; ?>
                    , <?php echo $tiga[3]['hasil']; ?>, <?php echo $empat[3]['hasil']; ?>
                    , <?php echo $lima[3]['hasil']; ?>, <?php echo $enam[3]['hasil']; ?>
                    , <?php echo $tujuh[3]['hasil']; ?>]

          }, {
              name: 'Mandiri',
              data: [<?php echo $satu[4]['hasil']; ?>, <?php echo $dua[4]['hasil']; ?>
                    , <?php echo $tiga[4]['hasil']; ?>, <?php echo $empat[4]['hasil']; ?>
                    , <?php echo $lima[4]['hasil']; ?>, <?php echo $enam[4]['hasil']; ?>
                    , <?php echo $tujuh[4]['hasil']; ?>]

          }]
      });



     </script>
    </body>
</html>
