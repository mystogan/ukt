<div id="content">
  <div class="tabs-wrapper text-center">
   <div class="panel box-shadow-none text-left content-header">
        <div class="panel-body" style="padding-bottom:0px;">
          <div class="col-md-12">
              <h3 class="animated fadeInLeft">Deti Mahasiswa</h3>
          </div>
        </div>
    </div>
  <div class="col-md-12 tab-content">
<style media="screen">
  label{
    font-size: 25px;
  }
</style>
    <div role="tabpanel" class="tab-pane fade active in" id="tabs-area-demo" aria-labelledby="tabs1">
      <div class="col-md-12">
        <div class="col-md-12">
          <div class="col-md-12 tabs-area">
            <div class="liner"></div>
            <ul class="nav nav-tabs nav-tabs-v5" id="tabs-demo6">
              <li class="active">
               <a href="#tabs-demo6-area1" data-toggle="tab" title="welcome">
                <span class="round-tabs one">
                  <i class="glyphicon glyphicon-user"></i>
                </span>
              </a>
            </li>

            <li>
              <a href="#tabs-demo6-area2" data-toggle="tab" title="profile">
               <span class="round-tabs two">
                 <i class="glyphicon glyphicon-home"></i>
               </span>
             </a>
           </li>

           <li>
            <a href="#tabs-demo6-area3" data-toggle="tab" title="bootsnipp goodies">
             <span class="round-tabs three">
              <i class="glyphicon glyphicon-tasks"></i>
            </span> </a>
          </li>

          <!-- <li>
            <a href="#tabs-demo6-area4" data-toggle="tab" title="blah blah">
             <span class="round-tabs four">
               <i class="glyphicon glyphicon-comment"></i>
             </span>
           </a>
         </li> -->

         <li><a href="#tabs-demo6-area5" data-toggle="tab" title="completed">
           <span class="round-tabs five">
            <i class="glyphicon glyphicon-ok"></i>
          </span> </a>
        </li>
      </ul>
      <div class="tab-content tab-content-v5">
        <div class="tab-pane fade in active" id="tabs-demo6-area1">
          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">Nama</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center"><?php echo $mhs['nama']; ?></h3>
            </div>
          </div>

          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">Kode Mahasiswa</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center"><?php echo $mhs['kode']; ?></h3>
            </div>
          </div>
          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">Program Studi</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center"><?php echo $mhs['prodi']; ?></h3>
            </div>
          </div>
          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">Alamat</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center">
                <?php
                if ($mhs['alamat'] != null) {
                  echo $mhs['alamat'];
                }else {
                  echo "-";
                }
                 ?>
              </h3>
            </div>
          </div>
          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">Provinsi</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center">
                <?php
                if ($mhs['nama_prov'] != null) {
                  echo $mhs['nama_prov'];
                }else {
                  echo "-";
                }
                 ?>
              </h3>
            </div>
          </div>
          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">Kota</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center">
                <?php
                if ($mhs['nama_kab'] != null) {
                  echo $mhs['nama_kab'];
                }else {
                  echo "-";
                }
                 ?>
              </h3>
            </div>
          </div>
          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">No hp</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center">
                <?php
                if ($mhs['hp'] != null) {
                  echo $mhs['hp'];
                }else {
                  echo "-";
                }
                 ?>
              </h3>
            </div>
          </div>
          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">Email</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center">
                <?php
                if ($mhs['email'] != null) {
                  echo $mhs['email'];
                }else {
                  echo "-";
                }
                 ?>
              </h3>
            </div>
          </div>
        </div>
        <div class="tab-pane fade" id="tabs-demo6-area2">

          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">Nama Ayah</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center">
                <?php
                if ($mhs['nama_ayah'] != null) {
                  echo $mhs['nama_ayah'];
                }else {
                  echo "-";
                }
                 ?>
              </h3>
            </div>
          </div>
          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">Nama Ibu</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center">
                <?php
                if ($mhs['nama_ibu'] != null) {
                  echo $mhs['nama_ibu'];
                }else {
                  echo "-";
                }
                 ?>
              </h3>
            </div>
          </div>
          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">Pekerjaan Ayah</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center">
                <?php
                if ($mhs['peker_ayah'] != null) {
                  echo $mhs['peker_ayah'];
                }else {
                  echo "-";
                }
                 ?>
              </h3>
            </div>
          </div>
          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">Pekerjaan Ibu</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center">
                <?php
                if ($mhs['peker_ibu'] != null) {
                  echo $mhs['peker_ibu'];
                }else {
                  echo "-";
                }
                 ?>
              </h3>
            </div>
          </div>
          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">Penghasilan Ayah</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center">
                <?php
                if ($mhs['peng_ayah'] != null) {
                  echo "Rp. ".number_format($mhs['peng_ayah'],2);
                }else {
                  echo "-";
                }
                 ?>
              </h3>
            </div>
          </div>
          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">Penghasilan Ibu</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center">
                <?php
                if ($mhs['peng_ibu'] != null) {
                  echo "Rp. ".number_format($mhs['peng_ibu'],2);
                }else {
                  echo "-";
                }
                 ?>
              </h3>
            </div>
          </div>

        </div>
        <div class="tab-pane fade" id="tabs-demo6-area3">

          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">Kepemilikan Rumah</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center">
                <?php
                if ($mhs['kepemilikan'] != null) {
                  echo $mhs['kepemilikan'];
                }else {
                  echo "-";
                }
                 ?>
              </h3>
            </div>
          </div>
          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">Luas Tanah</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center">
                <?php
                if ($mhs['luas_tanah'] != null) {
                  echo $mhs['luas_tanah']." M<sup>2</sup>";
                }else {
                  echo "-";
                }
                 ?>
              </h3>
            </div>
          </div>
          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">Luas Bangunan</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center">
                <?php
                if ($mhs['luas_bangunan'] != null) {
                  echo $mhs['luas_bangunan']." M<sup>2</sup>";
                }else {
                  echo "-";
                }
                 ?>
              </h3>
            </div>
          </div>
          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">PBB</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center">
                <?php
                if ($mhs['pbb'] != null) {
                  echo "Rp. ".number_format($mhs['pbb'],2);
                }else {
                  echo "-";
                }
                 ?>
              </h3>
            </div>
          </div>
          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">PLN</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center">
                <?php
                if ($mhs['pbb'] != null) {
                  echo $mhs['pln']." VA";
                }else {
                  echo "-";
                }
                 ?>
              </h3>
            </div>
          </div>
          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">PDAM</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center">
                <?php
                if ($mhs['pdam'] != null) {
                  echo "Rp. ".number_format($mhs['pdam'],2);
                }else {
                  echo "-";
                }
                 ?>
              </h3>
            </div>
          </div>
          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">Sumber Listrik</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center">
                <?php
                if ($mhs['sum_listrik'] != null) {
                  echo $mhs['sum_listrik'];
                }else {
                  echo "-";
                }
                 ?>
              </h3>
            </div>
          </div>
          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">Sumber Air</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center">
                <?php
                if ($mhs['sum_air'] != null) {
                  echo $mhs['sum_air'];
                }else {
                  echo "-";
                }
                 ?>
              </h3>
            </div>
          </div>

        </div>
        <div class="tab-pane fade" id="tabs-demo6-area4">


        </div>
        <div class="tab-pane fade" id="tabs-demo6-area5">

          <?php




          if ($mhs['ver_tanah'] == 2) { ?>
          <form  action="<?php echo base_url(); ?>pustipd/validasi" method="post">
            <input type="hidden" name="id_mhs" value="<?php echo $mhs['id_mhs']; ?>">
            <input type="hidden" name="kode" value="<?php echo $mhs['kode']; ?>">
            <input type="hidden" name="nama_kolom" value="ver_tanah">
            <input type="hidden" name="nama_kolom_edit" value="luas_tanah">
            <div class="form-group">
              <div class="col-sm-3 control-label text-center"><label>Luas Tanah</label></div>
              <div class="col-sm-3 control-label text-center">
                <input type="text" class="form-control" name="isi_edit" value="<?php echo $mhs['luas_tanah']; ?>">
              </div>
              <div class="col-sm-3">
                <label for="[object Object]" data-toggle="modal" data-target="#myModal">
                  <a href="javascript:getfoto('<?php if($mhs['upload_pbb'] == ''){echo "foto.jpg";}else {echo $mhs['upload_pbb'];}  ?>');">Klik Gambar</a>

                </label>
              </div>
              <div class="col-sm-2 text-center">

                  <input type="submit" name="" class="btn btn-success" style="margin-top:-10px;" value="Simpan">
              </div>
            </div>
          </form>

          <?php
          }
          ?>
          <?php
          if ($mhs['ver_bangunan'] == 2) { ?>
          <form  action="<?php echo base_url(); ?>pustipd/validasi" method="post">
            <input type="hidden" name="id_mhs" value="<?php echo $mhs['id_mhs']; ?>">
            <input type="hidden" name="kode" value="<?php echo $mhs['kode']; ?>">
            <input type="hidden" name="nama_kolom" value="ver_bangunan">
            <input type="hidden" name="nama_kolom_edit" value="luas_bangunan">
            <div class="form-group">
              <div class="col-sm-3 control-label text-center"><label>Luas Bangunan</label></div>
              <div class="col-sm-3 control-label text-center">
                <input type="text" class="form-control" name="isi_edit" value="<?php echo $mhs['luas_bangunan']; ?>">
              </div>
              <div class="col-sm-3">
                <label for="[object Object]" data-toggle="modal" data-target="#myModal">
                  <a href="javascript:getfoto('<?php if($mhs['upload_pbb'] == ''){echo "foto.jpg";}else {echo $mhs['upload_pbb'];}  ?>');">Klik Gambar</a>
                </label>
              </div>
              <div class="col-sm-2 text-center">
                  <input type="submit" name="" class="btn btn-success" style="margin-top:-10px;" value="Simpan">
              </div>
            </div>
          </form>
          <?php
          }
          ?>
          <?php
          if ($mhs['ver_pbb'] == 2) { ?>
          <form  action="<?php echo base_url(); ?>pustipd/validasi" method="post">
            <input type="hidden" name="id_mhs" value="<?php echo $mhs['id_mhs']; ?>">
            <input type="hidden" name="kode" value="<?php echo $mhs['kode']; ?>">
            <input type="hidden" name="nama_kolom" value="ver_pbb">
            <input type="hidden" name="nama_kolom_edit" value="pbb">
            <div class="form-group">
              <div class="col-sm-3 control-label text-center"><label>PBB/Tahun</label></div>
              <div class="col-sm-3 control-label text-center">
                <input type="text" class="form-control" name="isi_edit" value="<?php echo $mhs['pbb']; ?>">
              </div>
              <div class="col-sm-3">
                <label for="[object Object]" data-toggle="modal" data-target="#myModal">
                  <a href="javascript:getfoto('<?php if($mhs['upload_pbb'] == ''){echo "foto.jpg";}else {echo $mhs['upload_pbb'];}  ?>');">Klik Gambar</a>
                </label>
              </div>
              <div class="col-sm-2 text-center">
                  <input type="submit" name="" class="btn btn-success" style="margin-top:-10px;" value="Simpan">
              </div>
            </div>
          </form>
          <?php
          }
          ?>
          <?php
          if ($mhs['ver_pln'] == 2) { ?>
          <form  action="<?php echo base_url(); ?>pustipd/validasi" method="post">
            <input type="hidden" name="id_mhs" value="<?php echo $mhs['id_mhs']; ?>">
            <input type="hidden" name="kode" value="<?php echo $mhs['kode']; ?>">
            <input type="hidden" name="nama_kolom" value="ver_pln">
            <input type="hidden" name="nama_kolom_edit" value="pln">
            <div class="form-group">
              <div class="col-sm-3 control-label text-center"><label>Daya PLN </label></div>
              <div class="col-sm-3 control-label text-center">
                <input type="text" class="form-control" name="isi_edit" value="<?php echo $mhs['pln']; ?>">
              </div>
              <div class="col-sm-3">
                <label for="[object Object]" data-toggle="modal" data-target="#myModal">
                  <a href="javascript:getfoto('<?php if($mhs['upload_pln'] == ''){echo "foto.jpg";}else {echo $mhs['upload_pln'];}  ?>');">Klik Gambar</a>
                </label>
              </div>
              <div class="col-sm-2 text-center">
                  <input type="submit" name="" class="btn btn-success" style="margin-top:-10px;" value="Simpan">
              </div>
            </div>
          </form>
          <?php
          }
          ?>
          <?php
          if ($mhs['ver_pdam'] == 2) { ?>
          <form  action="<?php echo base_url(); ?>pustipd/validasi" method="post">
            <input type="hidden" name="id_mhs" value="<?php echo $mhs['id_mhs']; ?>">
            <input type="hidden" name="kode" value="<?php echo $mhs['kode']; ?>">
            <input type="hidden" name="nama_kolom" value="ver_pdam">
            <input type="hidden" name="nama_kolom_edit" value="pdam">
            <div class="form-group">
              <div class="col-sm-3 control-label text-center"><label>Biaya PDAM/Bulan </label></div>
              <div class="col-sm-3 control-label text-center">
                <input type="text" class="form-control" name="isi_edit" value="<?php echo $mhs['pdam']; ?>">
              </div>
              <div class="col-sm-3">
                <label for="[object Object]" data-toggle="modal" data-target="#myModal">
                  <a href="javascript:getfoto('<?php if($mhs['upload_pdam'] == ''){echo "foto.jpg";}else {echo $mhs['upload_pdam'];}  ?>');">Klik Gambar</a>
                </label>
              </div>
              <div class="col-sm-2 text-center">
                  <input type="submit" name="" class="btn btn-success" style="margin-top:-10px;" value="Simpan">
              </div>
            </div>
          </form>
          <?php
          }
          ?>
          <?php
          if ($mhs['ver_ayah'] == 2) { ?>
          <form  action="<?php echo base_url(); ?>pustipd/validasi" method="post">
            <input type="hidden" name="id_mhs" value="<?php echo $mhs['id_mhs']; ?>">
            <input type="hidden" name="kode" value="<?php echo $mhs['kode']; ?>">
            <input type="hidden" name="nama_kolom" value="ver_ayah">
            <input type="hidden" name="nama_kolom_edit" value="peng_ayah">
            <div class="form-group">
              <div class="col-sm-3 control-label text-center"><label>Penghasilan Ayah </label></div>
              <div class="col-sm-3 control-label text-center">
                <input type="text" class="form-control" name="isi_edit" value="<?php echo $mhs['peng_ayah']; ?>">
              </div>
              <div class="col-sm-3">
                <label for="[object Object]" data-toggle="modal" data-target="#myModal">
                  <a href="javascript:getfoto('<?php if($mhs['upload_gaji_ayah'] == ''){echo "foto.jpg";}else {echo $mhs['upload_gaji_ayah'];} ?>');">Klik Gambar</a>
                </label>
              </div>
              <div class="col-sm-2 text-center">
                  <input type="submit" name="" class="btn btn-success" style="margin-top:-10px;" value="Simpan">
              </div>
            </div>
          </form>
          <?php
          }
          ?>
          <?php
          if ($mhs['ver_ibu'] == 2) { ?>
          <form  action="<?php echo base_url(); ?>pustipd/validasi" method="post">
            <input type="hidden" name="id_mhs" value="<?php echo $mhs['id_mhs']; ?>">
            <input type="hidden" name="kode" value="<?php echo $mhs['kode']; ?>">
            <input type="hidden" name="nama_kolom" value="ver_ibu">
            <input type="hidden" name="nama_kolom_edit" value="peng_ibu">
            <div class="form-group">
              <div class="col-sm-3 control-label text-center"><label>Penghasilan Ibu </label></div>
              <div class="col-sm-3 control-label text-center">
                <input type="text" class="form-control" name="isi_edit" value="<?php echo $mhs['peng_ibu']; ?>">
              </div>
              <div class="col-sm-3">
                <label for="[object Object]" data-toggle="modal" data-target="#myModal">
                  <a href="javascript:getfoto('<?php if($mhs['upload_gaji_ibu'] == ''){echo "foto.jpg";}else {echo $mhs['upload_gaji_ibu'];} ?>');">Klik Gambar</a>
                </label>
              </div>
              <div class="col-sm-2 text-center">
                  <input type="submit" name="" class="btn btn-success" style="margin-top:-10px;" value="Simpan">
              </div>
            </div>
          </form>
          <?php
          }
          ?>
          <?php
          if ($mhs['ver_anggota'] == 2) { ?>
          <form  action="<?php echo base_url(); ?>pustipd/validasi" method="post">
            <input type="hidden" name="id_mhs" value="<?php echo $mhs['id_mhs']; ?>">
            <input type="hidden" name="kode" value="<?php echo $mhs['kode']; ?>">
            <input type="hidden" name="nama_kolom" value="ver_anggota">
            <input type="hidden" name="nama_kolom_edit" value="anggota">
            <div class="form-group">
              <div class="col-sm-3 control-label text-center"><label>Jumlah Anggota KK </label></div>
              <div class="col-sm-3 control-label text-center">
                <input type="text" class="form-control" name="isi_edit" value="<?php echo $mhs['anggota']; ?>">
              </div>
              <div class="col-sm-3">
                <label for="[object Object]" data-toggle="modal" data-target="#myModal">
                  <a href="javascript:getfoto('<?php if($mhs['upload_kk'] == ''){echo "foto.jpg";}else {echo $mhs['upload_kk'];} ?>');">Klik Gambar</a>
                </label>
              </div>
              <div class="col-sm-2 text-center">
                  <input type="submit" name="" class="btn btn-success" style="margin-top:-10px;" value="Simpan">
              </div>
            </div>
          </form>
          <?php
          }
          ?>

        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
    </div>
      </div>
      </div>
  </div>
</div>

</div>


<!-- start Modal Foto -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="myModalLabel">Foto</h4>
	  </div>
	  <div class="modal-body" id="set_foto" >

	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	 </div>
	</div>
  </div>
</div>
<!-- end Modal Foto -->

<!-- start: Javascript -->
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/bootstrap.min.js"></script>


<!-- plugins -->
<script src="<?php echo base_url(); ?>assets/admin/js/plugins/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/plugins/jquery.nicescroll.js"></script>


<!-- custom -->
<script src="<?php echo base_url(); ?>assets/admin/js/main.js"></script>
<script type="text/javascript">
$(document).ready(function(){

$(".nav-tabs a").click(function (e) {
  e.preventDefault();
  $(this).tab('show');
});

});

function getfoto(img){
  document.getElementById("set_foto").innerHTML = '<img style="width:100%;" src="<?php echo base_url(); ?>assets/foto/'+img+'" alt="">';
}
</script>
<!-- end: Javascript -->
</body>
</html>
