<!-- start: content -->
  <div id="content">

      <div class="col-md-12" style="padding:20px;">

        <div class="col-md-12 card-wrap padding-0">
          <div class="col-md-12">
              <div class="panel">

                <div class="panel-body" style="padding-bottom:50px;">
                    <div id="canvas-holder1">
                          <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                    </div>
                </div>
              </div>
          </div>

        </div>
      </div>
  </div>
<!-- end: content -->

  </div>
<!-- end: right menu -->

</div>


<!-- start: Javascript -->
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/bootstrap.min.js"></script>


<!-- plugins -->
<script src="<?php echo base_url(); ?>assets/admin/js/plugins/moment.min.js"></script>

<script src="<?php echo base_url(); ?>assets/admin/js/plugins/jquery.nicescroll.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/histogram-bellcurve.js"></script>

<!-- custom -->
<script src="<?php echo base_url(); ?>assets/admin/js/main.js"></script>

<script type="text/javascript">

var data = [3.5, 3];

Highcharts.chart('container', {

  title: {
    text: 'Bell curve'
  },

  xAxis: [{
    title: {
      text: 'Data'
    },
    alignTicks: false
  }, {
    title: {
      text: 'Bell curve'
    },
    alignTicks: false,
    opposite: true
  }],

  yAxis: [{
    title: { text: 'Data' }
  }, {
    title: { text: 'Bell curve' },
    opposite: true
  }],

  series: [{
    name: 'Bell curve',
    type: 'bellcurve',
    xAxis: 1,
    yAxis: 1,
    baseSeries: 1,
    zIndex: -1
  }, {
    name: 'Data',
    type: 'scatter',
    data: data,
    marker: {
      radius: 3.5
    }
  }]
});

</script>

</body>
</html>



