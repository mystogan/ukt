<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta name="description" content="Moch. Abdul Aziz">
  <meta name="author" content="Moch Abdul Aziz">
  <meta name="keyword" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Dashboard Verifikator</title>
  <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>assets/images/uin.png" />
  <!-- start: Css -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin/css/bootstrap.min.css">

  <!-- plugins -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin/css/plugins/font-awesome.min.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin/css/plugins/datatables.bootstrap.min.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin/css/plugins/animate.min.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin/css/plugins/bootstrap-material-datetimepicker.css"/>

  <link href="<?php echo base_url(); ?>assets/admin/css/style.css" rel="stylesheet">
  <!-- end: Css -->
  <script type="text/javascript">
    var base_url = '<?php echo base_url(); ?>';
  </script>

</head>

<body id="mimin" class="dashboard">
      <!-- start: Header -->
        <nav class="navbar navbar-default header navbar-fixed-top">
          <div class="col-md-12 nav-wrapper">
            <div class="navbar-header" style="width:100%;">
              <div class="opener-left-menu is-open">
                <span class="top"></span>
                <span class="middle"></span>
                <span class="bottom"></span>
              </div>
                <a href="index.html" class="navbar-brand time">
                 <b>UIN Sunan Ampel Surabaya</b>
                </a>


              <ul class="nav navbar-nav navbar-right user-nav">
                <li class="user-name"><span><?php echo $this->session->userdata('nama');; ?></span></li>
                  <li class="dropdown avatar-dropdown">
                   <img src="<?php echo base_url(); ?>assets/images/uin.png" class="img-circle avatar" alt="user name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"/>
                   <ul class="dropdown-menu user-dropdown">
                     <li><a href="<?php echo base_url(); ?>uin/logout"><span class="fa fa-calendar"></span> Log Out</a></li>

                  </ul>
                </li>
            </ul>
            </div>
          </div>
        </nav>
      <!-- end: Header -->

      <div class="container-fluid mimin-wrapper">

          <!-- start:Left Menu -->
            <div id="left-menu">
              <div class="sub-left-menu scroll">
                <ul class="nav nav-list">
                    <li><div class="left-bg"></div></li>
                    <li class="time">
                      <h1 class="animated fadeInLeft">21:00</h1>
                    </li>
                    
                    <li class="ripple">
                      <a href="<?php echo base_url(); ?>verifikator/uktMhs" >
                          <span class="fa-home fa text-right"></span> Besar UKT
                      </a>
                    </li>
                    <?php
                    $status = $this->session->userdata('status');
                    if ($status == 4) {
                      echo '
                      <li class="ripple">
                        <a href="'.base_url().'verifikator/akademik" >
                            <span class="fa-home fa text-right"></span> Data Mahasiswa
                        </a>
                      </li>';
                    }else {
                      echo '
                      <li class="ripple">
                        <a href="'.base_url().'verifikator/" >
                            <span class="fa-home fa text-right"></span> Data Mahasiswa
                        </a>
                      </li>';
                    }
                    ?>
                    
                  </ul>
                </div>
            </div>
          <!-- end: Left Menu -->
