

            <!-- start: Content -->
            <div id="content">
               <div class="panel box-shadow-none content-header">
                  <div class="panel-body">
                    <div class="col-md-12">
                        <h3 class="animated fadeInLeft">Data UKT Mahasiswa</h3>

                    </div>
                  </div>
              </div>
              <div class="col-md-12 top-20 padding-0">
                <div class="col-md-12">
                  <div class="panel">
                    <div class="panel-heading"><h3>Data UKT Mahasiswa</h3></div>
                    <div class="panel-body">
                      <div class="responsive-table">
                      <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Program Studi</th>
                          <th>Kelompok 1</th>
                          <th>Kelompok 2</th>
                          <th>Kelompok 3</th>
                          <th>Kelompok 4</th>
                          <th>Kelompok 5</th>
                          <th>Kelompok 6</th>
                          <th>Kelompok 7</th>

                        </tr>
                      </thead>
                      <tbody>
                        <?php
                          $c = 1;
                          foreach ($mhs as $Hmhs) {
                            ?>
                            <tr>
                              <td><?php echo $c; ?></td>
                              <td><?php echo $Hmhs['nama_prodi']; ?></td>
                              <td><?php echo "Rp.".number_format($Hmhs['kel1'],2); ?></td>
                              <td><?php echo "Rp.".number_format($Hmhs['kel2'],2); ?></td>
                              <td><?php echo "Rp.".number_format($Hmhs['kel3'],2); ?></td>
                              <td><?php echo "Rp.".number_format($Hmhs['kel4'],2); ?></td>
                              <td><?php echo "Rp.".number_format($Hmhs['kel5'],2); ?></td>
                              <td><?php echo "Rp.".number_format($Hmhs['kel6'],2); ?></td>
                              <td><?php echo "Rp.".number_format($Hmhs['kel7'],2); ?></td>
                            </tr>

                        <?php
                        $c++;
                          }
                         ?>

                      </tbody>
                        </table>
                      </div>
                  </div>
                </div>
              </div>
              </div>
            </div>
          <!-- end: content -->



          <!-- start: right menu -->
            <div id="right-menu">
              <ul class="nav nav-tabs">
                <li class="active">
                 <a data-toggle="tab" href="#right-menu-user">
                  <span class="fa fa-comment-o fa-2x"></span>
                 </a>
                </li>
                <li>
                 <a data-toggle="tab" href="#right-menu-notif">
                  <span class="fa fa-bell-o fa-2x"></span>
                 </a>
                </li>
                <li>
                  <a data-toggle="tab" href="#right-menu-config">
                   <span class="fa fa-cog fa-2x"></span>
                  </a>
                 </li>
              </ul>

            </div>
          <!-- end: right menu -->

      </div>


<!-- start: Javascript -->
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/bootstrap.min.js"></script>



<!-- plugins -->
<script src="<?php echo base_url(); ?>assets/admin/js/plugins/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/plugins/jquery.datatables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/plugins/datatables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/plugins/jquery.nicescroll.js"></script>


<!-- custom -->
<script src="<?php echo base_url(); ?>assets/admin/js/main.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#datatables-example').DataTable();

  });
</script>
<!-- end: Javascript -->
</body>
</html>
