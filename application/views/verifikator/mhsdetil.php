<div id="content">
  <div class="tabs-wrapper text-center">
   <div class="panel box-shadow-none text-left content-header">
        <div class="panel-body" style="padding-bottom:0px;">
          <div class="col-md-12">
              <h3 class="animated fadeInLeft">Detil Mahasiswa</h3>
          </div>
        </div>
    </div>
  <div class="col-md-12 tab-content">
<style media="screen">
  label{
    font-size: 25px;
  }
</style>
    <div role="tabpanel" class="tab-pane fade active in" id="tabs-area-demo" aria-labelledby="tabs1">
      <div class="col-md-12">
        <div class="col-md-12">
          <div class="col-md-12 tabs-area">
            <div class="liner"></div>
            <ul class="nav nav-tabs nav-tabs-v5" id="tabs-demo6">
              <li >
               <a href="#tabs-demo6-area1" data-toggle="tab" title="welcome">
                <span class="round-tabs one">
                  <i class="glyphicon glyphicon-user"></i>
                </span>
              </a>
            </li>

            <li>
              <a href="#tabs-demo6-area2" data-toggle="tab" title="profile">
               <span class="round-tabs two">
                 <i class="glyphicon glyphicon-home"></i>
               </span>
             </a>
           </li>

           <li>
            <a href="#tabs-demo6-area3" data-toggle="tab" title="bootsnipp goodies">
             <span class="round-tabs three">
              <i class="glyphicon glyphicon-tasks"></i>
            </span> </a>
          </li>

          <!-- <li>
            <a href="#tabs-demo6-area4" data-toggle="tab" title="blah blah">
             <span class="round-tabs four">
               <i class="glyphicon glyphicon-comment"></i>
             </span>
           </a>
         </li> -->

         <li class="active"><a href="#tabs-demo6-area5" data-toggle="tab" title="completed">
           <span class="round-tabs five">
            <i class="glyphicon glyphicon-ok"></i>
          </span> </a>
        </li>
      </ul>
      <div class="tab-content tab-content-v5">
        <div class="tab-pane fade " id="tabs-demo6-area1">
          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">Nama</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center"><?php echo $mhs['nama']; ?></h3>
            </div>
          </div>

          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">Kode Mahasiswa</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center"><?php echo $mhs['kode']; ?></h3>
            </div>
          </div>
          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">Program Studi</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center"><?php echo $mhs['prodi']; ?></h3>
            </div>
          </div>
          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">Alamat</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center">
                <?php
                if ($mhs['alamat'] != null) {
                  echo $mhs['alamat'];
                }else {
                  echo "-";
                }
                 ?>
              </h3>
            </div>
          </div>
          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">Provinsi</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center">
                <?php
                if ($mhs['nama_prov'] != null) {
                  echo $mhs['nama_prov'];
                }else {
                  echo "-";
                }
                 ?>
              </h3>
            </div>
          </div>
          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">Kota</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center">
                <?php
                if ($mhs['nama_kab'] != null) {
                  echo $mhs['nama_kab'];
                }else {
                  echo "-";
                }
                 ?>
              </h3>
            </div>
          </div>
          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">No hp</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center">
                <?php
                if ($mhs['hp'] != null) {
                  echo $mhs['hp'];
                }else {
                  echo "-";
                }
                 ?>
              </h3>
            </div>
          </div>
          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">Email</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center">
                <?php
                if ($mhs['email'] != null) {
                  echo $mhs['email'];
                }else {
                  echo "-";
                }
                 ?>
              </h3>
            </div>
          </div>
        </div>
        <div class="tab-pane fade" id="tabs-demo6-area2">

          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">Nama Ayah</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center">
                <?php
                if ($mhs['nama_ayah'] != null) {
                  echo $mhs['nama_ayah'];
                }else {
                  echo "-";
                }
                 ?>
              </h3>
            </div>
          </div>
          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">Nama Ibu</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center">
                <?php
                if ($mhs['nama_ibu'] != null) {
                  echo $mhs['nama_ibu'];
                }else {
                  echo "-";
                }
                 ?>
              </h3>
            </div>
          </div>
          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">Pekerjaan Ayah</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center">
                <?php
                if ($mhs['peker_ayah'] != null) {
                  echo $mhs['peker_ayah'];
                }else {
                  echo "-";
                }
                 ?>
              </h3>
            </div>
          </div>
          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">Pekerjaan Ibu</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center">
                <?php
                if ($mhs['peker_ibu'] != null) {
                  echo $mhs['peker_ibu'];
                }else {
                  echo "-";
                }
                 ?>
              </h3>
            </div>
          </div>
          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">Penghasilan Ayah</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center">
                <?php
                if ($mhs['peng_ayah'] != null) {
                  echo "Rp. ".number_format($mhs['peng_ayah'],2);
                }else {
                  echo "-";
                }
                 ?>
              </h3>
            </div>
          </div>
          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">Penghasilan Ibu</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center">
                <?php
                if ($mhs['peng_ibu'] != null) {
                  echo "Rp. ".number_format($mhs['peng_ibu'],2);
                }else {
                  echo "-";
                }
                 ?>
              </h3>
            </div>
          </div>

        </div>
        <div class="tab-pane fade" id="tabs-demo6-area3">

          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">Kepemilikan Rumah</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center">
                <?php
                if ($mhs['kepemilikan'] != null) {
                  echo $mhs['kepemilikan'];
                }else {
                  echo "-";
                }
                 ?>
              </h3>
            </div>
          </div>
          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">Luas Tanah</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center">
                <?php
                if ($mhs['luas_tanah'] != null) {
                  echo $mhs['luas_tanah'];
                }else {
                  echo "-";
                }
                 ?>
                 M<sup>2</sup>
              </h3>
            </div>
          </div>
          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">Luas Bangunan</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center">
                <?php
                if ($mhs['luas_bangunan'] != null) {
                  echo $mhs['luas_bangunan'];
                }else {
                  echo "-";
                }
                 ?>
                 M<sup>2</sup>
              </h3>
            </div>
          </div>
          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">Sumber Listrik</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center">
                <?php
                if ($mhs['sum_listrik'] != null) {
                  echo $mhs['sum_listrik'];
                }else {
                  echo "-";
                }
                 ?>
              </h3>
            </div>
          </div>
          <div class="form-group">
            <h3 class="col-sm-4 control-label head text-center">Sumber Air</h3>
            <h3 class="col-sm-1 control-label head text-center">:</h3>
            <div class="col-sm-7">
              <h3 class="head text-center">
                <?php
                if ($mhs['sum_air'] != null) {
                  echo $mhs['sum_air'];
                }else {
                  echo "-";
                }
                 ?>
              </h3>
            </div>
          </div>

        </div>
        <div class="tab-pane fade" id="tabs-demo6-area4">


        </div>
        <div class="tab-pane fade in active" id="tabs-demo6-area5">
          <input type="hidden" id="id_mhs" name="id_mhs" value="<?php echo $mhs['id_mhs']; ?>">
            <div class="form-group">
              <div class="col-sm-3 control-label text-center"><label>Foto</label></div>
              <div class="col-sm-3">
                <label for="[object Object]" data-toggle="modal" data-target="#myModal">
                  <a href="javascript:getfoto('<?php echo $mhs['foto']; ?>');">Lihat Gambar</a>
                </label>
              </div>
            </div>
            <br>
            <br>
            <hr>
            <?php
            $ceka = 0;
            if ($mhs['upload_kjs'] != '') { ?>
              <div class="form-group">
              <div class="col-sm-3 control-label text-center"><label>KKS</label></div>
              <div class="col-sm-3">
                <label for="[object Object]" data-toggle="modal" data-target="#myModal">
                  <a href="javascript:getfoto('<?php echo $mhs['upload_kjs']; ?>');">Lihat Gambar</a>
                </label>
              </div>
              <?php
                if($mhs['ver_kjs']  != 1 && $mhs['ver_kjs']  != 2 ){ ?>
              <div class="col-sm-2">
                <!-- <a href="<?php echo base_url()?>verifikator/<?php echo $mhs['id_mhs']; ?>/1" class="btn btn-info btn-xs" style="margin:-20px">Betul</a> -->
                <a id="btn_kks" href="javascript:saveverifikasiKKS('ver_kjs','kks',1);" class="btn btn-info btn-xs" style="margin:-20px">Betul</a>
              </div>
              <div class="col-sm-2">
                <a id="btn2_kks" href="javascript:saveverifikasiKKS('ver_kjs','kks',2);"  class="btn btn-danger btn-xs" style="margin:-20px">Tidak</a>
              </div>

                <?php
                }
                ?>
            </div>
            <br>
            <br>
            <hr>              
            <?php
            }else {
              $ceka++;
            } ?>

            <div class="form-group">
              <div class="col-sm-4 control-label text-center"><label>Luas Tanah</label></div>
              <div class="col-sm-2 control-label text-center">
                <input type="text" class="form-control" onkeypress="return isNumberKey(event)" id="luas_tanah" value="<?php echo $mhs['luas_tanah']; ?>" <?php if($mhs['ver_tanah']  == 1){ echo "disabled";$ceka++;} ?> >
              </div>
              <div class="col-sm-3">
                <label for="[object Object]" data-toggle="modal" data-target="#myModal">
                  <a href="javascript:getfoto('<?php echo $mhs['upload_pbb']; ?>');">Lihat Gambar</a>
                </label>
              </div> 
              <div class="col-sm-3">
                <?php
                if($mhs['ver_tanah']  != 1){ ?>
                <a id="btn_luas_tanah" href="javascript:saveverifikasi('ver_tanah','luas_tanah');" class="btn btn-info btn-xs" style="margin:-20px">Simpan</a>
                <?php
                }
                ?>
              </div>
            </div>
            <br>
            <br>
            <hr>
            <div class="form-group">
              <div class="col-sm-4 control-label text-center"><label>Luas Bangunan</label></div>
              <div class="col-sm-2 control-label text-center">
                <input type="text" class="form-control" onkeypress="return isNumberKey(event)"  id="luas_bangunan" value="<?php echo $mhs['luas_bangunan']; ?>" <?php if($mhs['ver_bangunan']  == 1){ echo "disabled";$ceka++;} ?> >
                <!-- <label for="[object Object]"><?php echo $mhs['luas_bangunan']; ?> M<sup>2</sup></label> -->
              </div>
              <div class="col-sm-3">
                <label for="[object Object]" data-toggle="modal" data-target="#myModal">
                  <a href="javascript:getfoto('<?php echo $mhs['upload_pbb']; ?>');">Lihat Gambar</a>
                </label>
              </div>
              <div class="col-sm-3">
                <?php
                if($mhs['ver_bangunan']  != 1){ ?>
                <a id="btn_luas_bangunan" href="javascript:saveverifikasi('ver_bangunan','luas_bangunan');" class="btn btn-info btn-xs" style="margin:-20px">Simpan</a>
                <?php
                }
                ?>
              </div>
            </div>
            <br>
            <br>
            <hr>
            <div class="form-group">
              <div class="col-sm-4 control-label text-center"><label>PBB/Tahun</label></div>
              <div class="col-sm-2 control-label text-center">
                <input type="text" class="form-control" onkeypress="return isNumberKey(event)"  id="pbb" value="<?php echo $mhs['pbb']; ?>" <?php if($mhs['ver_pbb']  == 1){ echo "disabled";$ceka++;} ?> >
              </div>
              <div class="col-sm-3">
                <label for="[object Object]" data-toggle="modal" data-target="#myModal">
                  <a href="javascript:getfoto('<?php echo $mhs['upload_pbb']; ?>');">Lihat Gambar</a>
                </label>
              </div>
              <div class="col-sm-3">
                <?php
                if($mhs['ver_pbb']  != 1){ ?>
                <a id="btn_pbb" href="javascript:saveverifikasi('ver_pbb','pbb');" class="btn btn-info btn-xs" style="margin:-20px">Simpan</a>
                <?php
                }
                ?>
              </div>
            </div>
            <br>
            <br>
            <hr>
            <div class="form-group">
              <div class="col-sm-4 control-label text-center"><label>Daya PLN </label></div>
              <div class="col-sm-2 control-label text-center">
                <input type="text" class="form-control" onkeypress="return isNumberKey(event)"  id="pln" value="<?php echo $mhs['pln']; ?>" <?php if($mhs['ver_pln']  == 1){ echo "disabled";$ceka++;} ?> >
              </div>
              <div class="col-sm-3">
                <label for="[object Object]" data-toggle="modal" data-target="#myModal">
                  <a href="javascript:getfoto('<?php echo $mhs['upload_pln']; ?>');">Lihat Gambar</a>
                </label>
              </div>
              <div class="col-sm-3">
              <?php
                if($mhs['ver_pln']  != 1){ ?>
                <a id="btn_pln" href="javascript:saveverifikasi('ver_pln','pln');" class="btn btn-info btn-xs" style="margin:-20px">Simpan</a>
                <?php
                }
                ?>
              </div>
            </div>
            <br>
            <br>
            <hr>
            <div class="form-group">
              <div class="col-sm-4 control-label text-center"><label>Biaya PDAM/Bulan </label></div>
              <div class="col-sm-2 control-label text-center">
                <input type="text" class="form-control" onkeypress="return isNumberKey(event)"  id="pdam" value="<?php echo $mhs['pdam']; ?>" <?php if($mhs['ver_pdam']  == 1){ echo "disabled";$ceka++;} ?> >

              </div>
              <div class="col-sm-3">
                <label for="[object Object]" data-toggle="modal" data-target="#myModal">
                  <a href="javascript:getfoto('<?php echo $mhs['upload_pdam']; ?>');">Lihat Gambar</a>
                </label>
              </div>
              <div class="col-sm-3">
                <?php
                if($mhs['ver_pdam']  != 1){ ?>
                <a id="btn_pdam" href="javascript:saveverifikasi('ver_pdam','pdam');" class="btn btn-info btn-xs" style="margin:-20px">Simpan</a>
                <?php
                }
                ?>
              </div>
            </div>
            <br>
            <br>
            <hr>
            <div class="form-group">
              <div class="col-sm-4 control-label text-center"><label>Penghasilan Ayah </label></div>
              <div class="col-sm-2 control-label text-center">
                <input type="text" class="form-control" onkeypress="return isNumberKey(event)"  id="peng_ayah" value="<?php echo $mhs['peng_ayah']; ?>" <?php if($mhs['ver_ayah']  == 1){ echo "disabled";$ceka++;} ?> >
              </div>
              <div class="col-sm-3">
                <label for="[object Object]" data-toggle="modal" data-target="#myModal">
                  <a href="javascript:getfoto('<?php echo $mhs['upload_gaji_ayah']; ?>');">Lihat Gambar</a>
                </label>
              </div>
              <div class="col-sm-3">
                <?php
                if($mhs['ver_ayah']  != 1){ ?>
                <a id="btn_peng_ayah" href="javascript:saveverifikasi('ver_ayah','peng_ayah');" class="btn btn-info btn-xs" style="margin:-20px">Simpan</a>
                <?php
                }
                ?>
              </div>
            </div>
            <br>
            <br>
            <hr>
            <div class="form-group">
              <div class="col-sm-4 control-label text-center"><label>Penghasilan Ibu </label></div>
              <div class="col-sm-2 control-label text-center">
                <input type="text" class="form-control" onkeypress="return isNumberKey(event)"  id="peng_ibu" value="<?php echo $mhs['peng_ibu']; ?>" <?php if($mhs['ver_ibu']  == 1){ echo "disabled";$ceka++;} ?> >
              </div>
              <div class="col-sm-3">
                <label for="[object Object]" data-toggle="modal" data-target="#myModal">
                  <a href="javascript:getfoto('<?php echo $mhs['upload_gaji_ibu']; ?>');">Lihat Gambar</a>
                </label>
              </div>
              <div class="col-sm-3">
                <?php
                if($mhs['ver_ibu']  != 1){ ?>
                <a id="btn_peng_ibu" href="javascript:saveverifikasi('ver_ibu','peng_ibu');" class="btn btn-info btn-xs" style="margin:-20px">Simpan</a>
                <?php
                }
                ?>
              </div>
            </div>
            <br>
            <br>
            <hr>
            <div class="form-group">
              <div class="col-sm-4 control-label text-center"><label>Jumlah Anggota KK </label></div>
              <div class="col-sm-2 control-label text-center">
                <input type="text" class="form-control" onkeypress="return isNumberKey(event)"  id="anggota" value="<?php echo $mhs['anggota']; ?>" <?php if($mhs['ver_anggota']  == 1){ echo "disabled";$ceka++;} ?> >
              </div>
              <div class="col-sm-3">
                <label for="[object Object]" data-toggle="modal" data-target="#myModal">
                  <a href="javascript:getfoto('<?php echo $mhs['upload_kk']; ?>');">Lihat Gambar</a>
                </label>
              </div>
              <div class="col-sm-3">
                <?php
                if($mhs['ver_anggota']  != 1){ ?>
                <a id="btn_anggota" href="javascript:saveverifikasi('ver_anggota','anggota');" class="btn btn-info btn-xs" style="margin:-20px">Simpan</a>
                <?php
                }
                ?>
              </div>
            </div>
            <br>
            <br>
            <hr>
            <input type="hidden" id="cekredirect" value="<?php echo $ceka; ?>">
            <!-- <div class="form-group">
              <div class="col-sm-4 control-label text-center"><label>Rumah </label></div>
              <div class="col-sm-3">
                <label for="[object Object]" data-toggle="modal" data-target="#myModal">
                  <a href="javascript:getfoto('<?php echo $mhs['upload_rmh']; ?>');">Lihat Gambar</a>
                </label>
              </div>
              <div class="col-sm-2">
                <a href="#" class="btn btn-info btn-xs" style="margin:-20px">Betul</a>
              </div>
              <div class="col-sm-2">
                <a href="#" class="btn btn-danger btn-xs" style="margin:-20px">Tidak</a>
              </div>
            </div> -->

        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
    </div>
      </div>
      </div>
  </div>
</div>

</div>


<!-- start Modal Foto -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="myModalLabel">Foto</h4>
	  </div>
	  <div class="modal-body" id="set_foto" >

	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	 </div>
	</div>
  </div>
</div>
<!-- end Modal Foto -->

<!-- start: Javascript -->
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/bootstrap.min.js"></script>


<!-- plugins -->
<script src="<?php echo base_url(); ?>assets/admin/js/plugins/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/plugins/jquery.nicescroll.js"></script>


<!-- custom -->
<script src="<?php echo base_url(); ?>assets/admin/js/main.js"></script>
<script type="text/javascript">
$(document).ready(function(){

$(".nav-tabs a").click(function (e) {
  e.preventDefault();
  $(this).tab('show');
});

});

function getfoto(img){
  document.getElementById("set_foto").innerHTML = '<img style="width:100%;" src="<?php echo base_url(); ?>assets/foto/'+img+'" alt="">';
}

function isNumberKey(evt)
  {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;

    return true;
  }
function saveverifikasi(kolom, id_input)
{
  let input = $("#"+id_input).val()
  let id_mhs = $("#id_mhs").val()
  
  $.ajax({
    url : "<?php echo base_url(); ?>verifikator/saveverifikator", 
    data: {kolom:kolom, input:input, id_mhs:id_mhs, id_input:id_input},
    type: "post", 
    success:function(response){
      // alert (response);
      let data = JSON.parse(response)
      console.log(response)
      if(data.cek == 1){
        alert (data.pesan)
        $("#"+id_input).prop('disabled', true);
        $("#btn_"+id_input).hide();
        let ceka = $("#cekredirect").val();
        let jumlah = parseInt(ceka)+1
        $("#cekredirect").val(jumlah);
        if(jumlah == '9'){
          window.location = "<?php echo base_url(); ?>verifikator";
        }
      }else {
        alert (data.pesan)
      }
      
    },
    error: function(xhr, Status, err) {
      console.log("Terjadi error : "+Status)
      alert ('terjadi kesalahan silahkan lapor ke pihak PUSTIPD')
    }
    
  });
}
function saveverifikasiKKS(kolom, id_input, value)
{
  let input = value
  let id_mhs = $("#id_mhs").val()
  
  $.ajax({
    url : "<?php echo base_url(); ?>verifikator/saveverifikator", 
    data: {kolom:kolom, input:input, id_mhs:id_mhs, id_input:kolom},
    type: "post", 
    success:function(response){
      // alert (response);
      let data = JSON.parse(response)
      console.log(response)
      if(data.cek == 1){
        alert (data.pesan)
        $("#btn_"+id_input).hide();
        $("#btn2_"+id_input).hide();
        let ceka = $("#cekredirect").val();
        let jumlah = parseInt(ceka)+1
        $("#cekredirect").val(jumlah);
        if(jumlah == '9'){
          window.location = "<?php echo base_url(); ?>verifikator";
        }
      }else {
        alert (data.pesan)
      }
      
    },
    error: function(xhr, Status, err) {
      console.log("Terjadi error : "+Status)
      alert ('terjadi kesalahan silahkan lapor ke pihak PUSTIPD')
    }
    
  });
  
}
</script>
<!-- end: Javascript -->
</body>
</html>
