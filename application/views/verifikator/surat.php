
<script type="text/javascript" src="http://ods.uinsby.ac.id/assets/js/jquery.min.js"></script>
<script>
	$(document).ready(function(){
		window.print();

	});
</script>
<style type="text/css" media="print">
    @page {
        size: auto;
        margin: 0mm;
    }
    
    html {
        background-color: #FFFFFF;
        margin: 0px;
    }
    
    body {
        margin: 5mm 15mm 10mm 15mm;
    }
    

#wrapper{
	width:50%;
	margin:30px auto 20px auto;
}

#header{
	height:15%;
	width:100%;
}
#gambar{
	height:100%;
	width:20%;
	float:left;
}
#tulisan_uin{
	height:100%;
	width:90%;
	margin-left:10%;
	text-align:center;
	font-size:1.2em;
}
#surat_keterangan{
	font-size:1.0em;
}
#garis{
	border-top:3px solid;
	border-bottom:1px solid;
	height:3px;
}
#ket{
	margin-top:2%;
	margin-left:30%;
	height:6%;
	width:50%;
	text-align:center;
}
#keterangan{
}
#satu{
	position: relative;
}
#id{
	position: absolute;
	right:25px;
	width:27vh;
	text-align:justify;
}
#id1{
	position: absolute;
	right:25px;
	text-align:justify;
}
#id_cuti{
	margin-left:67%;
}
#id2{
	float:left;
}

</style>
<html>

<head>
    <title>Surat Pernyataan Kesanggupan</title>
</head>
<font face="arial" size="11">
	<body>

<table style="margin:auto;">
	<tr>
		<td>

    <br/>
	<div id="satu">
    <table width="580px;" style="margin:auto; font-size:0.9em;">
      <tr>
        <td>
          <div style="text-align:center;">
            <strong> SURAT PERNYATAAN KESANGGUPAN <br>
              MEMBAYAR UANG KULIAH TUNGGAL (UKT) <br>
              UIN SUNAN AMPEL SURABAYA <br>
              TAHUN <?php echo date("Y")?>
            </strong>
          </div>
          <br/>
          <br/>
          <br/>
          <div id="">Yang bertandatangan di bawah ini:</div>
          <br/>
          <div id="">
            <table style="font-size:0.95em;">
              <tr>
                <td></td>
                <td>Nama</td>
                <td></td>
                <td>:</td>
                <td><?php echo $profil['nama']; ?></td>
              </tr>
              <tr></tr>
              <tr></tr>
              <tr>
                <td></td>
                <td>Nomor Peserta</td>
                <td></td>
                <td>:</td>
                <td><?php echo $profil['kode']; ?></td>
              </tr>
              <tr></tr>
              <tr></tr>
              <tr>
                <td></td>
                <td>Nama Orangtua</td>
                <td></td>
                <td>:</td>
                <td><?php 
                if ($profil['nama_ayah'] == '' && $profil['nama_ibu'] == '') {
                  $ortu = "-";
                }else if ($profil['nama_ayah'] == '' ) {
                  $ortu = $profil['nama_ibu'] ;
                }else if ($profil['nama_ibu'] == '' ) {
                  $ortu = $profil['nama_ayah'] ;
                }else {
                  $ortu = $profil['nama_ayah'];
                }
                echo $ortu;
                ?></td>
              </tr>
              <tr></tr>
              <tr></tr>
              <tr>
                <td></td>
                <td>Pekerjaan</td>
                <td></td>
                <td>:</td>
                <td>
                  <?php 
                  if ($profil['peker_ayah'] == '' && $profil['peker_ibu'] == '') {
                    echo "-";
                  }else if ($profil['peker_ayah'] == '' ) {
                    echo $profil['peker_ibu'] ;
                  }else if ($profil['peker_ibu'] == '' ) {
                    echo $profil['peker_ayah'] ;
                  }else {
                    echo $profil['peker_ayah'];
                  }
                  ?>
                </td>
              </tr>
              <tr></tr>
              <tr></tr>
              <tr>
                <td></td>
                <td>Alamat Rumah</td>
                <td></td>
                <td>:</td>
                <td><?php echo $profil['alamat_ortu']; ?></td>
              </tr>
              <tr>
                <td></td>
                <td>Nomor HP Pribadi</td>
                <td></td>
                <td>:</td>
                <td><?php echo $profil['hp']; ?></td>
              </tr>
              <tr>
                <td></td>
                <td>Nomor HP Orangtua	</td>
                <td></td>
                <td>:</td>
                <td><?php echo $profil['hp_ortu']; ?></td>
              </tr>
            </table>
          </div>
          <div style="margin-top: 7px;text-align:justify; line-height:25px;">
          menyatakan dengan sesungguhnya bahwa Saya bersedia dan sanggup membayar Uang Kuliah Tunggal (UKT) 
          sesuai dengan nominal yang telah ditetapkan sebesar:
          </div>
          <br>
          <div style="font-size:30px;"> <center> Rp <?php echo number_format($ukt['hasil']);?> </center> </div>
          <div style="margin-top: 7px;text-align:justify; line-height:25px;">
          Demikian surat pernyataan ini saya buat dengan sebenarnya, tanpa ada paksaaan dari pihak manapun.
          </div>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
            <div style="float:left; ">
              Mengetahui
              <br/>Orang Tua
              <br/>
              <br/>
              <br/>
              <br/>
              <br/>
              <br/><?php echo $ortu; ?><br/>
              </div>
            </div>
            <div id="id" style="margin-left:300px;'" >
              Surabaya, 06 April 2019 <br>
                Yang membuat pernyataan,
              <br>
              <br>
                <div style="border:1px solid; width:60px;height:30px;padding:5px;font-size:12px;margin-bottom:5px;">Materai 6000</div>
              <br/>
              <?php echo $profil['nama']; ?><br/>
              </div>
            </div>
          </td>
          </table>
        </td>
      </tr>
	</tr>
</table>

	</body>
</font>

</html>
