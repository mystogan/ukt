<?php
class Home_model extends CI_Model {
	function __construct(){
		parent::__construct();
	}
	function login($pendaftaran,$tgl){
		$this->db->select('id_mhs,kode,nama,tgl_lhr,jalur');
		$this->db->from("mhs");
		$this->db->where('kode =', $pendaftaran);
		$this->db->where('tgl_lhr =', $tgl);
		$this->db->where('status =', '1');
		$query = $this->db->get();
		$data = $query->result_array();
		return $data[0];
	}
	function getProfile($user = 0){
		$this->db->select('b.id as id_kab,b.nama AS nama_kab, c.name AS nama_prov,d.nama AS nama_prodi,a.*');
		$this->db->from('mhs a');
		$this->db->join('kabupaten b', 'kab  = b.id','left');
		$this->db->join('provinsi c', 'prov = c.id','left');
		$this->db->join('prodi d', 'a.prodi = d.kode','left');
		$this->db->where("a.kode = $user");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data[0];
	}
	function getProv(){
		$this->db->select('*');
		$this->db->from('provinsi');
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getsetting($id){
		$this->db->select('*');
		$this->db->from('settings');
		$this->db->where("id = $id");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data[0];
	}
	function updateProfile($kode,$data){
		$this->db->where('id_mhs', $kode);
		$this->db->update('mhs', $data);
	}
	function update_finalisasi($id_mhs,$data){
		$this->db->where('id_mhs', $id_mhs);
		$this->db->update('mhs', $data);
	}
	function input_perhitungan($data,$kode){
		$this->db->where('kode', $kode);
		$this->db->update('mhs', $data);
	}
	function getUKT($ukt = 5,$prodi){
		$strSql = "select kel$ukt as hasil from besar_ukt where prodi = '$prodi' ";
		$query = $this->db->query($strSql);
		$data = $query->result_array();
		return $data[0];
	}









	function getKota($id = 0){
		$this->db->select('id,nama');
		$this->db->from('kabupaten');
		$this->db->where("id_prov = $id");
		$query = $this->db->get();
		$data = $query->result_array();
		foreach($data as $op){
			print_r ("<option value='".$op['id']."'>".$op['nama']."</option>");
		}
	}

	function hariIni(){
		$strSql = 'select DATE_FORMAT(NOW(), "%d-%M-%Y" ) AS tgl';
		$query = $this->db->query($strSql);
		$data = $query->result_array();
		//print_r ($data);
		return $data;
	}


	function getBesarUkt($kel,$prodi){
		$kel = "kel".$kel;
		$this->db->select($kel);
		$this->db->from('besar_ukt');
		$this->db->where("prodi = '$prodi'");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getTidakDU($kode){
		$this->db->select("*");
		$this->db->from('mhs');
		$this->db->where("jalur = '2'");
		$this->db->where("ver_pbb IS NULL");
		$this->db->where("ver_pln IS NULL");
		$this->db->where("kode = '$kode'");
		$data = $this->db->count_all_results();
		return $data;
	}
	function rekap(){
		//$this->db->select("a.*,b.nama as program,c.name as prov,d.nama as kab");
		$this->db->select("a.*,b.nama as program");
		$this->db->from('mhs a');
		$this->db->join('prodi b', 'a.prodi = b.kode');
		//$this->db->join('provinsi c', 'a.prov = c.id');
		//$this->db->join('kabupaten d', 'a.kab = d.id');
		$this->db->where("jalur = '4'");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function saveupload($kolom,$upload){
		$user = $this->session->userdata('id_mhs');
		$data = array(
			   $kolom => $upload
			);
		$this->db->where('id_mhs', $user);
		$this->db->update('mhs', $data);
	}





}
?>
