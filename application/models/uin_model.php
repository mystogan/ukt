<?php
class Uin_model extends CI_Model {
	function __construct(){
		parent::__construct();
	}
	function login($nip,$password){
		$this->db->select('*');
		$this->db->from("verivikator");
		$this->db->where('username =', $nip);
		$this->db->where('password =', md5($password));
		$query = $this->db->get();
		$hasil = $query->result_array();
		return $hasil;
	}
	function loginVerifikator($nip){
		$this->db->select('*');
		$this->db->from("verivikator");
		$this->db->where('username =', $nip);
		$query = $this->db->get();
		$hasil = $query->result_array();
		return $hasil;
	}
	function getSetting(){
		$this->db->select('id,ket,status');
		$this->db->select("DATE_FORMAT(tgl_mulai,'%Y-%m-%d') AS tgl_mulai",false);
		$this->db->select("DATE_FORMAT(tgl_akhir,'%Y-%m-%d') AS tgl_akhir",false);
		$this->db->select("DATE_FORMAT(tgl_mulai,'%d-%m-%Y') AS tgl_mulai1",false);
		$this->db->select("DATE_FORMAT(tgl_akhir,'%d-%m-%Y') AS tgl_akhir1",false);
		$this->db->select("DATEDIFF(CURRENT_DATE(), tgl_mulai) AS selisih_tgl_mulai",false);
		$this->db->select("DATEDIFF(CURRENT_DATE(), tgl_akhir) AS selisih_tgl_akhir",false);
		$this->db->from("settings");
		$query = $this->db->get();
		$hasil = $query->result_array();
		return $hasil;
	}
	function getJalur(){
		// SELECT `status` FROM settings WHERE id = 2
		$this->db->select('a.status,b.ket');
		$this->db->from("settings a");
		$this->db->join('jalur b', 'b.id = a.status');
		$this->db->where('a.id = ', '2');
		$query = $this->db->get();
		$hasil = $query->result_array();
		return $hasil[0];
	}
	function getAlljalur(){
		$this->db->select('*');
		$this->db->from("jalur");
		$query = $this->db->get();
		$hasil = $query->result_array();
		return $hasil;
	}
	function getAllMhs($prodi = "0"){
		$this->db->select('a.kode,a.nama,b.nama as prodi,a.rekom_1,c.ket as jalurr,a.mhs_finalisasi,a.*');
		$this->db->from("mhs a");
		$this->db->join('prodi b', 'b.kode = a.prodi');
		$this->db->join('jalur c', 'a.jalur = c.id');
		$this->db->where('a.status =', '1');
		$this->db->where('a.jalur = (SELECT `status` FROM settings WHERE id = 2)');
		if($prodi != "0"){
			$this->db->where('a.prodi =', $prodi);
		}
		$query = $this->db->get();
		$hasil = $query->result_array();
		return $hasil;
	}
	function ur($prodi = "0"){
		$strSql = 'SELECT ket FROM jalur WHERE id = (SELECT `status` FROM settings WHERE id = 2)';
		$query = $this->db->query($strSql);
		$data = $query->result_array();
		return $data[0];
	}

	function gantiJalur($kode,$data){
		// update setting
		$this->db->where('id', $kode);
		$this->db->update('settings', $data);
		$hasil = $this->db->affected_rows();
		$ip = $this->get_client_ip();
		$data = array(
		   'setting_id' => $kode ,
		   'slg_user' => $this->session->userdata('nip') ,
		   'slg_ubah' => json_encode($data),
		   'slg_ip' => $ip,
		   'slg_tgl' => date("Y-m-d h:i")
		);

		$this->db->insert('settings_log', $data);
		return $hasil;
	}

	// fungsi untuk mencari IP
	function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

function getDetilMhs($kode_mhs){
	$this->db->select('a.*,b.nama as prodi,c.ket as jalurr,d.nama AS nama_kab, e.name AS nama_prov');
	$this->db->from("mhs a");
	$this->db->join('prodi b', 'b.kode = a.prodi','left');
	$this->db->join('jalur c', 'a.jalur = c.id');
	$this->db->join('kabupaten d', 'kab  = d.id','left');
	$this->db->join('provinsi e', 'prov = e.id','left');
	$this->db->where('a.status =', '1');
	$this->db->where("a.kode = '$kode_mhs'");
	$query = $this->db->get();
	$hasil = $query->result_array();
	return $hasil[0];
}
function getMhs_perkelompok($jalur,$kel){
	$this->db->select('a.*,b.nama as prodi');
	$this->db->from("mhs a");
	$this->db->join('prodi b', 'b.kode = a.prodi');
	$this->db->join('jalur c', 'c.id = a.jalur');
	$this->db->where('a.status =', '1');
	$this->db->where("c.ket = '$jalur'");
	$this->db->where('a.kel_1 =', $kel);
	$query = $this->db->get();
	$hasil = $query->result_array();
	return $hasil;
}
function getAllMhsVer($jalur = 0){

	$this->db->select('mhs_submiter,kode,COUNT(kode) AS hasil');
	$this->db->from("mhs ");
	$this->db->where('mhs_submiter IS NOT NULL ');
	$this->db->where('tahun_masuk  = ', date("Y"));
	if ($jalur != 0) {
		$this->db->where('jalur  = ', $jalur);
	}else {
		$this->db->where('jalur  = (SELECT `status` FROM settings WHERE id = 2) ' );
	}
	$this->db->group_by("mhs_submiter");
	$query = $this->db->get();
	$hasil = $query->result_array();
	return $hasil;
}
function getUkt($jalur,$kolom = "kel_1"){
	$tahunnow = date("Y");
	$strSql = 'SELECT COUNT(jalur) AS hasil,"SNMPTN" AS jalur FROM mhs WHERE '.$kolom.' = '.$jalur.' AND jalur = 1 and tahun_masuk = "'.$tahunnow.'"
							UNION
							SELECT COUNT(jalur) AS hasil,"SBMPTN" AS jalur FROM mhs WHERE '.$kolom.' = '.$jalur.' AND jalur = 2 and tahun_masuk = "'.$tahunnow.'"
							UNION
							SELECT COUNT(jalur) AS hasil,"SPANPTKIN" AS jalur FROM mhs WHERE '.$kolom.' = '.$jalur.' AND jalur = 3 and tahun_masuk = "'.$tahunnow.'"
							UNION
							SELECT COUNT(jalur) AS hasil,"UMPTKIN" AS jalur FROM mhs WHERE '.$kolom.' = '.$jalur.' AND jalur = 4 and tahun_masuk = "'.$tahunnow.'"
							UNION
							SELECT COUNT(jalur) AS hasil,"MANDIRI" AS jalur FROM mhs WHERE '.$kolom.' = '.$jalur.' AND jalur = 5 and tahun_masuk = "'.$tahunnow.'"';
	$query = $this->db->query($strSql);
	$data = $query->result_array();
	return $data;
}
function getJumlahUkt(){
	$tahunnow = date("Y");
	$strSql = 'SELECT COUNT(*) as hasil FROM mhs WHERE tahun_masuk = '.$tahunnow.'
							and jalur = (SELECT `status` FROM settings WHERE id = 2)
							UNION
							SELECT COUNT(*) as hasil
							FROM mhs WHERE mhs_finalisasi = 1 AND tahun_masuk = '.$tahunnow.'
							and jalur = (SELECT `status` FROM settings WHERE id = 2);';
	$query = $this->db->query($strSql);
	$data = $query->result_array();
	return $data;
}


////////////////////////////////////////////



















function getMhs($id){
	$this->db->select('a.*,b.nama as prodi');
	$this->db->from("mhs a");
	$this->db->join('prodi b', 'b.kode = a.prodi');
	$this->db->where('a.status =', '1');
	$this->db->where('a.kel_1 =', $id);
	$this->db->where('a.jalur = (SELECT `status` FROM settings WHERE id = 2)');
	$query = $this->db->get();
	$hasil = $query->result_array();
	return $hasil;
}
function getAllMhsPustipd($id = 0){
	$this->db->select('d.*,a.*,b.nama as prodi,c.ket as jalurr');
	$this->db->from("mhs a");
	$this->db->join('prodi b', 'b.kode = a.prodi');
	$this->db->join('jalur c', 'a.jalur = c.id');
	$this->db->join('besar_ukt d', 'b.kode = d.prodi');
	$this->db->where('a.status =', '1');
	$this->db->where('a.jalur = (SELECT `status` FROM settings WHERE id = 2)');
	if($id != 0){
	$this->db->where('a.ver_tanah is null');
	$this->db->where('a.ver_spd is null');
	$this->db->where('a.ver_pln is null');

	}
	$this->db->where('a.tahun_masuk =', date("Y"));
	$query = $this->db->get();
	$hasil = $query->result_array();
	return $hasil;
}
function getAllMhsVerifikator($id = 0){
	$this->db->select('d.*,a.*,b.nama as prodi,c.ket as jalurr');
	$this->db->from("mhs a");
	$this->db->join('prodi b', 'b.kode = a.prodi');
	$this->db->join('jalur c', 'a.jalur = c.id');
	$this->db->join('besar_ukt d', 'b.kode = d.prodi');
	$this->db->where('a.status =', '1');
	$this->db->where('a.prodi =', $this->session->userdata('akses'));
	$this->db->where('a.jalur = (SELECT `status` FROM settings WHERE id = 2)');
	$this->db->where('a.tahun_masuk =', date("Y"));
	$query = $this->db->get();
	$hasil = $query->result_array();
	return $hasil;
}
function getAllMhsAkademik($id = 0){
	$this->db->select('d.*,a.*,b.nama as prodi,c.ket as jalurr, e.nama as nama_kab');
	$this->db->from("mhs a");
	$this->db->join('prodi b', 'b.kode = a.prodi');
	$this->db->join('jalur c', 'a.jalur = c.id');
	$this->db->join('besar_ukt d', 'b.kode = d.prodi');
	$this->db->join('kabupaten e', 'e.id = a.kab', 'left');
	$this->db->where('a.status =', '1');
	$this->db->where('a.jalur = 1');
	$this->db->where('a.tahun_masuk =', date("Y"));
	$query = $this->db->get();
	$hasil = $query->result_array();
	return $hasil;
}
	function getAllMhsLaporan($id = 0){
		$this->db->select('a.*,b.nama as prodi,c.ket as jalurr,d.nama AS nama_kab, e.name AS nama_prov,b.kode as kode_prodi');
		$this->db->from("mhs a");
		$this->db->join('prodi b', 'b.kode = a.prodi');
		$this->db->join('jalur c', 'a.jalur = c.id');
		$this->db->join('kabupaten d', 'kab  = d.id','left');
		$this->db->join('provinsi e', 'prov = e.id','left');
		$this->db->where('a.jalur =', '1');
		// $this->db->where('a.hasil =', '1');
		$this->db->where("a.tahun_masuk = ".date("Y"));
		// $this->db->where('a.jalur = (SELECT `status` FROM settings WHERE id = 2)');

		$query = $this->db->get();
		$hasil = $query->result_array();
		return $hasil;
	}
	function getKota($id = 0){
		$this->db->select('id,nama');
		$this->db->from('kabupaten');
		$this->db->where("id_prov = $id");
		$query = $this->db->get();
		$data = $query->result_array();
		foreach($data as $op){
			print_r ("<option value='".$op['id']."'>".$op['nama']."</option>");
		}
	}

	function getUkt2($id = "0",$jalur = "",$tabel){
		$this->db->select("COUNT(*) as hasil");
		$this->db->from($tabel);
		if($id != "0"){
			$this->db->where("kel_1 = $id");
		}
		if($jalur == ""){
			$this->db->where("jalur = 2");
		}else {
			$this->db->where("jalur = $jalur");
		}
		$query = $this->db->get();
		$data = $query->result_array();
		return $data[0];
	}


	function getJumSudah($prodi){
		$strSql = "SELECT COUNT(a.nama) AS jml
					FROM mhs a
					JOIN `prodi` b ON `a`.`prodi` = `b`.`kode`
					WHERE a.jalur = 4
					AND a.prodi = '$prodi'
					AND a.pbb IS NOT NULL
					AND a.pln IS NOT NULL
					AND a.pdam IS NOT NULL
					AND kel_2 IS NULL
					AND kel_3 IS NULL";
		$query = $this->db->query($strSql);
		$data = $query->result_array();
		//print_r ($data);
		return $data;
	}
	function getgetmbohwes($prodi){
		$strSql = "SELECT * FROM mhs WHERE tahun_masuk = 2018 AND kel_1 IS NULL AND ver_tanah IS NOT NULL";
		$query = $this->db->query($strSql);
		$data = $query->result_array();
		//print_r ($data);
		return $data;
	}
	function getuktMhs(){
		$strSql = "SELECT b.nama as nama_prodi ,a.* FROM besar_ukt a JOIN prodi b ON a.prodi = b.kode";
		$query = $this->db->query($strSql);
		$data = $query->result_array();
		//print_r ($data);
		return $data;
	}
	function getJumBelum($prodi){
		$strSql = "SELECT COUNT(a.nama) AS jml
					FROM mhs a
					JOIN `prodi` b ON `a`.`prodi` = `b`.`kode`
					WHERE a.jalur = 4
					AND a.prodi = '$prodi'
					AND a.pbb IS NULL
					AND a.pln IS NULL
					AND a.pdam IS NULL
					AND kel_2 IS NULL
					AND kel_3 IS NULL";
		$query = $this->db->query($strSql);
		$data = $query->result_array();
		//print_r ($data);
		return $data;
	}
	function getProv(){
		$this->db->select('*');
		$this->db->from('provinsi');
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getProdi(){
		$strSql = "SELECT DISTINCT(a.prodi) AS kode ,b.nama
					FROM mhs a
					JOIN prodi b ON a.prodi = b.kode
					WHERE a.jalur = 4";
		$query = $this->db->query($strSql);
		$data = $query->result_array();
		return $data;
	}

	function updateProfile($kode,$data){
		$this->db->where('kode', $kode);
		$this->db->update('mhs', $data);
	}
	function saveupload($kolom,$upload){
		$user = $this->session->userdata('kode');
		$data = array(
			   $kolom => $upload
			);
		$this->db->where('kode', $user);
		$this->db->update('mhs', $data);
	}


}
?>
