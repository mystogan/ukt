<?php
class Simpeg_model extends CI_Model {
	function __construct(){
		parent::__construct();
		$this->db2 = $this->load->database('simpeg', TRUE);
	}

	function login($nip,$password,$unit){
		$this->db2->select('tbpegawai.id,nip,nama,id_unittugastambahan,id_unitkerja');
		$this->db2->from("tbpegawai ");
		$this->db2->join('tbsimpeguser', 'nip = username ');
		$this->db2->where('username =', $nip);
		$this->db2->where('password =', $password);
		$this->db2->where("(id_unitkerja = $unit OR id_unittugastambahan = $unit)");
		$query = $this->db2->get();
		$data = $query->result_array();
		return $data[0];
	}

	function getunitpustipd(){
		$this->db2->select('id');
		$this->db2->from('m_unit');
		$this->db2->where('nama_unit like "%Pusat%Teknologi%"');
		$query = $this->db2->get();
		$data = $query->result_array();
		return $data[0];
	}
	function loginVerifikator($nip,$password){
		$this->db2->select('tbpegawai.id,nip,nama,id_unittugastambahan,id_unitkerja');
		$this->db2->from("tbpegawai ");
		$this->db2->join('tbsimpeguser', 'nip = username ');
		$this->db2->where('username =', $nip);
		$this->db2->where('password =', $password);
		$query = $this->db2->get();
		$data = $query->result_array();
		return $data[0];
	}

}
?>
