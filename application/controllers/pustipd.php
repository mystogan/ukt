<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pustipd extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->database();
		$this->load->model('Uin_model');
		$this->load->helper(array('form','url','file','download'));
		// error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		date_default_timezone_set("Asia/Bangkok");
		$user = $this->session->userdata('nip');
		if ($user == null) {
				header("location:".base_url()."uin");
		}
	}
	public function index(){
		$this->load->model('Siakad_model');
		$data['satu'] = $this->Uin_model->getUkt(1);
		$data['dua'] = $this->Uin_model->getUkt(2);
		$data['tiga'] = $this->Uin_model->getUkt(3);
		$data['empat'] = $this->Uin_model->getUkt(4);
		$data['lima'] = $this->Uin_model->getUkt(5);
		$data['enam'] = $this->Uin_model->getUkt(6);
		$data['tujuh'] = $this->Uin_model->getUkt(7);
		$data['jumlah'] = $this->Siakad_model->getjummhs();
		// print_r ($data['jumlah']);
		$set = $this->Uin_model->getSetting();
		$data['jalur'] = $set[1]['status'];
		$this->load->view('pustipd/header',$data);
		$this->load->view('pustipd/dashboard',$data);
	}
	public function skip(){
		$this->load->model('Siakad_model');
		$data['satu'] = $this->Uin_model->getUkt(1);
		$data['dua'] = $this->Uin_model->getUkt(2);
		$data['tiga'] = $this->Uin_model->getUkt(3);
		$data['empat'] = $this->Uin_model->getUkt(4);
		$data['lima'] = $this->Uin_model->getUkt(5);
		$data['enam'] = $this->Uin_model->getUkt(6);
		$data['tujuh'] = $this->Uin_model->getUkt(7);
		$data['jumlah'] = $this->Siakad_model->getjummhs();
		// print_r ($data['jumlah']);
		$set = $this->Uin_model->getSetting();
		$data['jalur'] = $set[1]['status'];
		$this->load->view('pustipd/header',$data);
		$this->load->view('pustipd/skip',$data);
	}
	public function index2(){
		$this->load->view('pustipd/header',$data);
		$this->load->view('pustipd/index',$data);

	}
	public function mhs(){
		$data['mhs'] = $this->Uin_model->getAllMhsPustipd();
		$data['jalur'] = $this->Uin_model->getJalur();
		$this->load->view('pustipd/header',$data);
		$this->load->view('pustipd/detilkelompok',$data);
	}
	public function uktMhs(){
		$data['mhs'] = $this->Uin_model->getuktMhs();
		$this->load->view('pustipd/header',$data);
		$this->load->view('pustipd/harga_jalur',$data);
	}
	public function dataver(){
		$jalur = $this->input->post('jalur');
		$data['mhs'] = $this->Uin_model->getAllMhsVer($jalur);
		$data['jalur'] = $this->Uin_model->getAlljalur();
		if ($jalur == null) {
			$hasil = $this->Uin_model->getJalur();
			$jalur = $hasil['status'];
			
		}
		// print_r ($data['jalur']);
		$data['jalurs'] = $jalur;
		$this->load->view('pustipd/header',$data);
		$this->load->view('pustipd/data-ver',$data);
	}
	public function detil($kode_mhs){
		// $kode_mhs = $this->input->post('kode_mhs');
		$data['mhs'] = $this->Uin_model->getDetilMhs($kode_mhs);
		// print_r ($data['mhs']);
		$this->load->view('pustipd/header',$data);
		$this->load->view('pustipd/detil',$data);
	}
	public function settings(){
		$set = $this->Uin_model->getSetting();
		$data['pengumumanUKT'] = $set[0]['tgl_mulai'];
		$data['jalur'] = $set[1]['status'];
		$data['tgl_berkas_mulai'] = $set[2]['tgl_mulai'];
		$data['tgl_berkas_akhir'] = $set[2]['tgl_akhir'];
		$data['verifikator_mulai'] = $set[4]['tgl_mulai'];
		$data['verifikator_akhir'] = $set[4]['tgl_akhir'];
		$data['buka_ukt'] = $set[3]['status'];

		$this->load->view('pustipd/header',$data);
		$this->load->view('pustipd/setting',$data);

	}
	public function gantiJalur(){
		$jalur = $this->input->post('jalur');
		$data['status'] = $jalur;
		$set = $this->Uin_model->gantiJalur(2,$data);
		echo $set;

	}
	public function bukaUKT(){
		$id = $this->input->post('id');
		$status = $this->input->post('status');
		$data['status'] = $status;
		$set = $this->Uin_model->gantiJalur($id,$data);
		echo $set;

	}
	public function gantiTgl(){
		$id = $this->input->post('id');
		$tgl_mulai = $this->input->post('tgl_mulai');
		$kolom = $this->input->post('kolom');
		$data["$kolom"] = $tgl_mulai." 23:59:59";
		$set = $this->Uin_model->gantiJalur($id,$data);
		echo $set;

	}
	public function panggil_hitung($kode){
		require_once(APPPATH.'controllers/perhitungan.php');
		$proses = new Perhitungan();
		$proses->pilih_ukt($kode,"pustipd/mhs");
	}
	public function validasi(){
		$this->load->model('Home_model');
		$kode = $this->input->post('kode');
		$id_mhs = $this->input->post('id_mhs');
		$nama_kolom = $this->input->post('nama_kolom');
		$nama_kolom_edit = $this->input->post('nama_kolom_edit');
		$isi_edit = $this->input->post('isi_edit');

		$data[$nama_kolom] = '1';
		$data[$nama_kolom_edit] = $isi_edit;
		$data["mhs_submiter"] = $this->session->userdata('nip');;
		// print_r ($data);
		$this->Home_model->updateProfile($id_mhs,$data);
		$mhs = $this->Home_model->getProfile($kode);

		if ($mhs['ver_tanah'] == '1' && $mhs['ver_bangunan'] == '1' && $mhs['ver_spd'] == '1'
			&& $mhs['ver_atap'] == '1' && $mhs['ver_lantai'] == '1' && $mhs['ver_pbb'] == '1'
			&& $mhs['ver_pln'] == '1' && $mhs['ver_pdam'] == '1' && $mhs['ver_ayah'] == '1'
			&& $mhs['ver_ibu'] == '1' && $mhs['ver_anggota'] == '1'){
				if ($mhs['pilih_ukt'] == 1) {
					if($mhs['ver_alat_kom'] == '1' && $mhs['ver_srt_mrk'] == '1' &&
					$mhs['ver_penyakit'] == '1' && $mhs['ver_panti_yatim'] == '1' &&
					$mhs['ver_surat_asli'] == '1' ){
						// proses perhitungan fuzzy
						$this->panggil_hitung($kode);
					}else {
						if($mhs['ver_alat_kom'] != '' && $mhs['ver_srt_mrk'] != '' &&
						$mhs['ver_penyakit'] != '' && $mhs['ver_panti_yatim'] != '' &&
						$mhs['ver_surat_asli'] != '' ){
							$this->panggil_hitung($kode);
							header("location:".base_url()."pustipd/mhs");
						}else {
							header("location:".base_url()."pustipd/detil/".$kode);
						}
					}
				}else if ($mhs['pilih_ukt'] == 2) {
					if($mhs['ver_tetangga'] == '1' && $mhs['ver_sktm'] == '1' &&
						$mhs['ver_anakkuliah'] == '1'  ){
						// proses perhitungan fuzzy
						$this->panggil_hitung($kode);
					}else {
						if($mhs['ver_tetangga'] != '' && $mhs['ver_sktm'] != '' &&
						$mhs['ver_anakkuliah'] != ''  ){
							$this->panggil_hitung($kode);
							header("location:".base_url()."pustipd/mhs");
						}else {
							header("location:".base_url()."pustipd/detil/".$kode);
						}
					}
				}else if($mhs['pilih_ukt'] == 3){
					if($mhs['ver_tetangga'] >= '1'){
						// proses perhitungan fuzzy
						$this->panggil_hitung($kode);
					}else {
						if($mhs['ver_tetangga'] != ''  ){
							$this->panggil_hitung($kode);
							header("location:".base_url()."pustipd/");
						}else {
							header("location:".base_url()."pustipd/detil/".$kode);
						}
					}
				}else {
					// proses perhitungan pilih ukt
					// echo "proses perhitungan pilih ukt 4,5,6,7";
					$this->panggil_hitung($kode);
				}

			}else {
				header("location:".base_url()."pustipd/detil/".$kode);
			}
	}


}
