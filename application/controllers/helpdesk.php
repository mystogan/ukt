<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Helpdesk extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->database();
		$this->load->model('Uin_model');
		$this->load->helper(array('form','url','file','download'));
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		date_default_timezone_set("Asia/Bangkok");
		$user = $this->session->userdata('nip');
		if ($user == null) {
				header("location:".base_url()."uin");
		}
	}
	public function index(){
        $data['mhs'] = $this->Uin_model->getAllMhsPustipd();
        $data['jalur'] = $this->Uin_model->getJalur();
        $this->load->view('helpdesk/header', $data);
        $this->load->view('helpdesk/detilkelompok', $data);
    }
	public function updatetgl_lahir(){
		$idMHS = trim($this->input->post('idMHS'));
		$kodemhs = trim($this->input->post('kodemhs'));
		$data['tgl_lhr'] = trim($this->input->post('tgl_lhr'));
		$this->db->set($data);
		$this->db->where('kode', $kodemhs);
		$this->db->update('mhs');
		header("location:".base_url()."helpdesk");
    }
	public function updatefinalisasi($kodemhs){
		$data['mhs_finalisasi'] = '0';
		$this->db->set($data);
		$this->db->where('kode', $kodemhs);
		$this->db->update('mhs');
		header("location:".base_url()."helpdesk");
    }
}
