<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	function __construct(){
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->database();
		$this->load->model('Home_model');
		$this->load->helper(array('form','url','file','download'));
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		date_default_timezone_set("Asia/Bangkok");

	}
	public function index(){
		$user = $this->session->userdata('kode');
		$this->load->model('Uin_model');
		$jal = $this->Uin_model->getJalur();
		
		if($user == null){
			$set = $this->Uin_model->getSetting();
			$data['set'] = $set;
			$data['jalur'] = $jal['ket'];
			$buka_ukt = $set[3]['status'];
			if($buka_ukt == 1){
				$data['tgl_berkas_mulai'] = $set[2]['tgl_mulai'];
				$data['tgl_berkas_akhir'] = $set[2]['tgl_akhir'];
				$this->load->view('mhs/index',$data);
			}else {
				$this->load->view('notif',$data);
			}
		}
		else {
			$profile['profil']  = $this->Home_model->getProfile($user);
			$profile['setting']  = $this->Home_model->getsetting(1);
			$profile['prov']  = $this->Home_model->getProv();
			$profile['jalur']  = $jal['ket'];
			$sekarang = date("Y-m-d");
			if($profile['profil']['jalur'] == 1){
				$tanggalpengumuman = '2019-04-08';
			}else if($profile['profil']['jalur'] == 2){
				$tanggalpengumuman = '2019-06-06';
			}else if($profile['profil']['jalur'] == 3){
				$tanggalpengumuman = '2019-04-11';
			}else if($profile['profil']['jalur'] == 4){
				$tanggalpengumuman = '2019-06-11';
			}
			if($sekarang >= $tanggalpengumuman){
				if($profile['profil']['kel_1'] == 0){
					echo "<script>alert ('Terjadi Kesalahan Silahkan Menghubungi Pihak Kampus');
						window.location.href = '".base_url()."home/logout';</script>";
				}else {
					$profile['ukt']  = $this->Home_model->getUKT($profile['profil']['kel_1'],$profile['profil']['prodi']);
					$this->load->view('mhs/pengumuman',$profile);	
					// $this->load->view('mhs/pengumuman',$profile);	
				}
			}else {
				if ($profile['profil']['mhs_finalisasi'] == 0) {
					$this->load->view('mhs/header',$profile);
					$this->load->view('mhs/form_mahasiswa',$profile);
					$this->load->view('mhs/modal_mhs',$profile);
				}else {
					$this->load->view('mhs/header',$profile);
					$this->load->view('mhs/form_mhs_finalisasi',$profile);
				}
			}
		}
	}
	
	public function getKota($id){
		$data = $this->Home_model->getKota($id);
	}
	public function cek(){
		$cek = 0;
		$pendaftaran = $this->input->post('pendaftaran');
		$hari = $this->input->post('hari');
		$bulan = $this->input->post('bulan');
		$tahun = $this->input->post('tahun');
		$tgl = $hari."-".$bulan."-".$tahun;

		$this->load->model('Uin_model');
		$set = $this->Uin_model->getSetting(1);
		$selisih_tgl_mulai = $set[2]['selisih_tgl_mulai'];
		$selisih_tgl_akhir = $set[2]['selisih_tgl_akhir'];

		$tgl_mulai = date_format(date_create($set[2]['tgl_mulai']),"d-m-Y ");
		$pengumumanUKT = date_format(date_create($set[0]['tgl_mulai']),"d-m-Y ");

		// cek apakah tanggal yang sudah di tetapkan sudah terlewat dengan sekarang apa tidak
		if($selisih_tgl_mulai < 0){
			$cek = 1;
			echo "<script>alert ('Pengisian Berkas Belum dibuka ! Akan dibuka pada tanggal $tgl_mulai');
						window.location.href = '".base_url()."';</script>";
		}

		if ($cek == 0) {
			$data = $this->Home_model->login($pendaftaran,$tgl);
			if($data['id_mhs'] == ""){
				echo "<script>alert ('Kode Pendaftaran/Tanggal Lahir Salah !');window.location.href = '".base_url()."';</script>";
			}else if($data >= 1){
				$jalur = $this->Home_model->getsetting(6);
				$setting = json_decode($jalur['value']);
				$cekjalur = 0;
				foreach ($setting->ready as $value) {
					if($value == $data['jalur']){
						$this->session->set_userdata('kode',$data['kode']);
						$this->session->set_userdata('id_mhs',$data['id_mhs']);
						$this->session->set_userdata('nama',$data['nama']);
						$this->session->set_userdata('tgl_lhr',$data['tgl_lhr']);	
						$cekjalur = 1;
						break;
					}else {
						$cekjalur = 0;
					}
				}
				if ($cekjalur == 1) {
					echo "<script>window.location.href = '".base_url()."';</script>";					
				}else {
					echo "<script>alert ('Untuk Jalur Anda Sudah ditutup');
						window.location.href = '".base_url()."';</script>";
				}
			}else {
				echo "<script>window.location.href = '".base_url()."';</script>";
			}

		}
	}
	public function logout(){
		$this->session->unset_userdata(array('kode' => ''));
		$this->session->unset_userdata(array('id_mhs' => ''));
		$this->session->unset_userdata(array('nama' => ''));
		$this->session->unset_userdata(array('tgl_lhr' => ''));
		header("location:../");
	}
	public function jajal($kode){
		$profile['a'] = $this->Home_model->getTidakDU($kode);
		$profile['tgl'] = $this->Home_model->hariIni();
		$profile['profile']  = $this->Home_model->getProfile($kode);
		$profile['ukt'] = $this->Home_model->getBesarUkt($profile['profile'][0]['kel_1'],$profile['profile'][0]['prodi']);
		$profile['kolom'] = "kel".$profile['profile'][0]['kel_1'];
		$this->load->view('form_mahasiswa',$profile);
	}
	public function pengumuman($kode){
		$profile['a'] = $this->Home_model->getTidakDU($kode);
		$profile['tgl'] = $this->Home_model->hariIni();
		$profile['profile']  = $this->Home_model->getProfile($kode);
		$profile['ukt'] = $this->Home_model->getBesarUkt($profile['profile'][0]['kel_1'],$profile['profile'][0]['prodi']);
		$profile['kolom'] = "kel".$profile['profile'][0]['kel_1'];
		$this->load->view('form_mahasiswa',$profile);
	}
	public function save1(){
		$kode_mhs = $this->input->post('id_mhsh');
		if($kode_mhs == null ){			
			$kode_mhs = $this->session->userdata('id_mhs');
		}
		$alamat = $this->input->post('alamat');
		$gol_darah = $this->input->post('gol_darah');
		$provinsi = $this->input->post('provinsi');
		$kota = $this->input->post('kota');
		$kode_pos = $this->input->post('kode_pos');
		$telp_rumah = $this->input->post('telp_rumah');
		$hp = $this->input->post('hp');
		$email = $this->input->post('email');
		$agama = $this->input->post('agama');
		$warganegara = $this->input->post('warganegara');
		$ukuran_baju = $this->input->post('ukuran_baju');
		$data = array(
			   'alamat' => $alamat ,
			   'gol_dar' => $gol_darah,
			   'prov' => $provinsi,
			   'kab' => $kota,
			   'ukuran_baju' => $ukuran_baju,
			   'kode_pos' => $kode_pos,
			   'tlp_rmh' => $telp_rumah,
			   'hp' => $hp,
			   'email' => $email,
			   'agama' => $agama,
			   'warganegara' => $warganegara
			);
			// echo $kode_mhs;
			// print_r ($data);
		$this->Home_model->updateProfile($kode_mhs,$data);
		$this->session->set_userdata('tabsesion','1');
		header("location:../");
	}

	public function save2(){
		$kode = $this->input->post('id_mhsh');
		if($kode == null ){			
			$kode = $this->session->userdata('id_mhs');
		}
		
		$peker_ayah = $this->input->post('peker_ayah');
		$peng_ayah = $this->input->post('peng_ayah');
		$nama_ayah = $this->input->post('nama_ayah');
		if($peker_ayah == "1"){
			$peker_ayah = $this->input->post('pekeer_ayah');
		}

		
		$nama_ibu = $this->input->post('nama_ibu');
		$peker_ibu = $this->input->post('peker_ibu');
		if($peker_ibu == "1"){
			$peker_ibu = $this->input->post('pekeer_ibu');
		}
		$peng_ibu = $this->input->post('peng_ibu');
		$peng_sendiri = $this->input->post('peng_sendiri');
		
		$alamat_ortu = $this->input->post('alamat_ortu');
		$saudara = $this->input->post('saudara');
		$hp_ortu = $this->input->post('hp_ortu');
		$total = $peng_ayah+$peng_ibu;

		$data = array(
			'nama_ayah' => $nama_ayah ,
			'peng_ayah' => $peng_ayah ,
			'peker_ayah' => $peker_ayah,
			'nama_ibu' => $nama_ibu,
			'peng_ibu' => $peng_ibu,
			'peng_sendiri' => $peng_sendiri,
			'peker_ibu' => $peker_ibu,
			'alamat_ortu' => $alamat_ortu,
			'anggota' => $saudara,
			'hp_ortu' => $hp_ortu,
			'penghasilan_total' => $total
		);
		
		$this->Home_model->updateProfile($kode,$data);
		$this->session->set_userdata('tabsesion','2');
		header("location:../");
	}
	public function save3(){
		$kode = $this->input->post('id_mhsh');
		if($kode == null ){			
			$kode = $this->session->userdata('id_mhs');
		}
		$ukt = $this->input->post('ukt');
		$pbb = $this->input->post('pbb');
		$pln = $this->input->post('pln');
		$pdam = $this->input->post('pdam');
		$luas_tanah = $this->input->post('luas_tanah');
		$luas_bangunan = $this->input->post('luas_bangunan');
		$kepemilikan = $this->input->post('kepemilikan');
		$sum_listrik = $this->input->post('listrik');
		$sum_air = $this->input->post('sum_air');
		$alat_kom = $this->input->post('alat_kom');

		$data['pilih_ukt'] = $ukt;
		$data['pbb'] = $pbb;
		$data['pln'] = $pln;
		$data['pdam'] = $pdam;
		$data['luas_tanah'] = $luas_tanah;
		$data['luas_bangunan'] = $luas_bangunan;
		$data['kepemilikan'] = $kepemilikan;
		$data['sum_listrik'] = $sum_listrik;
		$data['sum_air'] = $sum_air;
		$data['alat_kom'] = $alat_kom;

		if($ukt == 2){
			$data['upload_kjs'] = '';
			$data['upload_alat_kom'] = '';
			$data['upload_srt_mrk'] = '';
			$data['upload_penyakit'] = '';
			$data['upload_panti_yatim'] = '';
			$data['upload_tetangga'] = '';
			$data['upload_surat_asli'] = '';
		}else if ($ukt == 3) {
			$data['upload_kjs'] = '';
			$data['upload_alat_kom'] = '';
			$data['upload_srt_mrk'] = '';
			$data['upload_penyakit'] = '';
			$data['upload_panti_yatim'] = '';
			$data['upload_surat_asli'] = '';
			$data['upload_sktm'] = '';
			$data['upload_anakkuliah'] = '';
		}else if($ukt == 1){
			$data['upload_tetangga'] = '';
			$data['upload_surat_asli'] = '';
			$data['upload_sktm'] = '';
			$data['upload_anakkuliah'] = '';
		}
		//print_r ($data);
		$this->Home_model->updateProfile($kode,$data);
		$this->session->set_userdata('tabsesion','3');
		header("location:".base_url());
	}

	public function upload(){
		$foto = $_FILES["foto"]["name"];
		$simpan = $_FILES["pbb"]["name"];
		$stnk = $_FILES["stnk"]["name"];
		$atap_rumah = $_FILES["atap_rumah"]["name"];
		$lantai_rumah = $_FILES["lantai_rumah"]["name"];
		$pln = $_FILES["pln"]["name"];
		$pdam = $_FILES["pdam"]["name"];
		$gaji = $_FILES["gaji"]["name"];
		$gaji_ibu = $_FILES["gaji_ibu"]["name"];
		$kk = $_FILES["kk"]["name"];
		$rmh = $_FILES["rmh"]["name"];
		$skl = $_FILES["skl"]["name"];
		$kjs = $_FILES["kjs"]["name"];
		$upload_datavalid = $_FILES["upload_datavalid"]["name"];
		$rapor = $_FILES["rapor"]["name"];
		$kesanggupan = $_FILES["kesanggupan"]["name"];
		

		if($kjs != "" || $kjs != null){
			$this->loop($kjs,"kjs","upload_kjs");
		}
		if($rmh != "" || $rmh != null){
			$this->loop($rmh,"rmh","upload_rmh");
		}
		if($foto != "" || $foto != null){
			$this->loop($foto,"foto","foto");
		}
		if($simpan != "" || $simpan != null){
			$this->loop($simpan,"pbb","upload_pbb");
		}
		if($stnk != "" || $stnk != null){
			$this->loop($stnk,"stnk","upload_stnk");
		}
		if($atap_rumah != "" || $atap_rumah != null){
			$this->loop($atap_rumah,"atap_rumah","upload_atap");
		}
		if($lantai_rumah['name'] != "" || $lantai_rumah['name'] != null){
			$this->loop($lantai_rumah,"lantai_rumah","upload_lantai");
		}
		if($pln['name'] != "" || $pln['name'] != null){
			$this->loop($pln,"pln","upload_pln");
		}
		if($pdam['name'] != "" && $pdam['name'] != null){
			$this->loop($pdam,"pdam","upload_pdam");
		}
		if($gaji['name'] != "" && $gaji['name'] != null){
			$this->loop($gaji,"gaji","upload_gaji_ayah");
		}
		if($gaji_ibu['name'] != "" && $gaji_ibu['name'] != null){
			$this->loop($gaji_ibu,"gaji_ibu","upload_gaji_ibu");
		}
		if($kk['name'] != "" && $kk['name'] != null){
			$this->loop($kk,"kk","upload_kk");
		}
		if($skl['name'] != "" && $skl['name'] != null){
			$this->loop($skl,"skl","upload_skl");
		}
		if($upload_datavalid['name'] != "" && $upload_datavalid['name'] != null){
			$this->loop($upload_datavalid,"upload_datavalid","upload_datavalid");
		}
		if($rapor['name'] != "" && $rapor['name'] != null){
			$this->loop($rapor,"rapor","upload_rapor");
		}
		if($kesanggupan['name'] != "" && $kesanggupan['name'] != null){
			$this->loop($kesanggupan,"kesanggupan","upload_kesanggupan");
		}
		//header("location:../");
		 echo "<script>window.location.href = '".base_url()."';</script>";
	}

	function loop($simpan,$nama,$kolom){
		$sub = substr($simpan,-5);
		$string = date("Y")."/".uniqid().$sub;


		$target_dir = "assets/foto/";
		$target_file = $target_dir . $string;
		$uploadOk = 1;
		$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

		if(isset($_POST["submit"])) {
			$check = getimagesize($_FILES[$nama]["tmp_name"]);
			if($check !== false) {
				$uploadOk = 1;
			} else {
				$uploadOk = 0;
			}
		}

		if($imageFileType != "jpg" && $imageFileType != "png" &&
	 			$imageFileType != "JPG" && $imageFileType != "PNG" &&
				$imageFileType != "jpeg" && $imageFileType != "JPEG" &&
				$imageFileType != "pdf" && $imageFileType != "PDF" &&
				$imageFileType != "gif" && $imageFileType != "GIF"  ) {
			echo "<script> alert ('Format Harus JPG, JPEG, PNG & GIF !')</script>";
			$uploadOk = 0;
		}
		if ($uploadOk == 0) {
			echo "<script> alert ('Error Waktu Uploads !')</script>";
		} else {
			if (move_uploaded_file($_FILES[$nama]["tmp_name"], $target_file)) {
				$this->Home_model->saveupload($kolom,$string);
				// echo $kolom." ".$string;
			} else {
				echo "<script> alert ('Error, Cek Ukuran Gambar !')</script>";

				// print_r($_FILES);
			}
		}
	}

	public function kelompok_1($data){
		$total_1 = 0;
		if($data['ver_tanah'] == 2){
			//$total_1 = $total_1 + 9;
		}else {
			if($data['luas_tanah'] >= 50 && $data['luas_tanah'] <= 84){
				$total_1 = $total_1 + 9;
				//echo $total_1."a1a  <br/>";
			}
		}
		if($data['ver_bangunan'] == 2){
			//$total_1 = $total_1 + 8;
		}else {
			if($data['luas_bangunan'] >= 21 && $data['luas_bangunan'] <= 36){
				$total_1 = $total_1 + 8;
				//echo $total_1."a2a <br/>";
			}
		}
		if($data['ver_spd'] == 2){
			//$total_1 = $total_1 + 8;
		}else {
			if($data['ver_spd'] >= 1 ){
				$total_1 = $total_1 + 8;
				//echo $total_1."a3a <br/>";
			}
		}

		/// kedua
		if($data['ver_pbb'] == 2){
			//$total_1 = $total_1 + 9;
		}else {
			if($data['pbb'] <= 100000 ){
				$total_1 = $total_1 + 9;
			}
		}

		if($data['ver_pln'] == 2){
			//$total_1 = $total_1 + 8;
		}else {
			if($data['pln'] <= 450 ){
				$total_1 = $total_1 + 8;
			}
		}

		if($data['ver_pdam'] == 2){
			//$total_1 = $total_1 + 8;
		}else {
			if($data['pdam'] <= 50000 ){
				$total_1 = $total_1 + 8;
			}
		}
		//// tigaaaa
		if($data['ver_ayah'] == 2){
			//$total_1 = $total_1 + 8;
		}else {
			if($data['peng_ayah'] >= 500000 && $data['peng_ayah'] <= 1000000){
				$total_1 = $total_1 + 8;
			}

		}

		if($data['ver_ibu'] == 2){
			//$total_1 = $total_1 + 8;
		}else {
			if($data['peng_ibu'] >= 750000){
				$total_1 = $total_1 + 0;
			}else if($data['peng_ibu'] >= 500000 && $data['peng_ibu'] <= 1000000){
				$total_1 = $total_1 + 8;
			}
		}
		$total_peng = $data['peng_ayah'] + $data['peng_ibu'];

		if($total_peng >= 1500000 && $total_peng <= 2000000){
			$total_1 = $total_1 + 9;
		}
		$bagi = $total_peng/$data['anggota'];


		if($data['ver_anggota'] == 2){
			//$total_1 = $total_1 + 25;
		}else {
			if($bagi >= 500000 ){
				$total_1 = $total_1 + 25;
				//echo $total_1."a2a <br/>";
			}
		}

		return $total_1;
	}
	public function kelompok_2($data){
		$total_1 = 0;
		if($data['ver_tanah'] == 2){
			//$total_1 = $total_1 + 9;
		}else {
			if($data['luas_tanah'] >= 90 && $data['luas_tanah'] <= 105){
				$total_1 = $total_1 + 9;
				//echo $total_1."a1a  <br/>";
			}
		}
		if($data['ver_bangunan'] == 2){
			//$total_1 = $total_1 + 8;
		}else {
			if($data['luas_bangunan'] >= 45 && $data['luas	_bangunan'] <= 54){
				$total_1 = $total_1 + 8;
				//echo $total_1."a2a <br/>";
			}
		}
		if($data['ver_spd'] == 2){
			//$total_1 = $total_1 + 8;
		}else {
			if($data['ver_spd'] >= 1 ){
				$total_1 = $total_1 + 8;
				//echo $total_1."a3a <br/>";
			}
		}

		/// kedua
		if($data['ver_pbb'] == 2){
			//$total_1 = $total_1 + 9;
		}else {
			if($data['pbb'] <= 150000 ){
				$total_1 = $total_1 + 9;
			}
		}

		if($data['ver_pln'] == 2){
			//$total_1 = $total_1 + 8;
		}else {
			if($data['pln'] <= 900 ){
				$total_1 = $total_1 + 8;
				//echo $total_1."a2a <br/>";
			}
		}

		if($data['ver_pdam'] == 2){
			//$total_1 = $total_1 + 8;
		}else {
			if($data['pdam'] <= 100000 ){
				$total_1 = $total_1 + 8;
				//echo $total_1."a2a <br/>";
			}
		}
		//// tigaaaa
		if($data['ver_ayah'] == 2){
			//$total_1 = $total_1 + 8;

		}else {
			if($data['peng_ayah'] >= 1000000 && $data['peng_ayah'] <= 2000000){
				$total_1 = $total_1 + 8;
			}

		}

		if($data['ver_ibu'] == 2){
			//$total_1 = $total_1 + 8;
		}else {
			if($data['peng_ibu'] <= 750000){
				$total_1 = $total_1 + 0;
			}else if($data['peng_ibu'] >= 1000000 && $data['peng_ibu'] <= 2000000){
				$total_1 = $total_1 + 8;
			}
		}
		$total_peng = $data['peng_ayah'] + $data['peng_ibu'];

		if($total_peng >= 1500000 ){
			$total_1 = $total_1 + 9;
		}
		$bagi = $total_peng/$data['anggota'];


		if($data['ver_anggota'] == 2){
			//$total_1 = $total_1 + 25;
		}else {
			if($bagi >= 750000 ){
				$total_1 = $total_1 + 25;
				//echo $total_1."a2a <br/>";
			}
		}
		return $total_1;
	}

	public function kelompok_3($data){
		$total_1 = 0;
		if($data['ver_tanah'] == 2){
			//$total_1 = $total_1 + 9;
		}else {
			if($data['luas_tanah'] >= 90 && $data['luas_tanah'] <= 105){
				$total_1 = $total_1 + 9;
				//echo $total_1."a1a  <br/>";
			}
		}
		if($data['ver_bangunan'] == 2){
			$total_1 = $total_1 + 8;
		}else {
			if($data['luas_bangunan'] >= 45 && $data['luas	_bangunan'] <= 54){
				$total_1 = $total_1 + 8;
			}
		}
		if($data['ver_spd'] == 2){
			//$total_1 = $total_1 + 8;
		}else {
			if($data['ver_spd'] >= 1 ){
				$total_1 = $total_1 + 8;
				//echo $total_1."a3a <br/>";
			}
		}

		/// kedua
		if($data['ver_pbb'] == 2){
			$total_1 = $total_1 + 9;
		}else {
			if($data['pbb'] <= 150000 ){
				$total_1 = $total_1 + 9;
			}
		}

		if($data['ver_pln'] == 2){
			$total_1 = $total_1 + 8;
		}else {
			if($data['pln'] <= 900 ){
				$total_1 = $total_1 + 8;
				//echo $total_1."a2a <br/>";
			}
		}

		if($data['ver_pdam'] == 2){
			$total_1 = $total_1 + 8;
		}else {
			if($data['pdam'] <= 100000 ){
				$total_1 = $total_1 + 8;
				//echo $total_1."a2a <br/>";
			}
		}
		//// tigaaaa
		if($data['ver_ayah'] == 2){
			$total_1 = $total_1 + 8;

		}else {
			if($data['peng_ayah'] >= 2000000 && $data['peng_ayah'] <= 3000000){
				$total_1 = $total_1 + 8;
			}

		}

		if($data['ver_ibu'] == 2){
			//$total_1 = $total_1 + 8;
		}else {
			if($data['peng_ibu'] <= 750000){
				$total_1 = $total_1 + 0;
			}else if($data['peng_ibu'] >= 2000000 && $data['peng_ibu'] <= 3000000){
				$total_1 = $total_1 + 8;
			}
		}
		$total_peng = $data['peng_ayah'] + $data['peng_ibu'];

		if($total_peng >= 2000000 ){
			$total_1 = $total_1 + 9;
		}
		$bagi = $total_peng/$data['anggota'];


		if($data['ver_anggota'] == 2){
			$total_1 = $total_1 + 25;
		}else {
			if($bagi >= 1000000 ){
				$total_1 = $total_1 + 25;
				//echo $total_1."a2a <br/>";
			}
		}

		return $total_1;
	}

	public function kelompok_4($data){
		$total_1 = 0;
		if($data['ver_tanah'] == 2){
				$total_1 = $total_1 + 9;
		}else {
			if($data['luas_tanah'] >= 120 ){
				$total_1 = $total_1 + 9;
				//echo $total_1."a1a  <br/>";
			}
		}
		if($data['ver_bangunan'] == 2){
			$total_1 = $total_1 + 8;
		}else {
			if($data['luas_bangunan'] <= 70){
				$total_1 = $total_1 + 8;
				//echo $total_1."a2a <br/>";
			}
		}
		if($data['ver_spd'] == 2){
			$total_1 = $total_1 + 8;
		}else {
			if($data['ver_spd'] >= 1 ){
				$total_1 = $total_1 + 8;
				//echo $total_1."a3a <br/>";
			}
		}

		/// kedua
		if($data['ver_pbb'] == 2){
			$total_1 = $total_1 + 9;
		}else {
			if($data['pbb'] <= 150000 ){
				$total_1 = $total_1 + 9;
			}
		}

		if($data['ver_pln'] == 2){
			$total_1 = $total_1 + 8;
		}else {
			if($data['pln'] <= 1300 ){
				$total_1 = $total_1 + 8;
			}
		}

		if($data['ver_pdam'] == 2){
			$total_1 = $total_1 + 8;
		}else {
			if($data['pdam'] <= 100000 ){
				$total_1 = $total_1 + 8;
			}
		}
		//// tigaaaa
		if($data['ver_ayah'] == 2){
			$total_1 = $total_1 + 8;

		}else {
			if($data['peng_ayah'] >= 2500000 && $data['peng_ayah'] <= 3500000){
				$total_1 = $total_1 + 8;
			}

		}

		if($data['ver_ibu'] == 2){
			$total_1 = $total_1 + 8;
		}else {
			if($data['peng_ibu'] <= 750000){
				$total_1 = $total_1 + 0;
			}else if($data['peng_ibu'] >= 2500000 && $data['peng_ibu'] <= 3500000){
				$total_1 = $total_1 + 8;
			}
		}
		$total_peng = $data['peng_ayah'] + $data['peng_ibu'];

		if($total_peng >= 4000000 ){
			$total_1 = $total_1 + 9;
		}
		$bagi = $total_peng/$data['anggota'];


		if($data['ver_anggota'] == 2){
			$total_1 = $total_1 + 25;
		}else {
			if($bagi >= 1500000 ){
				$total_1 = $total_1 + 25;
			}
		}

		return $total_1;
	}

	public function kelompok_5($data){
		$total_1 = 0;
		if($data['ver_tanah'] == 2){
				$total_1 = $total_1 + 9;
		}else {
			if($data['luas_tanah'] >= 150 ){
				$total_1 = $total_1 + 9;
			}
		}
		if($data['ver_bangunan'] == 2){
			$total_1 = $total_1 + 8;
		}else {
			if($data['luas_bangunan'] <= 100){
				$total_1 = $total_1 + 8;
			}
		}
		if($data['ver_spd'] == 2){
			$total_1 = $total_1 + 8;
		}else {
			if($data['ver_spd'] >= 1 ){
				$total_1 = $total_1 + 8;
			}
		}

		/// kedua
		if($data['ver_pbb'] == 2){
			$total_1 = $total_1 + 9;
		}else {
			if($data['pbb'] >= 150000 ){
				$total_1 = $total_1 + 9;
			}
		}

		if($data['ver_pln'] == 2){
			$total_1 = $total_1 + 8;
		}else {
			if($data['pln'] >= 1300 ){
				$total_1 = $total_1 + 8;
			}
		}

		if($data['ver_pdam'] == 2){
			$total_1 = $total_1 + 8;
		}else {
			if($data['pdam'] >= 100000 ){
				$total_1 = $total_1 + 8;
			}
		}
		//// tigaaaa
		if($data['ver_ayah'] == 2){
			$total_1 = $total_1 + 8;

		}else {
			if($data['peng_ayah'] >= 3500000 ){
				$total_1 = $total_1 + 8;
			}

		}

		if($data['ver_ibu'] == 2){
			$total_1 = $total_1 + 8;
		}else {
			if($data['peng_ibu'] <= 750000){
				$total_1 = $total_1 + 0;
			}else if($data['peng_ibu'] >= 3500000 ){
				$total_1 = $total_1 + 8;
			}
		}
		$total_peng = $data['peng_ayah'] + $data['peng_ibu'];

		if($total_peng >= 5000000 ){
			$total_1 = $total_1 + 9;
		}
		$bagi = $total_peng/$data['anggota'];


		if($data['ver_anggota'] == 2){
			$total_1 = $total_1 + 25;
		}else {
			if($bagi >= 2500000 ){
				$total_1 = $total_1 + 25;
			}
		}

		return $total_1;
	}



}
