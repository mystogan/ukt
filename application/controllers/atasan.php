<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Atasan extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->database();
		$this->load->model('Uin_model');
		$this->load->helper(array('form','url','file','download'));
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

	}
	public function index(){
		// $user = $this->session->userdata('username');
		// $jalur = $this->session->userdata('jalur');
		// if($user == "" || $user == null){
		// 	$this->load->view('log_atasan',$data);
		// }else {
		// 	$data['satu'] = $this->Uin_model->getUkt(1,$jalur);
		// 	$data['dua'] = $this->Uin_model->getUkt(2,$jalur);
		// 	$data['tiga'] = $this->Uin_model->getUkt(3,$jalur);
		// 	$data['empat'] = $this->Uin_model->getUkt(4,$jalur);
		// 	$data['lima'] = $this->Uin_model->getUkt(5,$jalur);
		// 	$data['all'] = $this->Uin_model->getUkt(0,$jalur);
		// 	//print_r ($data['all']);
		// 	$this->load->view('formatasan',$data);
		// }
		// echo "string";
		header("location:".base_url()."atasan/kesimpulan");
	}
	public function kesimpulan(){

			$data['satu'] = $this->Uin_model->getUkt(1);
			$data['dua'] = $this->Uin_model->getUkt(2);
			$data['tiga'] = $this->Uin_model->getUkt(3);
			$data['empat'] = $this->Uin_model->getUkt(4);
			$data['lima'] = $this->Uin_model->getUkt(5);
			$data['enam'] = $this->Uin_model->getUkt(6);
			$data['tujuh'] = $this->Uin_model->getUkt(7);
			$set = $this->Uin_model->getSetting();
			$data['jalur'] = $set[1]['status'];
			// print_r ($data);
			$this->load->view('pustipd/header',$data);
			$this->load->view('pustipd/kesimpulan',$data);
	}
	public function sementara(){

			$data['satu'] = $this->Uin_model->getUkt(1,"pilih_ukt");
			$data['dua'] = $this->Uin_model->getUkt(2,"pilih_ukt");
			$data['tiga'] = $this->Uin_model->getUkt(3,"pilih_ukt");
			$data['empat'] = $this->Uin_model->getUkt(4,"pilih_ukt");
			$data['lima'] = $this->Uin_model->getUkt(5,"pilih_ukt");
			$data['enam'] = $this->Uin_model->getUkt(6,"pilih_ukt");
			$data['tujuh'] = $this->Uin_model->getUkt(7,"pilih_ukt");
			$set = $this->Uin_model->getSetting();
			$data['nama_jalur'] = $this->Uin_model->getjalur();
			$data['jumlah'] = $this->Uin_model->getJumlahUkt();
			$data['jalur'] = $set[1]['status'];
			// print_r ($data['jumlah']);
			$this->load->view('pustipd/header',$data);
			$this->load->view('pustipd/sementara',$data);
	}
	public function detil($kel,$jalur){
			$data['ukt'] = $kel+1;
			$data['jalur'] = $jalur;
			$data['mhs'] = $this->Uin_model->getMhs_perkelompok($jalur,$kel+1);
			$this->load->view('pustipd/header',$data);
			$this->load->view('pustipd/detilkelompok',$data);
	}
	public function kel_ukt2(){
		$kelompok = $this->input->post('kelompok2');
		$kode = $this->input->post('kode');
		$kel_ukt2 = $this->input->post('kel_ukt2');
		$pers_ukt2 = $this->input->post('pers_ukt2');
		$this->load->model('Home_model');

		$datas= $this->Home_model->getProfile($kode);
		$rekom_1 = $datas['rekom_1'];
		$kel_1 = $datas['kel_1'];

		$data = array(
				 'kel_1' => $kel_ukt2 ,
				 'kel_2' => $kel_1 ,
				 'rekom_1' => $pers_ukt2 ,
				 'rekom_2' => $rekom_1 ,
				 'pindah' => '2' ,
			);
		print_r ($data);
		$this->Home_model->updateProfile($datas['id_mhs'],$data);
		header("location:".base_url()."atasan/kesimpulan");

	}
	public function kel_ukt3(){
		$kelompok = $this->input->post('kelompok3');
		$kode = $this->input->post('kode');
		$kel_ukt3 = $this->input->post('kel_ukt3');
		$pers_ukt3 = $this->input->post('pers_ukt3');
		$this->load->model('Home_model');

		$datas= $this->Home_model->getProfile($kode);
		$rekom_1 = $datas['rekom_1'];
		$kel_1 = $datas['kel_1'];

		$data = array(
				 'kel_1' => $kel_ukt3 ,
				 'kel_3' => $kel_1 ,
				 'rekom_1' => $pers_ukt3 ,
				 'rekom_3' => $rekom_1
			);
		// print_r ($data);
		$this->Home_model->updateProfile($datas['id_mhs'],$data);
		header("location:".base_url()."atasan/kesimpulan");
	}

	public function ubahUKT(){
		$ukt = $this->input->post('ukt');
		$id_mhs = $this->input->post('kode');
		$this->load->model('Home_model');
		$data = array(
			   'kel_1' => $ukt ,
			   'rekom_1' => "0"
		);
		$this->Home_model->updateProfile($id_mhs,$data);
		header("location:".base_url()."pustipd/mhs");
	}

	public function mhsdetil(){
		$kode_mhs = $this->input->post('kode_mhs');
		$data['mhs'] = $this->Uin_model->getDetilMhs($kode_mhs);
		// print_r ($data['mhs']);
		$this->load->view('pustipd/header',$data);
		$this->load->view('pustipd/mhsdetil',$data);
	}













	public function bayangan(){
		$user = $this->session->userdata('username');
		$jalur = 4;
		if($user == "" || $user == null){
			$this->load->view('log_atasan',$data);
		}else {
			$data['satu'] = $this->Uin_model->getUkt2(1,$jalur,'mhss');
			$data['dua'] = $this->Uin_model->getUkt2(2,$jalur,'mhss');
			$data['tiga'] = $this->Uin_model->getUkt2(3,$jalur,'mhss');
			$data['empat'] = $this->Uin_model->getUkt2(4,$jalur,'mhss');
			$data['lima'] = $this->Uin_model->getUkt2(5,$jalur,'mhss');
			$data['all'] = $this->Uin_model->getUkt2(0,$jalur,'mhss');
			//print_r ($data['all']);
			$this->load->view('formatasanbayangan',$data);
		}
	}
	public function bayangan2(){
		$user = $this->session->userdata('username');
		$jalur = 4;
		if($user == "" || $user == null){
			$this->load->view('log_atasan',$data);
		}else {
			$data['satu'] = $this->Uin_model->getUkt2(1,$jalur,'mhs3');
			$data['dua'] = $this->Uin_model->getUkt2(2,$jalur,'mhs3');
			$data['tiga'] = $this->Uin_model->getUkt2(3,$jalur,'mhs3');
			$data['empat'] = $this->Uin_model->getUkt2(4,$jalur,'mhs3');
			$data['lima'] = $this->Uin_model->getUkt2(5,$jalur,'mhs3');
			$data['all'] = $this->Uin_model->getUkt2(0,$jalur,'mhs3');
			//print_r ($data['all']);
			$this->load->view('formatasanbayangan',$data);
		}
	}
	public function cek(){
		$nip = $this->input->post('username');
		$password = md5($this->input->post('password'));
		$data = $this->Uin_model->login($nip,$password,2);
		if($data == 0){
			echo "<script>alert ('Username/Password Salah !');window.location.href = '".base_url()."atasan';</script>";
		}else if($data == 1){
			$this->session->set_userdata('username',$nip);
			echo "<script>window.location.href = '".base_url()."atasan';</script>";
		}else {
			echo "<script>window.location.href = '".base_url()."atasan';</script>";
		}
	}
	public function detil3($kode){
		$kode = str_replace("'","",$kode);
		$user = $this->session->userdata('nip');
		if($user == "" || $user == null){
			$this->load->view('index2',$data);
		}else {
			$this->load->model('Home_model');
			$data['profile'] = $this->Home_model->getProfile($kode);
			$data['tgl'] = $this->Home_model->hariIni();
			//print_r ($data['profile']);
			$this->load->view('detil',$data);

		}
	}
	public function logout(){
		$this->session->unset_userdata(array('jalur' => ''));
		$this->session->unset_userdata(array('username' => ''));
		header("location:../atasan");
	}

	public function lihatdata($id){
		$data['all'] = $this->Uin_model->getMhs($id);
		//print_r ($data['all']);
		$data['id'] = $id;
		$this->load->view('datamhs',$data);
	}
	public function setjalur($id){
		$this->session->set_userdata('jalur',$id);
		header("location:../atasan");
	}
	public function prodi(){
		$prodi = $this->Uin_model->getProdi();
		print_r ($data['prodi']);

		$i = 0;
		foreach($prodi as $Hprodi){
			$jum = $this->Uin_model->getJumSudah($Hprodi['kode']);
			$belum = $this->Uin_model->getJumBelum($Hprodi['kode']);
			$data['all'][$i]['nama'] = $Hprodi['nama'];
			$data['all'][$i]['jml'] = $jum[0]['jml'];
			$data['all'][$i]['belum'] = $belum[0]['jml'];
			$i++;
		}
		//print_r ($data);
		$this->load->view('datamhs2',$data);
	}

	public function detil2($kode){
		$this->load->model('Home_model');
		$data['profile'] = $this->Home_model->getProfile($kode);
		$data['tgl'] = $this->Home_model->hariIni();
		//print_r ($data['profile']);
		$this->load->view('detil3',$data);
	}

}
