<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Prin extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model('Home_model');
		$this->load->helper(array('form','url','file','download'));
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		
	}
	
	public function cekprinttemplate($kode){
		$data['profil']  = $this->Home_model->getProfile($kode);
		$data['ukt']  = $this->Home_model->getUKT($data['profil']['kel_1'],$data['profil']['prodi']);
		$this->load->view('verifikator/surat',$data);
	}

	function jajal(){
		$this->load->view('jajal2');
	}
	
}
