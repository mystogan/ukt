<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Verifikator extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->database();
		$this->load->model('Uin_model');
		$this->load->helper(array('form','url','file','download'));
		// error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		date_default_timezone_set("Asia/Bangkok");
		$user = $this->session->userdata('nip');
		if ($user == null) {
			header("location:".base_url()."uin");
		}
	}
	public function index(){
		// echo $this->session->userdata('akses');
		$status = $this->session->userdata('status');
		if ($status == 4) {
			header("location:".base_url()."verifikator/akademik");			
		}
		$data['mhs'] = $this->Uin_model->getAllMhsVerifikator();
		$data['jalur'] = $this->Uin_model->getJalur();
		$this->load->view('verifikator/header',$data);
		$this->load->view('verifikator/detilkelompok',$data);
	}
	public function akademik(){
		// echo $this->session->userdata('akses');
		$data['mhs'] = $this->Uin_model->getAllMhsAkademik();
		$data['jalur'] = $this->Uin_model->getJalur();
		$this->load->view('verifikator/header',$data);
		$this->load->view('verifikator/detilakademik',$data);
	}
	
	public function mhsdetil(){
		$kode_mhs = $this->input->post('kode_mhs');
		$data['mhs'] = $this->Uin_model->getDetilMhs($kode_mhs);
		// print_r ($data['mhs']);
		$this->load->view('verifikator/header',$data);
		$this->load->view('verifikator/mhsdetil',$data);
	}
	
	public function uktMhs(){
		$data['mhs'] = $this->Uin_model->getuktMhs();
		$this->load->view('verifikator/header',$data);
		$this->load->view('verifikator/harga_jalur',$data);
	}
	public function verikjs($kode,$status){
		$this->db->set($kolom, '1');
		$this->db->set($id_input, $input);
		$this->db->where('id_mhs', $id_mhs);
		$this->db->update('mhs');
		if($this->db->affected_rows() > 0){
			$data['cek'] = 1;
			$data['pesan'] = "Perubahan Telah disimpan :)";
		}else {
			$data['cek'] = 0;
			$data['pesan'] = "Terjadi Kesalahan Silahkan Ulangi Lagi.";
		}
	}
	public function saveverifikator(){
		$kolom = $this->input->post('kolom');
		$input = $this->input->post('input');
		$id_mhs = $this->input->post('id_mhs');
		$id_input = $this->input->post('id_input');
		// $data['kolom'] = $kolom;
		// $data['input'] = $input;
		// $data['id_mhs'] = $id_mhs;
		$this->db->set($kolom, '1');
		$this->db->set($id_input, $input);
		$this->db->where('id_mhs', $id_mhs);
		$this->db->update('mhs');
		if($this->db->affected_rows() > 0){
			$data['cek'] = 1;
			$data['pesan'] = "Perubahan Telah disimpan :)";
		}else {
			$data['cek'] = 0;
			$data['pesan'] = "Terjadi Kesalahan Silahkan Ulangi Lagi.";
		}
		echo json_encode($data);

	}
	public function cekprinttemplate($kode){
		$data['mhs'] = $this->Uin_model->getuktMhs();
		$this->load->view('verifikator/harga_jalur',$data);
	}

	public function update($kolom,$nilai,$kode){
		$this->db->set($kolom, $nilai);
		$this->db->where('kode', $kode);
		$this->db->update('mhs');
		header("location:".base_url()."verifikator/akademik");			
	}

}
