<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Finalisasi extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->database();
		$this->load->model('Home_model');
		$this->load->helper(array('form','url','file','download'));
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

	}
	public function index(){
		$user = $this->session->userdata('username');
		$jalur = $this->session->userdata('jalur');
		if($user == "" || $user == null){
			$this->load->view('log_atasan',$data);
		}else {
			//print_r ($data['all']);
			$this->load->view('formatasan',$data);
		}

	}
	public function mhs_finalisasi(){
		$status = $this->input->post('status');
		$user = $this->session->userdata('id_mhs');
		$data['mhs_finalisasi'] = 1;
		$this->Home_model->update_finalisasi($user,$data);
		echo $user;
	}

}
