<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Simulasi extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->database();
		$this->load->model('Uin_model');
		$this->load->model('Simulasi_model');
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));


	}
	public function index(){

	}
	public function rekap(){
		$profile['mhs'] = $this->Uin_model->getAllMhsLaporan();
	 	$this->load->view('pustipd/laporan',$profile);

	}
public function index2(){
	$kode = 4180371312;
	$data['ver_tanah'] = 1;
	$data['ver_bangunan'] = 1;
	$data['ver_atap'] = 1;
	$data['ver_lantai'] = 1;
	$data['ver_pbb'] = 1;
	$data['ver_pln'] = 1;
	$data['ver_pdam'] = 1;
	$data['ver_ayah'] = 1;
	$data['ver_ibu'] = 1;
	$data['ver_anggota'] = 1;
	$this->Simulasi_model->updateProfile($kode,$data);

	$this->pilih_ukt($kode);

}

public function update_ver(){
		$all = $this->Uin_model->getgetmbohwes();
		// print_r ($all);

		foreach ($all as $as ) {
			$data = null;
			$kode = $as['kode'];
			if ($as['pilih_ukt'] == 1) {
				$data['ver_kjs'] = 1;
				$data['ver_alat_kom'] = 1;
				$data['ver_srt_mrk'] = 1;
				$data['ver_penyakit'] = 1;
				$data['ver_panti_yatim'] = 1;
				$data['ver_tetangga'] = 1;
				$data['ver_surat_asli'] = 1;
			}else if ($as['pilih_ukt'] == 1) {
				$data['ver_anakkuliah'] = 1;
				$data['ver_sktm'] = 1;
				$data['ver_tetangga'] = 1;
			}else if ($as['pilih_ukt'] == 1) {
				$data['ver_tetangga'] = 1;
			}

		}
	}
	public function uin2(){
		$this->load->view('form_admin',$data);

	}


	public function pilih_ukt($kode = '0',$url = 'uin'){
		$kode = 4180224943;
		$this->load->model('Home_model');
		$mhs = $this->Home_model->getProfile($kode);
		$this->cek_prin($mhs);
		$data['1'] = $this->cek_ukt1($mhs);
		$data['2'] = $this->cek_ukt2($mhs);
		$data['3'] = $this->cek_ukt3($mhs);
		$data['4'] = $this->cek_ukt4($mhs);
		$data['5'] = $this->cek_ukt5($mhs);
		$data['6'] = $this->cek_ukt6($mhs);
		$data['7'] = $this->cek_ukt7($mhs);
		echo "<br>";
		print_r ($data);
		echo "<br>";
		echo "<br>";

		if($mhs['ver_alat_kom'] >= '1' && $mhs['ver_srt_mrk'] >= '1' &&
			$mhs['ver_penyakit'] >= '1' && $mhs['ver_panti_yatim'] >= '1' &&
			$mhs['ver_surat_asli'] >= '1' && $mhs['ver_tetangga'] >= '1'){
				if($mhs['ver_alat_kom'] == '1' && $mhs['ver_srt_mrk'] == '1' &&
					$mhs['ver_penyakit'] == '1' && $mhs['ver_panti_yatim'] == '1' &&
					$mhs['ver_surat_asli'] == '1' && $mhs['ver_tetangga'] == '1'){
					$datas[] = $data['1'];
					$datas[] = $data['2'];
				}else {
					$datas[] = $data['2'];
					$datas[] = $data['3'];
				}
		}else if($mhs['ver_tetangga'] >= '1' && $mhs['ver_sktm'] >= '1' &&
				$mhs['ver_anakkuliah'] >= '1'  ){
					if ($mhs['ver_tetangga'] == '1' && $mhs['ver_sktm'] == '1' &&
							$mhs['ver_anakkuliah'] == '1') {
								$datas[] = $data['2'];
								$datas[] = $data['3'];
					}else {
						$datas[] = $data['3'];
						$datas[] = $data['4'];
					}
		}else if($mhs['ver_tetangga'] >= '1' ){
			if ($mhs['ver_tetangga'] == '1') {
				$datas[] = $data['3'];
				$datas[] = $data['4'];
			}else {
				$datas[] = $data['3'];
				$datas[] = $data['4'];
			}
		}else {
			if ($mhs['pilih_ukt'] == 4) {
				$datas[] = $data['4'];
				$datas[] = $data['5'];
			}else if ($mhs['pilih_ukt'] == 5) {
				$datas[] = $data['4'];
				$datas[] = $data['5'];
			}else if ($mhs['pilih_ukt'] == 6) {
				$datas[] = $data['6'];
				$datas[] = $data['7'];
			}else if ($mhs['pilih_ukt'] == 7) {
				$datas[] = $data['6'];
				$datas[] = $data['7'];
			}else {
				$datas[] = $data['7'];
				$datas[] = $data['7'];
			}
		}
		print_r ($datas);
		if($datas[0]['persen'] >= $datas[1]['persen'] ){
			$ukt1 = $datas[0]['kelompok'];
			$persen1 = $datas[0]['persen'];
			$alasan1 = json_encode($datas[0]['pilih']);
			$ukt2 = $datas[1]['kelompok'];
			$persen2 = $datas[1]['persen'];
			$alasan2 = json_encode($datas[1]['pilih']);

		}else {
			$ukt1 = $datas[1]['kelompok'];
			$persen1 = $datas[1]['persen'];
			$alasan1 = json_encode($datas[1]['pilih']);
			$ukt2 = $datas[0]['kelompok'];
			$persen2 = $datas[0]['persen'];
			$alasan2 = json_encode($datas[0]['pilih']);

		}
		$data_input['rekom_1'] =  $persen1;
		$data_input['kel_1'] =  $ukt1;
		$data_input['alasan_1'] =  $alasan1;

		$data_input['rekom_2'] =  $persen2;
		$data_input['kel_2'] =  $ukt2;
		$data_input['alasan_2'] =  $alasan2;
		$this->Home_model->input_perhitungan($data_input,$kode);

	}
	public function cek_prin($mhs){
		echo "1. Luas Tanah ";
		print_r ($mhs['luas_tanah']);
		echo "<br/>";
		echo "2. Luas Bangunan ";
		print_r ($mhs['luas_bangunan']);
		echo "<br/>";
		echo "alat komunikasi ";
		print_r ($mhs['alat_kom']);
		echo "<br/>";
		echo "3. PBB ";
		print_r ($mhs['pbb']);
		echo "<br/>";
		echo "4. pln ";
		print_r ($mhs['pln']);
		echo "<br/>";
		echo "5. PDAM ";
		print_r ($mhs['pdam']);
		echo "<br/>";
		echo "6. Penghasilan Ayah ";
		print_r ($mhs['peng_ayah']);
		echo "<br/>";
		echo "7. Penghasilan Ibu ";
		print_r ($mhs['peng_ibu']);
		echo "<br/>";
		echo "8. penghasilan_total ";
		print_r ($mhs['penghasilan_total']);
		echo "<br/>";
		echo " Jumlah Anggota ";
		print_r ($mhs['anggota']);
		echo "<br/>";
		echo "9. Jumlah Pengeluaran ";
		$pengeluaran = $mhs['penghasilan_total']/$mhs['anggota'];
		print_r ($pengeluaran);
		echo "<br/>";
		echo "10. Sepeda Motor ";
		echo "<br/>";

	}
	public function cek_ukt1($mhs){

		$hasil = 'cek_ukt1';
		$total = 0;
		// cek luas tanah apakah mencukupi atau tidak
		if ($mhs['luas_tanah'] >= 50 && $mhs['luas_tanah'] <= 84) {
			$total += 10;
			$data['pilih'][] = "Luas Tanah";
		}
		// cek luas bangunnan apakah sesuai atau tidak
		if ($mhs['luas_bangunan'] >= 21 && $mhs['luas_bangunan'] <= 36) {
			$total += 10;
			$data['pilih'][] = "Luas Bangunan";
		}
		// cek Sepeda apakah punya atau tidak
		if ($mhs['ver_spd'] == 1 ) {
			$total += 10;
			$data['pilih'][] = "Sepeda";
		}
		// cek alat komunikasi punya atau tidak
		if ($mhs['alat_kom'] <= 800000 ) {
			$total += 10;
			$data['pilih'][] = "Alat Komunikasi";
		}
		// cek PBB memenuhi kriteria atau tidak
		if ($mhs['pbb'] <= 100000 ) {
			$total += 10;
			$data['pilih'][] = "PBB";
		}
		// cek PLN memenuhi kriteria atau tidak
		if ($mhs['pln'] <= 450 ) {
			$total += 10;
			$data['pilih'][] = "PLN";
		}
		// cek PDAM memenuhi kriteria atau tidak
		if ($mhs['pdam'] <= 50000 ) {
			$total += 10;
			$data['pilih'][] = "PDAM";
		}
		// cek Penghasilan Ayah memenuhi kriteria atau tidak
		if ($mhs['peng_ayah'] <= 1000000 ) {
			$total += 10;
			$data['pilih'][] = "Penghasilan Ayah";
		}
		// cek Penghasilan Ibu memenuhi kriteria atau tidak
		if ($mhs['peng_ibu'] <= 1000000 ) {
			$total += 10;
			$data['pilih'][] = "Penghasilan Ibu";
		}
		// menghitung oengeluaran dari keluarga tersebut
		$pengeluaran = $mhs['penghasilan_total']/$mhs['anggota'];
		// cek pengeluaran keluarga memenuhi kriteria atau tidak
		if ($mhs['penghasilan_total'] >= 1000000 && $mhs['penghasilan_total'] <= 1500000 ) {
			$total += 10;
			$data['pilih'][] = "Pengeluaran ";
		}
		$data['kelompok'] = '1';
		$data['persen'] = $total;
		return $data;
	}
	public function cek_ukt2($mhs){
		$total = 0;
		// cek luas tanah apakah mencukupi atau tidak
		if ($mhs['luas_tanah'] >= 90 && $mhs['luas_tanah'] <= 105) {
			$total += 10;
			$data['pilih'][] = "Luas Tanah";
		}
		// cek luas bangunnan apakah sesuai atau tidak
		if ($mhs['luas_bangunan'] >= 45 && $mhs['luas_bangunan'] <= 54) {
			$total += 10;
			$data['pilih'][] = "Luas bangunan";
		}
		// cek Sepeda apakah punya atau tidak
		if ($mhs['ver_spd'] == 1 ) {
			$total += 10;
			$data['pilih'][] = "Sepeda";
		}
		// cek PBB memenuhi kriteria atau tidak
		if ($mhs['pbb'] <= 150000 ) {
			$total += 10;
			$data['pilih'][] = "PBB";
		}
		// cek PLN memenuhi kriteria atau tidak
		if ($mhs['pln'] <= 900 ) {
			$total += 10;
			$data['pilih'][] = "PLN";
		}
		// cek PDAM memenuhi kriteria atau tidak
		if ($mhs['pdam'] <= 100000 ) {
			$total += 10;
			$data['pilih'][] = "PDAM";
		}
		// cek Penghasilan Ayah memenuhi kriteria atau tidak
		if ($mhs['peng_ayah'] >= 1000000 && $mhs['peng_ayah'] <= 2000000 ) {
			$total += 10;
			$data['pilih'][] = "Penghasilan Ayah";
		}
		// cek Penghasilan Ibu memenuhi kriteria atau tidak
		if ($mhs['peng_ibu'] >= 1000000 && $mhs['peng_ibu'] <= 2000000 ) {
			$total += 10;
			$data['pilih'][] = "Penghasilan Ibu";
		}
		// cek TOTal Pengahasilan keluarga memenuhi kriteria atau tidak
		if ($mhs['penghasilan_total'] >= 1500000 && $mhs['penghasilan_total'] <= 2000000 ) {
			$total += 10;
			$data['pilih'][] = "Penghasilan Total";
		}
		// menghitung oengeluaran dari keluarga tersebut
		$pengeluaran = $mhs['penghasilan_total']/$mhs['anggota'];
		// cek pengeluaran keluarga memenuhi kriteria atau tidak
		if ($pengeluaran <= 1000000 ) {
			$total += 10;
			$data['pilih'][] = "pengeluaran";
		}
		$data['kelompok'] = '2';
		$data['persen'] = $total;

		// echo "$total";
		return $data;
	}
	public function cek_ukt3($mhs){
		$total = 0;
		// cek luas tanah apakah mencukupi atau tidak
		if ($mhs['luas_tanah'] >= 90 && $mhs['luas_tanah'] <= 105) {
			$total += 10;
			$data['pilih'][] = "Luas Tanah";
		}
		// cek luas bangunnan apakah sesuai atau tidak
		if ($mhs['luas_bangunan'] >= 45 && $mhs['luas_bangunan'] <= 54) {
			$total += 10;
			$data['pilih'][] = "Luas Bangunan";
		}
		// cek Sepeda apakah punya atau tidak
		if ($mhs['ver_spd'] == 1 ) {
			$total += 10;
			$data['pilih'][] = "Sepeda";
		}
		// cek PBB memenuhi kriteria atau tidak
		if ($mhs['pbb'] <= 150000 ) {
			$total += 10;

			$data['pilih'][] = "PBB";
		}
		// cek PLN memenuhi kriteria atau tidak
		if ($mhs['pln'] <= 900 ) {
			$total += 10;

			$data['pilih'][] = "PLN";
		}
		// cek PDAM memenuhi kriteria atau tidak
		if ($mhs['pdam'] <= 100000 ) {
			$total += 10;
			$data['pilih'][] = "PDAM ";
		}
		// cek Penghasilan Ayah memenuhi kriteria atau tidak
		if ($mhs['peng_ayah'] >= 2000000 && $mhs['peng_ayah'] <= 3000000 ) {
			$total += 10;
			$data['pilih'][] = "Penghasilan Ayah";
		}
		// cek Penghasilan Ibu memenuhi kriteria atau tidak
		if ($mhs['peng_ibu'] >= 2000000 && $mhs['peng_ibu'] <= 3000000 ) {
			$total += 10;
			$data['pilih'][] = "Penghasilan Ibu";
		}
		// cek TOTal Pengahasilan keluarga memenuhi kriteria atau tidak
		if ($mhs['penghasilan_total'] >= 2000000 && $mhs['penghasilan_total'] <= 3500000 ) {
			$total += 10;
			$data['pilih'][] = "Penghasilan Total";
		}
		// menghitung oengeluaran dari keluarga tersebut
		$pengeluaran = $mhs['penghasilan_total']/$mhs['anggota'];
		// cek pengeluaran keluarga memenuhi kriteria atau tidak
		if ($pengeluaran <= 1250000 ) {
			$total += 10;
			$data['pilih'][] = "Pengeluaran";
		}
		$data['kelompok'] = '3';
		$data['persen'] = $total;

		// echo "$total";
		return $data;
	}
	// cek UKT 4
	public function cek_ukt4($mhs){
		$total = 0;
		// cek luas tanah apakah mencukupi atau tidak
		if ($mhs['luas_tanah'] >= 120 ) {
			$total += 10;
			$data['pilih'][] = "Luas Tanah";
		}
		// cek luas bangunnan apakah sesuai atau tidak
		if ($mhs['luas_bangunan'] >= 70) {
			$total += 10;
			$data['pilih'][] = "Luas Bangunan";
		}
		// cek Sepeda apakah punya atau tidak
		if ($mhs['ver_spd'] == 1 ) {
			$total += 10;
			$data['pilih'][] = "Sepeda";
		}
		// cek PBB memenuhi kriteria atau tidak
		if ($mhs['pbb'] >= 150000 ) {
			$total += 10;
			$data['pilih'][] = "PBB";
		}
		// cek PLN memenuhi kriteria atau tidak
		if ($mhs['pln'] <= 1300 ) {
			$total += 10;
			$data['pilih'][] = "PLN";
		}
		// cek PDAM memenuhi kriteria atau tidak
		if ($mhs['pdam'] >= 100000 ) {
			$total += 10;
			$data['pilih'][] = "PDAM";
		}
		// cek Penghasilan Ayah memenuhi kriteria atau tidak
		if ($mhs['peng_ayah'] >= 2500000 && $mhs['peng_ayah'] <= 3000000 ) {
			$total += 10;
			$data['pilih'][] = "Penghasilan Ayah";
		}
		// cek Penghasilan Ibu memenuhi kriteria atau tidak
		if ($mhs['peng_ibu'] >= 2000000 && $mhs['peng_ibu'] <= 3000000 ) {
			$total += 10;
			$data['pilih'][] = "Penghasilan Ibu";
		}
		// cek TOTal Pengahasilan keluarga memenuhi kriteria atau tidak
		if ($mhs['penghasilan_total'] >= 3500000 && $mhs['penghasilan_total'] <= 5000000  ) {
			$total += 10;
			$data['pilih'][] = "Penghasilan Total";
		}
		// menghitung oengeluaran dari keluarga tersebut
		$pengeluaran = $mhs['penghasilan_total']/$mhs['anggota'];
		// cek pengeluaran keluarga memenuhi kriteria atau tidak
		if ($pengeluaran <= 3000000 ) {
			$total += 10;
			$data['pilih'][] = "Pengeluaran";
		}
		$data['kelompok'] = '4';
		$data['persen'] = $total;

		return $data;
	}
	// cek UKT 5
	public function cek_ukt5($mhs){
		$total = 0;
		// cek luas tanah apakah mencukupi atau tidak
		if ($mhs['luas_tanah'] >= 150 ) {
			$total += 10;
			$data['pilih'][] = "Luas Tanah";
		}
		// cek luas bangunnan apakah sesuai atau tidak
		if ($mhs['luas_bangunan'] >= 100) {
			$total += 10;
			$data['pilih'][] = "Luas Bangunan";
		}
		// cek Sepeda apakah punya atau tidak
		if ($mhs['ver_spd'] == 1 ) {
			$total += 10;
			$data['pilih'][] = "Sepeda";
		}
		// cek PBB memenuhi kriteria atau tidak
		if ($mhs['pbb'] >= 250000 ) {
			$total += 10;
			$data['pilih'][] = "PBB";
		}
		// cek PLN memenuhi kriteria atau tidak
		if ($mhs['pln'] >= 1300 ) {
			$total += 10;
			$data['pilih'][] = "PLN";
		}
		// cek PDAM memenuhi kriteria atau tidak
		if ($mhs['pdam'] >= 100000 ) {
			$total += 10;
			$data['pilih'][] = "PDAM";
		}
		// cek Penghasilan Ayah memenuhi kriteria atau tidak
		if ($mhs['peng_ayah'] >= 6000000  ) {
			$total += 10;
			$data['pilih'][] = "Penghasilan Ayah";
		}
		// cek Penghasilan Ibu memenuhi kriteria atau tidak
		if ($mhs['peng_ibu'] >= 3500000) {
			$total += 10;
			$data['pilih'][] = "Penghasilan Ibu";
		}
		// cek TOTal Pengahasilan keluarga memenuhi kriteria atau tidak
		if ($mhs['penghasilan_total'] >= 5000000 && $mhs['penghasilan_total'] <= 6500000 ) {
			$total += 10;
			$data['pilih'][] = "Penghasilan Total";
		}
		// menghitung oengeluaran dari keluarga tersebut
		$pengeluaran = $mhs['penghasilan_total']/$mhs['anggota'];
		// cek pengeluaran keluarga memenuhi kriteria atau tidak
		if ($pengeluaran >= 3500000 ) {
			$total += 10;
			$data['pilih'][] = "Pengeluaran";
		}
		$data['kelompok'] = '5';
		$data['persen'] = $total;

		return $data;
	}
	// cek UKT 6
	public function cek_ukt6($mhs){
		$total = 0;
		// cek luas tanah apakah mencukupi atau tidak
		if ($mhs['luas_tanah'] >= 150 ) {
			$total += 10;
			$data['pilih'][] = "Luas Tanah";
		}
		// cek luas bangunnan apakah sesuai atau tidak
		if ($mhs['luas_bangunan'] >= 100) {
			$total += 10;
			$data['pilih'][] = "Luas Bangunan";
		}
		// cek Sepeda apakah punya atau tidak
		if ($mhs['ver_spd'] == 1 ) {
			$total += 10;
			$data['pilih'][] = "Sepeda";
		}
		// cek PBB memenuhi kriteria atau tidak
		if ($mhs['pbb'] >= 350000 ) {
			$total += 10;
			$data['pilih'][] = "PBB";
		}
		// cek PLN memenuhi kriteria atau tidak
		if ($mhs['pln'] >= 1300 ) {
			$total += 10;
			$data['pilih'][] = "PLN";
		}
		// cek PDAM memenuhi kriteria atau tidak
		if ($mhs['pdam'] >= 100000 ) {
			$total += 10;
			$data['pilih'][] = "PDAM";
		}
		// cek Penghasilan Ayah memenuhi kriteria atau tidak
		if ($mhs['peng_ayah'] >= 7500000  ) {
			$total += 10;
			$data['pilih'][] = "Penghasilan Ayah";
		}
		// cek Penghasilan Ibu memenuhi kriteria atau tidak
		if ($mhs['peng_ibu'] >= 3500000) {
			$total += 10;
			$data['pilih'][] = "Penghasilan Ibu";
		}
		// cek TOTal Pengahasilan keluarga memenuhi kriteria atau tidak
		if ($mhs['penghasilan_total'] >= 6500000 && $mhs['penghasilan_total'] <= 8000000 ) {
			$total += 10;
			$data['pilih'][] = "Penghasilan Total";
		}
		// menghitung oengeluaran dari keluarga tersebut
		$pengeluaran = $mhs['penghasilan_total']/$mhs['anggota'];
		// cek pengeluaran keluarga memenuhi kriteria atau tidak
		if ($pengeluaran >= 4500000 ) {
			$total += 10;
			$data['pilih'][] = "Pengeluaran";
		}
		$data['kelompok'] = '6';
		$data['persen'] = $total;

		return $data;
	}
	// cek UKT 6
	public function cek_ukt7($mhs){
		$total = 0;
		// cek luas tanah apakah mencukupi atau tidak
		if ($mhs['luas_tanah'] >= 150 ) {
			$total += 10;
			$data['pilih'][] = "Luas Tanah";
		}
		// cek luas bangunnan apakah sesuai atau tidak
		if ($mhs['luas_bangunan'] >= 100) {
			$total += 10;
			$data['pilih'][] = "Luas Bangunan";
		}
		// cek Sepeda apakah punya atau tidak
		if ($mhs['ver_spd'] == 1 ) {
			$total += 10;
			$data['pilih'][] = "Sepeda";
		}
		// cek PBB memenuhi kriteria atau tidak
		if ($mhs['pbb'] >= 450000 ) {
			$total += 10;
			$data['pilih'][] = "PBB";
		}
		// cek PLN memenuhi kriteria atau tidak
		if ($mhs['pln'] >= 1300 ) {
			$total += 10;
			$data['pilih'][] = "PLN";
		}
		// cek PDAM memenuhi kriteria atau tidak
		if ($mhs['pdam'] >= 200000 ) {
			$total += 10;
			$data['pilih'][] = "PDAM";
		}
		// cek Penghasilan Ayah memenuhi kriteria atau tidak
		if ($mhs['peng_ayah'] >= 10000000  ) {
			$total += 10;
			$data['pilih'][] = "Penghasilan Ayah";
		}
		// cek Penghasilan Ibu memenuhi kriteria atau tidak
		if ($mhs['peng_ibu'] >= 3500000) {
			$total += 10;
			$data['pilih'][] = "Penghasilan Ibu";
		}
		// cek TOTal Pengahasilan keluarga memenuhi kriteria atau tidak
		if ($mhs['penghasilan_total'] >= 8000000  ) {
			$total += 10;
			$data['pilih'][]= "Penghasilan Total";
		}
		// menghitung oengeluaran dari keluarga tersebut
		$pengeluaran = $mhs['penghasilan_total']/$mhs['anggota'];
		// cek pengeluaran keluarga memenuhi kriteria atau tidak
		if ($pengeluaran >= 4500000 ) {
			$total += 10;
			$data['pilih'][] = "Pengeluaran";
		}
		$data['kelompok'] = '7';
		$data['persen'] = $total;

		return $data;
	}


}
