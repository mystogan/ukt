<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Uin extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->database();
		$this->load->model('Uin_model');
		$this->load->helper(array('form','url','file','download'));
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));


	}
	public function index(){
		$akses = $this->session->userdata('akses');
		$get = $this->session->userdata('nama');
		$hasil = explode("-",$get);
		$prodi = $hasil[1];
		// $prodi = "SINF";
	
		
		if($akses == "" || $prodi == null){
			$this->load->view('kaprodi/login_prodi',$data);
		}else if($akses == 1){
			$data['prodi'] = $prodi;
			$data['all'] = $this->Uin_model->getAllMhs($prodi);
			// $data['all'] = $this->Uin_model->getAllMhsPustipd();
			$this->load->view('kaprodi/form_kaprod',$data);
		}else {
			$data['all'] = $this->Uin_model->getAllMhsPustipd();
			$this->load->view('pustipd/form_admin',$data);
		}
	}
	public function arifin(){
		$data['prodi'] = $prodi;
		$data['all'] = $this->Uin_model->getAllMhsPustipd();

		$this->load->view('kaprodi/form_kaprod',$data);

	}
	public function uin2(){
		$data['all'] = $this->Uin_model->getAllMhsPustipd(1);
		$this->load->view('form_admin',$data);

	}
	public function cek(){
		// akses 1 = kaprodi, 2= pustipd, 3=rektor
		$nip = trim($this->input->post('nip'));
		$password = ($this->input->post('password'));
		$hasil = file_get_contents("http://eis.uinsby.ac.id/ukt/getunit2");
		$pas = json_decode($hasil,$true);
		$unit = $pas->id;
		$logins = file_get_contents("http://eis.uinsby.ac.id/ukt/loginDosen/".$nip."/".$password."/".$unit);
		$login = json_decode($logins,$true);
		$cekloginhelpdesk = $this->Uin_model->login($nip,$password);
		if ($cekloginhelpdesk != null) {
			
			$this->session->set_userdata('nip',$cekloginhelpdesk[0]['id']);
			$this->session->set_userdata('nama',$cekloginhelpdesk[0]['username']);
			$this->session->set_userdata('id_user',$cekloginhelpdesk[0]['id']);
			$this->session->set_userdata('akses',$cekloginhelpdesk[0]['status']);
			echo "<script>window.location.href = '".base_url()."helpdesk';</script>";				
			// echo "<script>window.location.href = '".base_url()."verifikator';</script>";			
		}else if ($login->id != null) {
			$this->session->set_userdata('nip',$login->nip);
			$this->session->set_userdata('nama',$login->nama);
			$this->session->set_userdata('id_user',$login->id);
			$this->session->set_userdata('akses','2');
			 echo "<script>window.location.href = '".base_url()."pustipd';</script>";
		}else {
			// login untuk verifikator 
			$loginverifikator = $this->Uin_model->loginVerifikator($nip);
			if ($loginverifikator != null) {
				$this->load->model('Simpeg_model');
				$getlogin = $this->Simpeg_model->loginVerifikator($nip, md5($password));
				if ($getlogin != null) {
					$this->session->set_userdata('nip',$getlogin['nip']);
					$this->session->set_userdata('nama',$getlogin['nama']);
					$this->session->set_userdata('id_user',$getlogin['id']);
					$this->session->set_userdata('akses',$loginverifikator[0]['password']);			
					$this->session->set_userdata('status',$loginverifikator[0]['status']);			
					echo "<script>window.location.href = '".base_url()."verifikator';</script>";							
				}else {
					echo "<script>
					alert ('Password Anda Salah :(');
					window.location.href = '".base_url()."uin';</script>";
	
				}
			}else {
				echo "<script>
				alert ('Maaf anda Bukan Verifikator ...');
				window.location.href = '".base_url()."uin';</script>";
			}
		}

	}
	public function bayangan(){
		echo "sapi";
		// $this->load->view('form_admin2',$data);
	}
	public function detil($kode){

		$kode = str_replace("'","",$kode);
		$user = $this->session->userdata('nip');
		if($user == "" || $user == null){
			$this->load->view('kaprodi/login_prodi',$data);
		}else {
			$this->load->model('Home_model');
			$data['profil'] = $this->Home_model->getProfile($kode);
			$data['tgl'] = $this->Home_model->hariIni();
			$this->load->view('kaprodi/detil',$data);

		}
	}

	public function logout(){

		$this->session->unset_userdata(array('nip' => ''));
		$this->session->unset_userdata(array('nama' => ''));
		$this->session->unset_userdata(array('id_user' => ''));
		$this->session->unset_userdata(array('akses' => ''));
		header("location:../uin");
	}

	public function panggil_hitung($kode){
		require_once(APPPATH.'controllers/perhitungan.php');
		$proses = new Perhitungan();
		$proses->pilih_ukt($kode);
	}
	public function validasi($id,$nama){
		$this->load->model('Home_model');
		$kode = $this->input->post('kode');
		$id_mhs = $this->input->post('id_mhs');
		$data = array(
			$nama => $id
		);
		$this->Home_model->updateProfile($id_mhs,$data);
		$mhs = $this->Home_model->getProfile($kode);

		if ($mhs['ver_tanah'] == '1' && $mhs['ver_bangunan'] == '1' && $mhs['ver_spd'] == '1'
			&& $mhs['ver_atap'] == '1' && $mhs['ver_lantai'] == '1' && $mhs['ver_pbb'] == '1'
			&& $mhs['ver_pln'] == '1' && $mhs['ver_pdam'] == '1' && $mhs['ver_ayah'] == '1'
			&& $mhs['ver_ibu'] == '1' && $mhs['ver_anggota'] == '1'){
				if ($mhs['pilih_ukt'] == 1) {
					if($mhs['ver_alat_kom'] == '1' && $mhs['ver_srt_mrk'] == '1' &&
					$mhs['ver_penyakit'] == '1' && $mhs['ver_panti_yatim'] == '1' &&
					$mhs['ver_surat_asli'] == '1' ){
						// proses perhitungan fuzzy
						$this->panggil_hitung($kode);
					}else {
						if($mhs['ver_alat_kom'] != '' && $mhs['ver_srt_mrk'] != '' &&
						$mhs['ver_penyakit'] != '' && $mhs['ver_panti_yatim'] != '' &&
						$mhs['ver_surat_asli'] != '' ){
							header("location:".base_url()."uin/");
						}else {
							header("location:".base_url()."uin/detil/".$kode);
						}
					}
				}else if ($mhs['pilih_ukt'] == 2) {
					if($mhs['ver_tetangga'] == '1' && $mhs['ver_sktm'] == '1' &&
						$mhs['ver_anakkuliah'] == '1'  ){
						// proses perhitungan fuzzy
						$this->panggil_hitung($kode);
					}else {
						if($mhs['ver_tetangga'] != '' && $mhs['ver_sktm'] != '' &&
						$mhs['ver_anakkuliah'] != ''  ){
							header("location:".base_url()."uin/");
						}else {
							header("location:".base_url()."uin/detil/".$kode);
						}
					}
				}else if($mhs['pilih_ukt'] == 3){
					if($mhs['ver_tetangga'] >= '1'){
						// proses perhitungan fuzzy
						$this->panggil_hitung($kode);
					}else {
						if($mhs['ver_tetangga'] != ''  ){
							header("location:".base_url()."uin/");
						}else {
							header("location:".base_url()."uin/detil/".$kode);
						}
					}
				}else {
					// proses perhitungan pilih ukt
					// echo "proses perhitungan pilih ukt 4,5,6,7";
					$this->panggil_hitung($kode);
				}

			}else {

				if ($mhs['ver_tanah'] != '' && $mhs['ver_bangunan'] != '' && $mhs['ver_spd'] != ''
				&& $mhs['ver_atap'] != '' && $mhs['ver_lantai'] != '' && $mhs['ver_pbb'] != ''
				&& $mhs['ver_pln'] != '' && $mhs['ver_pdam'] != '' && $mhs['ver_ayah'] != ''
				&& $mhs['ver_ibu'] != '' && $mhs['ver_anggota'] != ''){
					if ($mhs['pilih_ukt'] == 1) {
						if($mhs['ver_alat_kom'] != '' && $mhs['ver_srt_mrk'] != '' &&
						$mhs['ver_penyakit'] != '' && $mhs['ver_panti_yatim'] != '' &&
						$mhs['ver_surat_asli'] != '' ){
							header("location:".base_url()."uin/");
						}else {
							header("location:".base_url()."uin/detil/".$kode);
						}
					}else if ($mhs['pilih_ukt'] == 2) {
						if($mhs['ver_tetangga'] != '' && $mhs['ver_sktm'] != '' &&
						$mhs['ver_anakkuliah'] != ''  ){
							header("location:".base_url()."uin/");
						}else {
							header("location:".base_url()."uin/detil/".$kode);
						}
					}else if($mhs['pilih_ukt'] == 3){
						if($mhs['ver_tetangga'] != ''  ){
							header("location:".base_url()."uin/");
						}else {
							header("location:".base_url()."uin/detil/".$kode);
						}
					}else {
						header("location:".base_url()."uin/");
					}

				}else {
					header("location:".base_url()."uin/detil/".$kode);
				}
			}



	}



}
